(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['chats'] = template({"1":function(container,depth0,helpers,partials,data) {
    return " ";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"messages") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":1,"column":28}}})) != null ? stack1 : "")
    + "\nHandlebars.registerHelper(\"each\", function(context, options) {\n    var listVal= \"\";\n    _.forEach(context, function(value, key){\n        listVal += \"<li class='media content-divider justify-content-center text-muted mx-0'><span class='text-muted px-2 font-weight-semibold font-italic'>\"+ timeAgoFormat(key) +\"</span></li>\";\n        _.forEach(value, function(item,ind){\n            if(item.message_side){\n                listVal += \"<li class='media media-chat-item-reverse'><div class='media-body'><div class='media-chat-item'>\" + (item.text)+ \"</div><div class='font-size-sm text-muted mt-1'>\"+ (item.time) +\"<a href='#'><i class='icon-pin-alt ml-2 text-muted'></i></a></div></div><div class='ml-3'></div></li>\"\n            }else{\n                listVal += \"<li class='media'><div class='mr-3'><a href=''><img src='' class='rounded-circle' width='40' height='40' alt=''></a></div><div class='media-body'><div class='media-chat-item'>\" + (item.text)+ \"</div><div class='font-size-sm text-muted mt-1'>\"+ (item.time) +\"<a href='#'><i class='icon-pin-alt ml-2 text-muted'></i></a></div></div><div class='ml-3'></div></li>\"\n            }\n        });\n    });\n    return listVal;\n});";
},"useData":true});
})();
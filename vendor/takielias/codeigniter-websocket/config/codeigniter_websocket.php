<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Author: takielias
 * Github Repo : https://github.com/takielias/codeigniter-websocket
 * Date: 04/05/2019
 * Time: 09:04 PM
 */
$config['codeigniter_websocket'] = array(
    'host' => '127.0.0.1',
    'port' => 8080,
    'timer_enabled' => false,
    'timer_interval' => 1, //1 means 1 seconds
    'auth' => true,
    'debug' => true
);

<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the social_history table.
	*/ 

class Social_history extends Crud {

protected static $tablename = "Social_history"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('general_diet','sexual_orientation','hard_drug','excercise','stress_factor','language');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = '';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','occupation' => 'text','education_background' => 'text','birthplace' => 'varchar','marital_status' => 'varchar','children_birthed' => 'text','religion' => 'varchar','general_diet' => 'text','sexual_orientation' => 'enum','smoking' => 'tinyint','alcohol_intake' => 'tinyint','hard_drug' => 'text','excercise' => 'text','stress_factor' => 'varchar','language' => 'varchar','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','occupation' => '','education_background' => '','birthplace' => '','marital_status' => '','children_birthed' => '','religion' => '','general_diet' => '','sexual_orientation' => '','smoking' => '','alcohol_intake' => '','hard_drug' => '','excercise' => '','stress_factor' => '','language' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
);
static $tableAction = array('delete' => 'delete/social_history', 'edit' => 'edit/social_history');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient Id</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getOccupationFormField($value = ''){
	return "<div class='row'><div class='col-lg-6'><div class='form-group'>
				<label for='occupation'>Occupation</label>
				<input type='text' name='occupation' id='occupation' value='$value' class='form-control' required placeholder='e.g. Accountant, Engineer etc...'/>
			</div>";
} 
 function getEducation_backgroundFormField($value = ''){
	return "<div class='form-group'>
				<label for='education_background'>Education Background</label>
				<input type='text' name='education_background' id='education_background' value='$value' class='form-control' required placeholder='e.g. Bsc,MSc, phD'/>
			</div>";
} 
 function getBirthplaceFormField($value = ''){
	return "<div class='form-group'>
				<label for='birthplace'>Birthplace</label>
				<input type='text' name='birthplace' id='birthplace' value='$value' class='form-control' required placeholder='e.g. City: Ibadan, State: Oyo, Country: Nigeria'/>
			</div>";
} 
 function getMarital_statusFormField($value = ''){
	$arr =array('Single','Married','Divorced','Widowed','Others');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='gender' >Marital Status</label>
		<select  name='marital_status' id='marital_status'  class='custom-select' required>
		$option
		</select>
</div> ";
} 
 function getChildren_birthedFormField($value = ''){
	return "<div class='form-group'>
				<label for='children_birthed'>Children</label>
				<input type='text' name='children_birthed' id='children_birthed' value='$value' class='form-control' required placeholder='e.g. No child, two boys and a girl'/>
			</div>";
} 
 function getReligionFormField($value = ''){
	return "<div class='form-group'>
				<label for='religion'>Religion</label>
				<input type='text' name='religion' id='religion' value='$value' class='form-control' required placeholder='e.g. Christain, Islam, Hinduism, Atheist,'/>
			</div>";
} 
 function getGeneral_dietFormField($value = ''){
	return "<div class='form-group'>
				<label for='general_diet'>General Diet</label>
				<input type='text' name='general_diet' id='general_diet' value='$value' class='form-control' placeholder='e.g. combination of protein and carbohydrate' />
			</div></div>";
} 
 function getSexual_orientationFormField($value = ''){
	$arr =array('heterosextual','homosextual','bisextual');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='col-lg-6'><div class='form-group'>
	<label for='sexual_orientation' >Sexual Orientation</label>
		<select  name='sexual_orientation' id='sexual_orientation'  class='custom-select'>
		$option
		</select>
</div> ";
} 
 function getSmokingFormField($value = ''){
	$arr =array('yes','no');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='smoking'>Smoking</label>
		<select  name='smoking' id='smoking'  class='custom-select' required >
		$option
		</select>
</div> ";
} 
 function getAlcohol_intakeFormField($value = ''){
	$arr =array('yes','no');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='alcohol_intake'>Alcohol Intake</label>
		<select  name='alcohol_intake' id='alcohol_intake'  class='custom-select' required >
		$option
		</select>
</div> ";
} 
 function getHard_drugFormField($value = ''){
	return "<div class='form-group'>
				<label for='hard_drug'>Hard Drug</label>
				<input type='text' name='hard_drug' id='hard_drug' value='$value' class='form-control'  placeholder='e.g. Cocaine, Heroin etc...'/>
			</div>";
} 
 function getExcerciseFormField($value = ''){
	return "<div class='form-group'>
				<label for='excercise'>Excercise</label>
				<input type='text' name='excercise' id='excercise' value='$value' class='form-control' placeholder='e.g. Moderate physical activity etc...' />
			</div>";
} 
 function getStress_factorFormField($value = ''){
	return "<div class='form-group'>
				<label for='stress_factor'>Stress Factor</label>
				<input type='text' name='stress_factor' id='stress_factor' value='$value' class='form-control' placeholder='e.g. Greatly stressed, relaxed' />
			</div>";
} 
 function getLanguageFormField($value = ''){
	return "<div class='form-group'>
				<label for='language'>Language</label>
				<input type='text' name='language' id='language' value='$value' class='form-control' placeholder='e.g. English, Yoruba, Igbo, Hausa' />
			</div></div></div>";
}  
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}

 
}

?>

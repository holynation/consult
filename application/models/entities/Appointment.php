<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the appointment table.
	*/ 

class Appointment extends Crud {

protected static $tablename = "Appointment"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('appoint_time','appoint_reason','date_created','meetup_location','appoint_status');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = '';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','doctor_id' => 'int','health_services_id' => 'int','meetup_location' => 'varchar','appoint_date' => 'date','appoint_time' => 'time','appoint_reason' => 'text','appoint_status'=>'enum','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','doctor_id' => '','health_services_id' => '','meetup_location' => '','appoint_date' => '','appoint_time' => '','appoint_reason' => '','appoint_status'=>'','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('date_created' => 'current_timestamp()','appoint_status'=>'pending');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
,'doctor' => array('doctor_id','id')
,'health_services' => array('health_services_id','id')
);
static $tableAction = array('delete' => 'delete/appointment', 'edit' => 'edit/appointment');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getDoctor_idFormField($value = ''){
	$where = ($value != '') ? " where ID= '$value' " : "";

	$query = "select id,concat_ws(' ','Dr. ',firstname,' ',lastname ) as value from doctor $where";
	$result ="<div class='form-group'>
		<label for='doctor_id'>Doctor </label>";
		$option = buildOptionFromQuery($this->db,$query,null,$value);
		//load the value from the given table given the name of the table to load and the display field
		$result.="<select name='doctor_id' id='doctor_id' class='form-control border-teal border-1' required>
				<option value=''>..choose a doctor....</option>
					$option
				</select>";
	$result.="</div>";
	return $result;
}
 function getHealth_services_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'health_servicea','display'=>'health_services_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'health_service_name' as value from 'health_service' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('health_service', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='health_services_id' id='health_services_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='health_services_id'>Health Services</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='health_services_id' id='health_services_id' class='form-control border-teal border-1'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getMeetup_locationFormField($value = ''){
	return "<div class='form-group'>
				<input type='hidden' name='meetup_location' id='meetup_location' value='$value' class='form-control border-teal border-1'  />
			</div>";
} 
 function getAppoint_dateFormField($value = ''){
	return "<div class='form-group mb-3'>
			<label for='appoint_date'>Appointment Date</label><div class='input-group mb-2'>
				<span class='input-group-prepend'>
					<span class='input-group-text'><i class='icon-calendar5'></i></span>
				</span>
				<input type='text' name='appoint_date' id='appoint_date' value='$value' class='form-control border-teal border-1 pickadate-appoint autoload' required data-load='checkDoctorSchedule' data-depend='doctor_id' data-child='appointDateHelper'/>
			</div>
			<span class='badge bg-teal font-italic' style='font-size:13.5px;' id='appointDateHelper'></span></div>";
} 
 function getAppoint_timeFormField($value = ''){
	return "<div class='form-group mb-3'>
			<label for='appoint_time'>Appointment Time</label>
			<div class='input-group mb-2'>
				<span class='input-group-prepend'>
					<span class='input-group-text'><i class='icon-alarm'></i></span>
				</span>
				<input type='text' name='appoint_time' id='appoint_time' value='$value' class='form-control border-teal border-1 pickatime-time autoload' data-load='checkAppointTime' data-child='appointTimeHelper' data-depend='doctor_id,appoint_date' required />
				
				<span class='text-muted mt-2 ml-1 mt-lg-0 align-self-center'><b>(WAT)</b></span>
			</div>
			<span class='badge bg-teal font-italic' style='font-size:13.5px;' id='appointTimeHelper'></span></div>";
} 
 function getAppoint_reasonFormField($value = ''){
	return "<div class='form-group'>
				<label for='appoint_reason' class='ft-4'>Appointment Reason</label>
				<textarea name='appoint_reason' id='appoint_reason' class='form-control border-teal border-1' placeholder='e.g. General body checkup with healthcare provider.'>$value</textarea>
			</div>";
}
 function getAppoint_statusFormField($value = ''){
 	$userType = $this->webSessionManager->getCurrentUserProp('user_type');
	$arr = ($userType != 'patient') ? array('active','pending','cancelled','success') : array('pending','cancelled');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='appoint_status' >Appointment Status</label>
		<select name='appoint_status' id='appoint_status' class='form-control'>
		$option
		</select>
</div> ";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['patient_id'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}
 protected function getDoctor(){
	$query ='SELECT * FROM doctor WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['doctor_id'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Doctor.php');
	$resultObject = new Doctor($result[0]);
	return $resultObject;
}
 protected function getHealth_services(){
	$query ='SELECT * FROM health_services WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['health_services_id'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Health_services.php');
	$resultObject = new Health_services($result[0]);
	return $resultObject;
}

 
}

?>

<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the health_condition table.
	*/ 

class Health_condition extends Crud {

protected static $tablename = "Health_condition"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('health_name','diagnosed_period','medication_id','note','date_created');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'health_name';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','health_name' => 'varchar','diagnosed_period' => 'date','condition_status' => 'enum','treated_by' => 'varchar','medication_id' => 'int','note' => 'text','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','health_name' => 'Health Condition','diagnosed_period' =>'Diagnosed in','condition_status' => '','treated_by' => '','medication_id' => '','note' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('condition_status' => 'ongoing','date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
,'medication' => array('medication_id','id')
);
static $tableAction = array('delete' => 'delete/health_condition', 'edit' => 'edit/health_condition');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient Id</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getHealth_nameFormField($value = ''){
	return "<div class='form-group'>
				<label for='health_name'>Name</label>
				<input type='text' name='health_name' id='health_name' value='$value' class='form-control border-teal border-1' required />
			</div>";
} 
 function getDiagnosed_periodFormField($value = ''){
if($value != ''){
 	return "<div class='form-group'>
				<label for='diagnosed_period'>Diagnosed On</label>
				<input type='text' name='diagnosed_period' id='diagnosed_period' value='$value' class='form-control' required />
			</div>";
 }else{
return "<label for='diagnosed_period'>Diagnosed On</label>
		<div class='row'>
			<div class='col-md-4'>
				<div class='form-group'>
				<select name='health_year' id='health_year' class='custom-select' required>".getDropDownYear()."</select>
				</div>	
			</div>
			<div class='col-md-4'>
			<select name='health_month' id='health_month' class='custom-select' required>".getDropDownMonth()."</select>
			</div>
			<div class='col-md-4'>
			<select name='health_day' id='health_day' class='custom-select' required>".getDropDays()."</select>
			</div>
		</div>";
	}
} 
 function getCondition_statusFormField($value = ''){
	$arr =array('ongoing','past');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='condition_status' >Condition Status</label>
		<select  name='condition_status' id='condition_status'  class='form-control'  >
		$option
		</select>
</div> ";
} 
 function getTreated_byFormField($value = ''){
	return "<div class='form-group'>
				<label for='treated_by'>Treated By</label>
				<input type='text' name='treated_by' id='treated_by' value='$value' class='form-control' required />
			</div>";
} 
 function getMedication_idFormField($value = ''){
	$fk = array('table'=>'medication','display'=>'medicine_name');; 
 	//change the value of this variable to array('table'=>'medication','display'=>'medication_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'medication_name' as value from 'medication' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('medication', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='medication_id' id='medication_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			if($value != ''){
				return "<div class='form-group'>
			<label for='medication_id'>Medication</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='medication_id' id='medication_id' class='form-control'>
						$option
					</select>";
				$result.="</div>";
			}else{
			$result ="<div class='row'>
			<div class='col-md-6'>
			<div class='form-group'>
			<label for='medication_id'>Medication</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='medication_id' id='medication_id' class='form-control'>
						$option
					</select>";
			$result.="</div></div>
			<div class='col-md-6'>
				<div class='form-group'>
					<label></label>
					<a href='#' class='btn btn-info waves-effect width-md mt-1' data-toggle='modal' data-target='#myModal_medication' id='modal_medication'> Add New Medication</a>
				</div>
			</div></div>";
			return $result;} 
		}
		
}
 function getNoteFormField($value = ''){
	return "<div class='form-group'>
				<label for='note'>Note</label>
				<textarea name='note' id='note' class='form-control'>$value</textarea>
			</div>";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}
 protected function getMedication(){
	$query ='SELECT * FROM medication WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Medication.php');
	$resultObject = new Medication($result[0]);
	return $resultObject;
}

 
}

?>

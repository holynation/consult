<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the health_year table.
	*/ 

class Health_year extends Crud {

protected static $tablename = "Health_year"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('status');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'year_name';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array('year_name');
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('year_name' => 'varchar','status' => 'tinyint');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','year_name' => '','status' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('status' => '0');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array();
static $tableAction=array('enable'=>'getEnabled','delete'=>'delete/health_year','edit'=>'edit/health_year');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getYear_nameFormField($value = ''){
	return "<div class='form-group'>
				<label for='year_name'>Year Name</label>
				<input type='text' name='year_name' id='year_name' value='$value' class='form-control' required />
			</div>";
} 
 function getStatusFormField($value = ''){
	return "<div class='form-group'>
	<label class='form-checkbox'>Status</label>
	<select class='form-control' id='status' name='status' >
		<option value='1' selected='selected'>Yes</option>
		<option value='0'>No</option>
	</select>
	</div> ";
} 

public function enable($id=null,&$db=null)
{
	//disable all to enable another one, one session must be active at  a time
	if ($id==NULL && !isset($this->array['ID'])) {
				throw new Exception("object does not have id");
			}
			if ($id ==NULL) {
				$id = $this->array["ID"];
			}
			$db=$this->db;
			$db->trans_begin();
			$query="update health_year set status=0";
			if (!$db->query($query)) {
				$db->trans_rollback();
				return false;
			}
			return $this->setEnabled($id,1,$db);
}

 
}

?>

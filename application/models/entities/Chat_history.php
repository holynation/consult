<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the chat_history table.
	*/ 

class Chat_history extends Crud {

protected static $tablename = "Chat_history"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('date_created');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = '';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','doctor_id' => 'int','message' => 'text','message_side' => 'tinyint','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','doctor_id' => '','message' => '','message_side' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('message_side' => '1','date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
,'doctor' => array('doctor_id','id')
);
static $tableAction = array('delete' => 'delete/chat_history', 'edit' => 'edit/chat_history');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
					$result.="</div>";
		return $result;
		}
		
}
 function getDoctor_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'doctor','display'=>'doctor_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'doctor_name' as value from 'doctor' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('doctor', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='doctor_id' id='doctor_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='doctor_id'>Doctor</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='doctor_id' id='doctor_id' class='form-control'>
						$option
					</select>";
					$result.="</div>";
		return $result;
		}
		
}
 function getMessageFormField($value = ''){
	return "<div class='form-group'>
				<label for='message'>Message</label>
				<input type='text' name='message' id='message' value='$value' class='form-control' required />
			</div>";
} 
 function getMessage_sideFormField($value = ''){
	return "<div class='form-group'>
				<label for='message_side'>Message Side</label>
				<input type='text' name='message_side' id='message_side' value='$value' class='form-control' required />
			</div>";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}
 protected function getDoctor(){
	$query ='SELECT * FROM doctor WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Doctor.php');
	$resultObject = new Doctor($result[0]);
	return $resultObject;
}

public function getChatHistory($patient,$doctor,$user=null){
	$condition="";
	// if($user == 'patient'){
	// 	$condition = "if(message_side = 1, 'true','false')";
	// }else if($user == 'doctor'){
	// 	$condition = "if(message_side = 0, 'true','false')";
	// }
	$query = "SELECT message as text,date_format(date_created, '%h:%i %p') as time,cast(date_created as date) as date_created, message_side from chat_history where patient_id = ? and doctor_id = ? order by date_created asc";
	$result = $this->query($query, array($patient,$doctor));
	if(!$result){
		return false;
	}
	// print_r($result);exit;
	return $result;
}

 
}

?>

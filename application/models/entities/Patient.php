<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the patient table.
	*/ 

class Patient extends Crud {

protected static $tablename = "Patient"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('middlename','date_created','status');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = array('firstname','lastname');// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array('phone_num','email');
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_key' => 'varchar','title_id' => 'varchar','firstname' => 'varchar','middlename' => 'varchar','lastname' => 'varchar','gender' => 'varchar','dob' => 'date','phone_num' => 'varchar','email' => 'varchar','address' => 'text','city_address' => 'text','state' => 'varchar','lga' => 'varchar','occupation' => 'varchar','marital_status' => 'varchar','patient_path' => 'varchar','status' => 'tinyint','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_key' => '','title_id' => '','firstname' => '','middlename' => '','lastname' => '','gender' => '','dob' => '','phone_num' => '','email' => '','address' => '','city_address' => '','state' => '','lga' => '','occupation' => '','marital_status' => '','patient_path' => '','status' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('date_created' => 'current_timestamp()','status' => '1');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array('patient_path'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'patient/','preserve'=>false,'max_width'=>'500','max_height'=>'500')); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('title' => array('title_id','id')
);
static $tableAction = array('enable'=>'getEnabled','delete' =>array('delete/patient','patient_path'), 'edit' => 'edit/patient');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_keyFormField($value = ''){
	$value = ($value != '') ? $value : $phashKey;
	return "<div class='form-group' style='display:none;'>
				<label for='patient_key'>Patient Key</label>
				<input type='hidden' name='patient_key' id='patient_key' value='$value' class='form-control' required />
			</div>";
} 
 function getTitle_idFormField($value = ''){
	$fk = array('table'=>'title','display'=>'title_name');
 	//change the value of this variable to array('table'=>'title','display'=>'title_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'title_name' as value from 'title' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('title', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='title_id' id='title_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='title_id'>Title</label>";
			$option = $this->loadOption($fk,$value,'','order by title_name asc');
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='title_id' id='title_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getFirstnameFormField($value = ''){
	return "<div class='form-group'>
				<label for='firstname'>Firstname</label>
				<input type='text' name='firstname' id='firstname' value='$value' class='form-control' required />
			</div>";
} 
 function getMiddlenameFormField($value = ''){
	return "<div class='form-group'>
				<label for='middlename'>Middlename</label>
				<input type='text' name='middlename' id='middlename' value='$value' class='form-control' />
			</div>";
} 
 function getLastnameFormField($value = ''){
	return "<div class='form-group'>
				<label for='lastname'>Lastname</label>
				<input type='text' name='lastname' id='lastname' value='$value' class='form-control' required />
			</div>";
} 
 function getGenderFormField($value = ''){
	$arr =array('Male','Female','Other');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='gender' >Gender</label>
		<select name='gender' id='gender' class='form-control' required>
		$option
		</select>
</div> ";
} 
 function getDobFormField($value = ''){
	return "<div class='form-group'>
				<label for='dob'>Dob</label>
				<input type='text' name='dob' id='dob' value='$value' class='form-control' required data-toggle='input-mask' data-mask='0000-00-00' placeholder='e.g (9999-12-01)'/>
			</div>";
} 
 function getPhone_numFormField($value = ''){
	return "<div class='form-group'>
				<label for='phone_num'>Phone Num</label>
				<input type='text' name='phone_num' id='phone_num' value='$value' class='form-control' required data-toggle='input-mask' data-mask='0000-000-0000' placeholder='e.g (0000-000-0000)'/>
			</div>";
} 
 function getEmailFormField($value = ''){
	return "<div class='form-group'>
				<label for='email'>Email</label>
				<input type='email' name='email' id='email' value='$value' class='form-control' required />
			</div>";
} 
 function getAddressFormField($value = ''){
	return "<div class='form-group'>
				<label for='address'>Address</label>
				<textarea name='address' id='address' class='form-control' required>$value</textarea>
			</div>";
} 
 function getCity_addressFormField($value = ''){
	return "<div class='form-group'>
				<label for='city_address'>City Address</label>
				<input type='text' name='city_address' id='city_address' value='$value' class='form-control' required />
			</div>";
} 
 function getStateFormField($value = ''){
	$states = loadStates();
	$option = buildOptionUnassoc($states,$value);
	return "<div class='form-group'>
	<label for='state' >State Of Origin</label>
		<select  name='state' id='state' value='$value' class='form-control autoload' data-child='lga' data-load='lga'> 
		<option value=''>..select state...</option>
		$option
		</select>
</div> ";
} 
 function getLgaFormField($value = ''){
	$option='';
	if ($value) {
		$arr=array($value);
		$option = buildOptionUnassoc($arr,$value);
	}
	return "<div class='form-group'>
	<label for='lga' >Lga Of Origin</label>
		<select type='text' name='lga' id='lga' value='$value' class='form-control'  >
		<option value=''></option>
		$option
		</select>
</div> ";
} 
 function getOccupationFormField($value = ''){
	return "<div class='form-group'>
				<label for='occupation'>Occupation</label>
				<input type='text' name='occupation' id='occupation' value='$value' class='form-control' />
			</div>";
} 
 function getMarital_statusFormField($value = ''){
	$arr =array('Single','Married','Divorced','Widowed','Others');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='gender' >Marital Status</label>
		<select  name='marital_status' id='marital_status'  class='form-control'  >
		$option
		</select>
</div> ";
} 
 function getPatient_pathFormField($value = ''){
	$path=  ($value != '') ? base_url($value) : "";
	return "<div class='form-group'>
				<label for='patient_path'>Patient Image</label>
				<img src='$path' alt='patient pic' class='img-responsive' width='25%'/>
				<input type='file' name='patient_path' id='patient_path' value='$value' class='form-control' required />
			</div>";
} 
 function getStatusFormField($value = ''){
	return "<div class='form-group'>
	<label class='form-checkbox'>Status</label>
	<select class='form-control' id='status' name='status' >
		<option value='1' selected='selected'>Yes</option>
		<option value='0'>No</option>
	</select>
	</div> ";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getTitle(){
	$query ='SELECT * FROM title WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Title.php');
	$resultObject = new Title($result[0]);
	return $resultObject;
}
public function delete($id=null,&$db=null)
{
	$db=$this->db;
	$db->trans_begin();
	if(parent::delete($id,$db)){
		$query="delete from user where user_table_id=? and user_type='patient'";
		if($this->query($query,array($id))){
			$db->trans_commit();
			return true;
		}
		else{
			$db->trans_rollback();
			return false;
		}
	}
	else{
		$db->trans_rollback();
		return false;
	}
}

public function getJournals(){
	$query = "SELECT * from health_journal where patient_id = ? order by ID desc limit 20";
	$result = $this->query($query, array($this->ID));
	if(!$result){
		return false;
	}
	return $result;
}

public function getAppoints(){
	$query = "SELECT app.ID,app.appoint_date,app.appoint_time,app.appoint_status,health.name,health.price,concat_ws('',doc.firstname,' ',doc.lastname,' ',doc.middlename) as doc_name,doc.doctor_path,doc.ID as doc_id from appointment app join health_services health on app.health_services_id = health.ID join doctor doc on doc.ID = app.doctor_id where patient_id = ? order by ID desc limit 20";
	$result = $this->query($query, array($this->ID));
	if(!$result){
		return false;
	}
	return $result;
}

 
}

?>

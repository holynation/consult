<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the allergy table.
	*/ 

class Allergy extends Crud {

protected static $tablename = "Allergy"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('occurrence','diagnosed_date','addtional_note','date_created');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'triggered_by';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','name' => 'text','triggered_by' => 'text','reaction' => 'text','occurrence' => 'text','diagnosed_date' => 'datetime','medication_id' => 'int','addtional_note' => 'text','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','name' => 'allergy','triggered_by' => '','reaction' => '','occurrence' => '','diagnosed_date' => 'First Noticed','medication_id' => 'medications','addtional_note' => 'notes','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
,'medication' => array('medication_id','id')
);
static $tableAction = array('delete' => 'delete/allergy', 'edit' => 'edit/allergy');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient Id</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getNameFormField($value = ''){
	return "<div class='form-group'>
				<label for='name'>Name</label>
				<input type='text' name='name' id='name' value='$value' class='form-control' required placeholder='e.g. e.g. Allergic Rhinitis , Latex allergy etc...'/>
			</div>";
} 
 function getTriggered_byFormField($value = ''){
	return "<div class='form-group'>
				<label for='triggered_by'>Triggered By</label>
				<input type='text' name='triggered_by' id='triggered_by' value='$value' class='form-control' required placeholder='e.g. Pollens, Pet Hair , Balloons , Dust mites, Gloves etc...'/>
			</div>";
} 
 function getReactionFormField($value = ''){
	return "<div class='form-group'>
				<label for='reaction'>Reaction</label>
				<input type='text' name='reaction' id='reaction' value='$value' class='form-control' required placeholder='Reaction'/>
			</div>";
} 
 function getOccurrenceFormField($value = ''){
	return "<div class='form-group'>
				<label for='occurrence'>How often does it occur</label>
				<input type='text' name='occurrence' id='occurrence' value='$value' class='form-control' placeholder='e.g.Only during summer season , Only during out open door activity etc...' />
			</div>";
} 
 function getDiagnosed_dateFormField($value = ''){
	return "<div class='form-group'>
				<label for='diagnosed_date'>First diagnosed on</label>
				<div class='input-group'>
					<span class='input-group-prepend'>
						<span class='input-group-text'><i class='icon-calendar5'></i></span>
					</span>
					<input type='text' class='form-control' name='diagnosed_date' id='diagnosed_date' data-provide='datepicker' data-date-format='yyyy-mm-dd' autocomplete='off' value='$value'>
				</div>
			</div>";
} 
 function getMedication_idFormField($value = ''){
	$fk = array('table'=>'medication','display'=>'medicine_name');; 
 	//change the value of this variable to array('table'=>'medication','display'=>'medication_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'medication_name' as value from 'medication' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('medication', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='medication_id' id='medication_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='medication_id'>Medication</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='medication_id' id='medication_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getAddtional_noteFormField($value = ''){
	return "<div class='form-group'>
				<label for='addtional_note'>Addtional Notes</label>
				<textarea name='addtional_note' id='addtional_note' class='form-control' placeholder='e.g Carry an epinephrine auto injector for emergency treatment , Keep windows closed during high pollen periods.'>$value</textarea>
			</div>";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}
 protected function getMedication(){
	$query ='SELECT * FROM medication WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Medication.php');
	$resultObject = new Medication($result[0]);
	return $resultObject;
}

 
}

?>

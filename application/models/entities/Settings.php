<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the settings table.
	*/ 

class Settings extends Crud {

protected static $tablename = "Settings"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('blog_alert','health_journal','sms_alert','date_created','vital_tracker');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'role_title';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','vital_tracker' => 'tinyint','blog_alert' => 'tinyint','health_journal' => 'tinyint','sms_alert' => 'tinyint','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','vital_tracker' => '','blog_alert' => '','health_journal' => '','sms_alert' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('vital_tracker' => '1','blog_alert' => '1','health_journal' => '0','sms_alert' => '1','date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
);
static $tableAction = array('delete' => 'delete/settings', 'edit' => 'edit/settings');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient Id</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getVital_trackerFormField($value = ''){
	return "<div class='form-group'>
				<label for='vital_tracker'>Vital Tracker</label>
				<input type='text' name='vital_tracker' id='vital_tracker' value='$value' class='form-control' required />
			</div>";
} 
 function getBlog_alertFormField($value = ''){
	return "<div class='form-group'>
				<label for='blog_alert'>Blog Alert</label>
				<input type='text' name='blog_alert' id='blog_alert' value='$value' class='form-control' required />
			</div>";
} 
 function getHealth_journalFormField($value = ''){
	return "<div class='form-group'>
	<label class='form-checkbox'>Journal Privacy</label>
	<select class='form-control' id='status' name='status' >
		<option value='1'>Make It Public</option>
		<option value='0' selected='selected'>No, don't</option>
	</select>
	</div> ";
} 
 function getSms_alertFormField($value = ''){
	return "<div class='form-group'>
				<label for='sms_alert'>Sms Alert</label>
				<input type='text' name='sms_alert' id='sms_alert' value='$value' class='form-control' required />
			</div>";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}

 
}

?>

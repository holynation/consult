<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the text_consultation table.
	*/ 

class Text_consultation extends Crud {

protected static $tablename = "Text_consultation"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('consult_question','age','gender','text_consultation_path','date_created','user_type');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'consult_question';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','age' => 'varchar','gender' => 'varchar','user_type'=>'enum','consult_question' => 'longtext','doctor_id' => 'int','department_id' => 'int','text_consultation_path' => 'varchar','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','consult_question' => '','age' => '','gender' => '','user_type'=>'','doctor_id' => '','department_id' => '','text_consultation_path' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('date_created' => 'current_timestamp()','user_type' => 'personal');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array('text_consultation_path'=>array('type'=>array('jpeg','png','pdf','doc','txt'),'size'=>'5242880','directory'=>'text_consultation/','preserve'=>false)); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
,'doctor' => array('doctor_id','id')
,'department' => array('department_id','id')
);
static $tableAction = array('delete' => 'delete/text_consultation', 'edit' => 'edit/text_consultation');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient Id</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getConsult_questionFormField($value = ''){
	return "<div class='form-group'>
				<label for='consult_question'>Consultation Question</label>
				<textarea name='consult_question' id='consult_question' class='form-control' required>$value</textarea>
			</div>";
} 
 function getAgeFormField($value = ''){
	return "<div id='someoneElse' style='display:none;'><div class='form-group'>
				<label for='age'>Age</label>
				<input type='number' name='age' id='age' value='$value' class='form-control' min='0' max='120'/>
			</div>";
} 
 function getGenderFormField($value = ''){
	$arr =array('Male','Female','Other');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='gender' >Gender</label>
		<select name='gender' id='gender' class='form-control'>
		<option value=''>..select gender....</option>
		$option
		</select>
</div> 
</div>";
}
function getUser_typeFormField($value=''){
	return "";
} 
 function getDoctor_idFormField($value = ''){
	 $where = ($value != '') ? " where ID= '$value' " : "";

	$query = "select id,concat_ws(' ','Dr. ',firstname,' ',lastname ) as value from doctor $where";
	$result ="<div class='form-group'>
		<label for='doctor_id'>Doctor </label>";
		$option = buildOptionFromQuery($this->db,$query,null,$value);
		//load the value from the given table given the name of the table to load and the display field
		$result.="<select name='doctor_id' id='doctor_id' class='form-control' required>
				<option value=''>..choose a doctor....</option>
					$option
				</select>";
	$result.="</div>";
	return $result;
}
 function getDepartment_idFormField($value = ''){
	$fk=array('table'=>'department','display'=>'department_name'); 
 	//change the value of this variable to array('table'=>'department','display'=>'department_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'department_name' as value from 'department' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('department', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='department_id' id='department_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='department_id'>Department</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='department_id' id='department_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getText_consultation_pathFormField($value = ''){
	return "<div class='form-group row'>
				<div class='col-lg-10'>
					<div class='form-group'>
				<input type='file' class='file-input' data-show-caption='false' data-show-upload='false' data-fouc name='text_consultation_path' id='text_consultation_path' />
				<span class='form-text text-muted'>File size should not be more than 5 MB. Supported formats: <code>txt, doc, pdf, jpeg.</code></span></div></div>
			</div>";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}
 protected function getDoctor(){
	$query ='SELECT * FROM doctor WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Doctor.php');
	$resultObject = new Doctor($result[0]);
	return $resultObject;
}
 protected function getDepartment(){
	$query ='SELECT * FROM department WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Department.php');
	$resultObject = new Department($result[0]);
	return $resultObject;
}

 
}

?>

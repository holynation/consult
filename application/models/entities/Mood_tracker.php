<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the mood_tracker table.
	*/ 

class Mood_tracker extends Crud {

protected static $tablename = "Mood_tracker"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('note');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'note';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','date_created' => 'timestamp','mood_status' => 'varchar','note' => 'text');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','mood_status' => '','note' => '','date_created' => 'Recorded On');
/*associative array of fields that have default value*/ 
static $defaultArray = array('date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
);
static $tableAction = array('delete' => 'delete/mood_tracker', 'edit' => 'edit/mood_tracker');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient Id</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getMood_statusFormField($value = ''){
 	if($value != ''){
 		return "<div class='form-group'>
				<label for='mood_status'>Mood Status</label>
				<input type='text' class='form-control ion-pips-height-helper' id='mood_status' name='mood_status' value='$value'>
			</div>";
 	}else{
	return "<div class='form-group'>
				<label for='mood_status'>Mood Status</label>
				<input type='text' class='form-control ion-pips-height-helper' id='mood_status' name='mood_status' data-fouc>
			</div>";
		}
} 
 function getNoteFormField($value = ''){
	return "<div class='form-group'>
				<label for='note'>Note</label>
				<textarea name='note' id='note' class='form-control' placeholder='Great! What is the cause for your good mood today?'>$value</textarea>
			</div>";
} 
 function getDate_createdFormField($value = ''){
	if($value != ''){
 		return "<div class='form-group'>
			<label>Date Taken</label>
			<div class='input-group'>
				<span class='input-group-prepend'>
					<span class='input-group-text'><i class='icon-calendar5'></i></span>
				</span>
				<input type='text' class='form-control' name='date_created' id='date_created' data-provide='datepicker' data-date-format='yyyy-mm-dd' autocomplete='off' value='$value'>
			</div>
		</div>";
 	}else{
 		// $timeValue = date('G:i a');
 		$dateValue = date('Y-m-d');
		return "<div class='row'>
		<div class='col-sm-6'>
			<div class='form-group'>
				<label>Date Taken</label>
				<div class='input-group'>
					<span class='input-group-prepend'>
						<span class='input-group-text'><i class='icon-calendar5'></i></span>
					</span>
					<input type='text' class='form-control' name='mt_date' id='mt_date' data-provide='datepicker' data-date-format='yyyy-mm-dd' autocomplete='off' value='$dateValue'>
				</div>
			</div>
		</div>
		<div class='col-sm-6'>
			<div class='form-group'>
				<label>Around</label>
				<div class='input-group'>
					<span class='input-group-prepend'>
						<span class='input-group-text'><i class='icon-alarm'></i></span>
					</span>
					<input type='text' name='mt_time' id='mt_time' class='form-control border-teal border-1 bs-time' required placeholder='e.g. 7:30 AM' />
				</div>
			</div>
		</div>
		</div>";
	}
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}

 
}

?>

<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the Health_services table.
	*/ 

class Health_services extends Crud {

protected static $tablename = "Health_services"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('status');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'name';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array('name');
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('name' => 'varchar','price' => 'float','health_year_id'=>'int','status' => 'tinyint');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','name' => '','price' => '','health_year_id'=>'','status' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('status' => '1');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array();
static $tableAction = array('enable'=>'getEnabled','delete' => 'delete/health_services', 'edit' => 'edit/health_services');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getNameFormField($value = ''){
	return "<div class='form-group'>
				<label for='name'>Name</label>
				<input type='text' name='name' id='name' value='$value' class='form-control' required />
			</div>";
} 
 function getPriceFormField($value = ''){
	return "<div class='form-group'>
				<label for='price'>Price</label>
				<input type='number' name='price' id='price' value='$value' class='form-control' required min='1' />
			</div>";
}
function getHealth_year_idFormField($value=''){
	$fk = array('table'=>'health_year','display'=>'year_name'); 
 	//change the value of this variable to array('table'=>'health_year','display'=>'year_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'year_name' as value from 'health_year' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('health_year', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='health_year_id' id='health_year_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='health_year_id'>Health Year</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='health_year_id' id='health_year_id' class='form-control' required>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getStatusFormField($value = ''){
	return "<div class='form-group'>
	<label class='form-checkbox'>Status</label>
	<select class='form-control' id='status' name='status' >
		<option value='1' selected='selected'>Yes</option>
		<option value='0'>No</option>
	</select>
	</div> ";
} 

public function getService($param){
	$sql = "select * from health_services where name like '%$param%' order by ID desc limit 1";
	$result = $this->query($sql);
	if(!$result){
		return false;
	}
	return $result;
}
 
}

?>

<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the medication table.
	*/ 

class Medication extends Crud {

protected static $tablename = "Medication"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('dosage_instructions','end_duration','side_effects','date_created','prescribed_by','diagnosis_id');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = 'medicine_name';// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('patient_id' => 'int','medicine_name' => 'varchar','medicine_form_id' => 'int','strength' => 'int','method_intake_id' => 'int','dosage_quantity' => 'int','dosage_times' => 'varchar','dosage_period' => 'varchar','dosage_instructions' => 'text','start_duration' => 'date','end_duration' => 'date','diagnosis_id' => 'int','prescribed_by' => 'varchar','side_effects' => 'text','date_created' => 'timestamp');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','patient_id' => '','medicine_name' => 'Medication','medicine_form_id' => '','strength' => '','method_intake_id' => '','dosage_quantity' => '','dosage_times' => '','dosage_period' => '','dosage_instructions' => '','start_duration' => '','end_duration' => '','diagnosis_id' => 'Taken for','prescribed_by' => '','side_effects' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array(); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('patient' => array('patient_id','id')
,'medicine_form' => array('medicine_form_id','id')
,'method_intake' => array('method_intake_id','id')
,'diagnosis' => array('diagnosis_id','id')
);
static $tableAction = array('delete' => 'delete/medication', 'edit' => 'edit/medication');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getPatient_idFormField($value = ''){
	$fk = null; 
 	//change the value of this variable to array('table'=>'patient','display'=>'patient_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'patient_name' as value from 'patient' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('patient', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='patient_id' id='patient_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='patient_id'>Patient Id</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='patient_id' id='patient_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getMedicine_nameFormField($value = ''){
	return "<div class='form-group'>
				<label for='medicine_name'>Medicine Name</label>
				<input type='text' name='medicine_name' id='medicine_name' value='$value' class='form-control' placeholder='Enter name of medication' required />
			</div>";
} 
 function getMedicine_form_idFormField($value = ''){
	$fk = array('table'=>'medicine_form','display'=>'name');

		if(is_null($fk)){
			return $result = "<input type='hidden' name='medicine_form_id' id='medicine_form_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			return "<h4>Form of Medicine</h4><div class='row'>
			<div class='col-md-4'>
				<div class='form-group'>
					<label for='medicine_form_id'>Medicine Form</label>
					<select name='medicine_form_id' id='medicine_form_id' class='custom-select' required>
						$option
					</select>
				</div>	
			</div>";
		}

}
 function getStrengthFormField($value = ''){
	return "<div class='col-md-4'>
				<label for='strength'>Strength</label>
				<input type='text' name='strength' id='strength' value='$value' class='form-control' required />
			</div>";
} 
 function getMethod_intake_idFormField($value = ''){
	$fk = array('table'=>'method_intake','display'=>'name');

		if(is_null($fk)){
			return $result = "<input type='hidden' name='method_intake_id' id='method_intake_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='col-md-4'>
			<label for='method_intake_id'>Method Intake</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='method_intake_id' id='method_intake_id' class='custom-select' required>
						$option
					</select>";
			$result.="</div></div>";
			return $result;
		}
		
}
 function getDosage_quantityFormField($value = ''){
	return "<h4>Dosage</h4><div class='row'><div class='col-md-4'><div class='form-group'>
				<label for='dosage_quantity'>Quantity</label>
				<input type='number' name='dosage_quantity' id='dosage_quantity' value='$value' class='form-control' required />
			</div></div>";
} 
 function getDosage_timesFormField($value = ''){
	return "<div class='col-md-4'><div class='form-group'>
				<label for='dosage_times'>How many times</label>
				<input type='text' name='dosage_times' id='dosage_times' value='$value' class='form-control' placeholder='3 times daily' required />
			</div></div>";
} 
 function getDosage_periodFormField($value = ''){
	return "<div class='col-md-4'><div class='form-group'>
				<label for='dosage_period'>When</label>
				<input type='text' name='dosage_period' id='dosage_period' value='$value' class='form-control' placeholder='e.g. Before Food' required />
			</div></div></div>";
} 
 function getDosage_instructionsFormField($value = ''){
	return "<div class='form-group'>
				<label for='dosage_instructions'>Other Instructions</label>
				<textarea name='dosage_instructions' id='dosage_instructions' class='form-control' placeholder='Note to patient e.g. Medicine to be taken 30 min before meal.'></textarea>
			</div>";
} 
 function getStart_durationFormField($value = ''){
	return "<h4>Duration</h4><div class='row'><div class='col-md-4'><div class='form-group'>
				<label for='start_duration'>Start on</label>
				<div class='input-group'>
					<span class='input-group-prepend'>
						<span class='input-group-text'><i class='icon-calendar5'></i></span>
					</span>
					<input type='text' class='form-control' name='start_duration' id='start_duration' data-provide='datepicker' data-date-format='yyyy-mm-dd' autocomplete='off' value='$value'>
				</div>
			</div></div>";
} 
 function getEnd_durationFormField($value = ''){
	return "<div class='col-md-4'><div class='form-group'>
				<label for='end_duration'>Till</label>
				<div class='input-group'>
					<span class='input-group-prepend'>
						<span class='input-group-text'><i class='icon-calendar5'></i></span>
					</span>
					<input type='text' class='form-control' name='end_duration' id='end_duration' data-provide='datepicker' data-date-format='yyyy-mm-dd' autocomplete='off'>
				</div>
			</div></div></div>";
} 
 function getDiagnosis_idFormField($value = ''){
	$fk = array('table'=>'diagnosis','display'=>'name');; 
 	//change the value of this variable to array('table'=>'diagnosis','display'=>'diagnosis_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'diagnosis_name' as value from 'diagnosis' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('diagnosis', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='diagnosis_id' id='diagnosis_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<h4>Additional Details (optional)</h4><div class='form-group'>
			<label for='diagnosis_id'>Medicine Taken For</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='diagnosis_id' id='diagnosis_id' class='form-control' placeholder='e.g. Ulcer'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getPrescribed_byFormField($value = ''){
	return "<div class='form-group'>
				<label for='prescribed_by'>Prescribed By</label>
				<input type='text' name='prescribed_by' id='prescribed_by' value='$value' class='form-control' />
			</div>";
} 
 function getSide_effectsFormField($value = ''){
	return "<div class='form-group'>
				<label for='side_effects'>Side Effects</label>
				<input type='text' name='side_effects' id='side_effects' value='$value' class='form-control' placeholder='e.g. Swelling, body rashes' />
			</div>";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getPatient(){
	$query ='SELECT * FROM patient WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Patient.php');
	$resultObject = new Patient($result[0]);
	return $resultObject;
}
 protected function getMedicine_form(){
	$query ='SELECT * FROM medicine_form WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Medicine_form.php');
	$resultObject = new Medicine_form($result[0]);
	return $resultObject;
}
 protected function getMethod_intake(){
	$query ='SELECT * FROM method_intake WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Method_intake.php');
	$resultObject = new Method_intake($result[0]);
	return $resultObject;
}
 protected function getDiagnosis(){
	$query ='SELECT * FROM diagnosis WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['ID'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Diagnosis.php');
	$resultObject = new Diagnosis($result[0]);
	return $resultObject;
}

 
}

?>

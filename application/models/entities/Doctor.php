<?php 
defined("BASEPATH") OR exit("No direct script access allowed");
	require_once("application/models/Crud.php");

	/**
	* This class  is automatically generated based on the structure of the table. And it represent the model of the doctor table.
	*/ 

class Doctor extends Crud {

protected static $tablename = "Doctor"; 
/* this array contains the field that can be null*/ 
static $nullArray = array('middlename','status','date_created');
static $compositePrimaryKey = array();
static $uploadDependency = array();
/*this array contains the fields that are unique*/ 
static $displayField = array('firstname','lastname');// this display field properties is used as a column in a query if a their is a relationship between this table and another table.In the other table, a field showing the relationship between this name having the name of this table i.e something like this. table_id. We cant have the name like this in the table shown to the user like table_id so the display field is use to replace that table_id.However,the display field name provided must be a column in the table to replace the table_id shown to the user,so that when the other model queries,it will use that field name as a column to be fetched along the query rather than the table_id alone.;
static $uniqueArray = array();
/* this is an associative array containing the fieldname and the type of the field*/ 
static $typeArray = array('doctor_key' => 'varchar','title_id' => 'int','firstname' => 'varchar','middlename' => 'varchar','lastname' => 'varchar','mdcn_num' => 'varchar','dob' => 'varchar','email' => 'varchar','phone_num' => 'varchar','gender' => 'varchar','marital_status' => 'varchar','state' => 'varchar','lga' => 'varchar','address' => 'text','department_id' => 'varchar','specialty_id' => 'int','role_id' => 'varchar','doctor_path' => 'varchar','status' => 'tinyint','date_created' => 'datetime');
/*this is a dictionary that map a field name with the label name that will be shown in a form*/ 
static $labelArray = array('ID' => '','doctor_key' => '','title_id' => '','firstname' => '','middlename' => '','lastname' => '','mdcn_num' => '','dob' => '','gender' => '','marital_status' => '','phone_num' => '','email' => '','state' => '','lga' => '','address' => '','department_id' => '','specialty_id' => '','role_id' => '','doctor_path' => '','status' => '','date_created' => '');
/*associative array of fields that have default value*/ 
static $defaultArray = array('status' => '1','date_created' => 'current_timestamp()');
 // populate this array with fields that are meant to be displayed as document in the format array('fieldname'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'pastDeans/','preserve'=>false,'max_width'=>'1000','max_height'=>'500'))
//the folder to save must represent a path from the basepath. it should be a relative path,preserve filename will be either true or false. when true,the file will be uploaded with it default filename else the system will pick the current user id in the session as the name of the file.
static $documentField = array('doctor_path'=>array('type'=>array('jpeg','jpg','png','gif'),'size'=>'1048576','directory'=>'doctor/','preserve'=>false,'max_width'=>'500','max_height'=>'500')); //array containing an associative array of field that should be regareded as document field. it will contain the setting for max size and data type.;
static $relation = array('title' => array('title_id','id')
,'department' => array('department_id','id')
,'specialty' => array('specialty_id','id')
,'role' => array('role_id','id')
);
static $tableAction = array('enable'=>'getEnabled','delete' => array('delete/doctor','doctor_path'), 'edit' => 'edit/doctor');
function __construct($array = array())
{
	parent::__construct($array);
}
 
function getDoctor_keyFormField($value = ''){
	$value = ($value != '') ? $value : $hashKey;
	return "<div class='form-group' style='display:none;'>
				<label for='doctor_key'>Doctor Key</label>
				<input type='hidden' name='doctor_key' id='doctor_key' value='$value' class='form-control' required />
			</div>";
} 
 function getTitle_idFormField($value = ''){
	$fk = array('table'=>'title','display'=>'title_name');
 	//change the value of this variable to array('table'=>'title','display'=>'title_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'title_name' as value from 'title' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('title', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='title_id' id='title_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='title_id'>Title</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='title_id' id='title_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getFirstnameFormField($value = ''){
	return "<div class='form-group'>
				<label for='firstname'>Firstname</label>
				<input type='text' name='firstname' id='firstname' value='$value' class='form-control' required />
			</div>";
} 
 function getMiddlenameFormField($value = ''){
	return "<div class='form-group'>
				<label for='middlename'>Middlename</label>
				<input type='text' name='middlename' id='middlename' value='$value' class='form-control' />
			</div>";
} 
 function getLastnameFormField($value = ''){
	return "<div class='form-group'>
				<label for='lastname'>Lastname</label>
				<input type='text' name='lastname' id='lastname' value='$value' class='form-control' required />
			</div>";
} 
 function getMdcn_numFormField($value = ''){
	return "<div class='form-group'>
				<label for='mdcn_num'>Mdcn Num</label>
				<input type='text' name='mdcn_num' id='mdcn_num' value='$value' class='form-control' required />
			</div>";
} 
 function getDobFormField($value = ''){
	return '<div class="form-group mb-3">
			<label for="dob">Select Date of birth</label>
				<div class="input-group">
					<span class="input-group-prepend">
						<span class="input-group-text"><i class="icon-calendar5"></i></span>
					</span>
					<input type="text" class="form-control" placeholder="select birth date" name="dob" id="dob" data-provide="datepicker" data-date-format="yyyy-mm-dd" autocomplete="off">
				</div>
			</div>';
} 
 function getGenderFormField($value = ''){
	$arr =array('Male','Female','Other');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='gender' >Gender</label>
		<select name='gender' id='gender' class='form-control' required>
		$option
		</select>
</div> ";
} 
 function getMarital_statusFormField($value = ''){
	$arr =array('Single','Married','Divorced','Widowed','Others');
	$option = buildOptionUnassoc($arr,$value);
	return "<div class='form-group'>
	<label for='gender' >Marital Status</label>
		<select  name='marital_status' id='marital_status'  class='form-control'  >
		$option
		</select>
</div> ";
} 
 function getPhone_numFormField($value = ''){
	return "<div class='form-group'>
				<label for='phone_num'>Phone Num</label>
				<input type='text' name='phone_num' id='phone_num' value='$value' class='form-control' required />
			</div>";
} 
 function getEmailFormField($value = ''){
	return "<div class='form-group'>
				<label for='email'>Email</label>
				<input type='text' name='email' id='email' value='$value' class='form-control' required />
			</div>";
} 
 function getStateFormField($value = ''){
	$states = loadStates();
	$option = buildOptionUnassoc($states,$value);
	return "<div class='form-group'>
	<label for='state' >State Of Origin</label>
		<select  name='state' id='state' value='$value' class='form-control autoload' data-child='lga' data-load='lga'> 
		<option value=''>..select state...</option>
		$option
		</select>
</div> ";
} 
 function getLgaFormField($value = ''){
	$option='';
	if ($value) {
		$arr=array($value);
		$option = buildOptionUnassoc($arr,$value);
	}
	return "<div class='form-group'>
	<label for='lga' >Lga Of Origin</label>
		<select type='text' name='lga' id='lga' value='$value' class='form-control'  >
		<option value=''></option>
		$option
		</select>
</div> ";
}
 function getAddressFormField($value = ''){
	return "<div class='form-group'>
				<label for='address'>Address</label>
				<textarea name='address' id='address' class='form-control'>$value</textarea>
			</div>";
} 
 function getDepartment_idFormField($value = ''){
	$fk=array('table'=>'department','display'=>'department_name'); 
 	//change the value of this variable to array('table'=>'department','display'=>'department_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'department_name' as value from 'department' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('department', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='department_id' id='department_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='department_id'>Department</label>";
			$option = $this->loadOption($fk,$value);
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='department_id' id='department_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getSpecialty_idFormField($value = ''){
	$fk=array('table'=>'specialty','display'=>'name'); 
 	//change the value of this variable to array('table'=>'specialty','display'=>'specialty_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'specialty_name' as value from 'specialty' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('specialty', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

		if(is_null($fk)){
			return $result = "<input type='hidden' name='specialty_id' id='specialty_id' value='$value' class='form-control' />";
		}

		if(is_array($fk)){
			
			$result ="<div class='form-group'>
			<label for='specialty_id'>Specialty</label>";
			$option = $this->loadOption($fk,$value,'','order by specialty.name asc');
			//load the value from the given table given the name of the table to load and the display field
			$result.="<select name='specialty_id' id='specialty_id' class='form-control'>
						$option
					</select>";
		}
		$result.="</div>";
		return $result;
}
 function getRole_idFormField($value = ''){
	$fk= array('table'=>'role','display'=>'role_title');
 	//change the value of this variable to array('table'=>'role','display'=>'role_name'); if you want to preload the value from the database where the display key is the name of the field to use for display in the table.[i.e the display key is a column name in the table specify in that array it means select id,'role_name' as value from 'role' meaning the display name must be a column name in the table model].It is important to note that the table key can be in this format[array('table' => array('role', 'another table name'))] provided that their is a relationship between these tables. The value param in the function is set to true if the form model is used for editing or updating so that the option value can be selected by default;

	if(is_null($fk)){
		return $result = "<input type='hidden' name='role_id' id='role_id' value='$value' class='form-control' />";
	}

	if(is_array($fk)){
		
		$result ="<div class='form-group'>
		<label for='role_id'>Role</label>";
		$option = $this->loadOption($fk,$value,'','where ID <> 1');
		//load the value from the given table given the name of the table to load and the display field
		$result.="<select name='role_id' id='role_id' class='form-control'>
					$option
				</select>";
	}
	$result.="</div>";
	return $result;
}
 function getDoctor_pathFormField($value = ''){
 	$path=  ($value != '') ? base_url($value) : "";
	return "<div class='form-group'>
				<label for='doctor_path'>Doctor Image</label>
				<img src='$path' alt='doctor pic' class='img-responsive' width='25%'/>
				<input type='file' name='doctor_path' id='doctor_path' value='$value' class='form-control' />
			</div>";
} 
 function getStatusFormField($value = ''){
	return "<div class='form-group'>
	<label class='form-checkbox'>Status</label>
	<select class='form-control' id='status' name='status' >
		<option value='1' selected='selected'>Yes</option>
		<option value='0'>No</option>
	</select>
	</div> ";
} 
 function getDate_createdFormField($value = ''){
	return "";
} 

protected function getTitle(){
	$query ='SELECT * FROM title WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['title_id'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Title.php');
	$resultObject = new Title($result[0]);
	return $resultObject;
}
 protected function getDepartment(){
	$query ='SELECT * FROM department WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['department_id'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Department.php');
	$resultObject = new Department($result[0]);
	return $resultObject;
}
 protected function getSpecialty(){
	$query ='SELECT * FROM specialty WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['specialty_id'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Specialty.php');
	$resultObject = new Specialty($result[0]);
	return $resultObject;
}
 protected function getRole(){
	$query ='SELECT * FROM role WHERE id=?';
	if (!isset($this->array['ID'])) {
		return null;
	}
	$id = $this->array['role_id'];
	$result = $this->db->query($query,array($id));
	$result = $result->result_array();
	if (empty($result)) {
		return false;
	}
	include_once('Role.php');
	$resultObject = new Role($result[0]);
	return $resultObject;
}
public function delete($id=null,&$db=null)
{
	$db=$this->db;
	$db->trans_begin();
	if(parent::delete($id,$db)){
		$query="delete from user where user_table_id=? and user_type='doctor'";
		if($this->query($query,array($id))){
			$db->trans_commit();
			return true;
		}
		else{
			$db->trans_rollback();
			return false;
		}
	}
	else{
		$db->trans_rollback();
		return false;
	}
}
// 'active','pending','cancelled','success'
public function getPatientAppoint($patientID=''){
	// this query is to get the recent distinct patient_id in the db
	$extra="";
	$limit = 50;
	$status="";
	if($patientID != ''){
		$limit = 1;
		$extra = " and patient_id = '$patientID'";
		$status=", appoint_status ";
	}
	$query = "SELECT id,patient_id $status from (SELECT patient_id, max(id) id $status from appointment where doctor_id = ? and appoint_status <> 'cancelled' $extra group by patient_id) A order by id desc limit $limit";
	$result = $this->query($query, array($this->ID));
	if(!$result){
		return false;
	}
	if($patientID != ''){
		// print_r($result);exit;
		return $result[0];
	}
	$patientRes = array();
	foreach($result as $value){
		$patientRes[] = $this->getPatientData($value['patient_id']);
	}
	return $patientRes;
}

public function getPatientData($patient_id=null){
	$query = "SELECT id,firstname,lastname,middlename,patient_path from patient where id = ? limit 1";
	$result = $this->query($query, array($patient_id));
	if(!$result){
		return false;
	}
	return $result[0];
}
 
}

?>

<?php 
	/**
	* This class contains  the method for performing extra action performed
	*/
	class ModelControllerCallback extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('webSessionManager');
			$this->load->helper('string');
			$this->load->library('hash_created');
		}

		public function onAdminInserted($data,$type,&$db,&$message)
		{
			//remember to remove the file if an error occured here
			//the user type should be admin
			loadClass($this->load,'user');
			if ($type=='insert') {
				// login details as follow: username = email, password = firstname(in lowercase)
				$password = $this->hash_created->encode_password(strtolower($data['firstname']));
				$param = array('user_type'=>'admin','username'=>$data['email'],'password'=>$password,'user_table_id'=>$data['LAST_INSERT_ID']);
				$std = new User($param);
				if ($std->insert($db,$message)) {
					return true;
				}
				return false;
			}
			return true;
		}

		// this only work when admin is the one registering user
		public function onPatientInserted($data,$type,&$db,&$message)
		{
			//remember to remove the file if an error occured here
			//the user type should be admin
			loadClass($this->load,'user');
			if ($type=='insert') {
				// login details as follow: username = phone number, password = firstname(in lowercase)
				$password = $this->hash_created->encode_password(strtolower($data['firstname']));
				$param = array('user_type'=>'patient','username'=>$data['email'],'password'=>$password,'user_table_id'=>$data['LAST_INSERT_ID']);
				$settingParam = array(
					'patient_id' => $data['LAST_INSERT_ID'],
					'vital_tracker'=> '0',
					'blog_alert' => '1',
					'health_journal' => '0',
					'sms_alert' => '1'
				);
				$std = new User($param);
				$setting = new Settings($settingParam);
				if ($std->insert($db,$message)) {
					if($setting->insert($db,$message)){
						return true;
					}
					return true;
				}
				return false;
			}
			return true;
		}

		public function onDoctorInserted($data,$type,&$db,&$message)
		{
			//remember to remove the file if an error occured here
			//the user type should be admin
			loadClass($this->load,'user');
			if ($type=='insert') {
				// login details as follow: username = email, password = firstname(in lowercase)
				$password = $this->hash_created->encode_password(strtolower($data['firstname']));
				$param = array('user_type'=>'doctor','username'=>$data['email'],'password'=>$password,'user_table_id'=>$data['LAST_INSERT_ID'],'status'=> '0');
				$std = new User($param);
				if ($std->insert($db,$message)) {
					// this is to verify the agent email account
					$mailResponse = ($this->sendUserMail($data['email'],'Verification of account from Online Medical Consultation',1)) ? true : false;
					return $mailResponse;
				}
				return false;
			}
			return true;
		}

		public function onText_consultationInserted($data,$type,&$db,&$message,&$redirect)
		{
			// print_r($data);exit;
			if($type == 'insert'){
				$service = $data['service_id'];
				$serviceFee = $data['service_fee'];
				$curlParam = $this->userParam($service,$serviceFee);
				$result = makeOnlineTransaction($curlParam,$errorMessage);
				if($errorMessage){
					$message = $errorMessage;
					return false;
				}
				if($result){
					$redirect = $result;
					return true;
				}
				
			}
			return true;
		}

		public function onAppointmentInserted($data,$type,&$db,&$message,&$redirect)
		{
			// print_r($data);exit;
			if($type == 'insert'){
				$service = $data['health_services_id'];
				$serviceFee = $data['service_fee'];
				$appointID = $data['LAST_INSERT_ID'];
				$modelParam = "appointment/$appointID";
				$curlParam = $this->userParam($service,$serviceFee,$modelParam);
				$result = makeOnlineTransaction($curlParam,$errorMessage);
				if($errorMessage){
					$message = $errorMessage;
					return false;
				}
				if($result){
					$redirect = $result;
					return true;
				}
				
			}
			return true;
		}

		private function sendUserMail($recipient,$subject,$type){
			$this->load->library('email');
			$emailObj = $this->email;
			return sendMailToRecipient($emailObj,$recipient,$subject,$type);
		}

		private function userParam($service,$serviceFee,$modelParam=null){
			$email = $this->webSessionManager->getCurrentUserProp('email');
			$fullname = $this->webSessionManager->getCurrentUserProp('firstname')." ".$this->webSessionManager->getCurrentUserProp('lastname');
			$phone = $this->webSessionManager->getCurrentUserProp('phone_num');
			$priceInKobo = ($serviceFee * 100);
			$curlParam = initializeTransact($service,$priceInKobo,$email,$phone,$fullname,$modelParam);
			return $curlParam;
		}

			
	}

 ?>
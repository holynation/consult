<?php 

/**
* The controller that validate forms that should be inserted into a table based on the request url.
each method wil have the structure validate[modelname]Data
*/
class ModelControllerDataValidator extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('webSessionManager');
	}

	public function validateText_consultationData(&$data,$type,&$message){
		if($type == 'insert'){
			if(@$data['user_type'] == 'personal'){
				$data['age'] = "";
				$data['gender'] = "";
				unset($data['age'],$data['gender']);
				return true;
			}
		}
		return true;
	}

	public function validateAppointmentData(&$data,$type,&$message){
		if($type == 'insert'){
			$appoint_date = (@$data['appoint_date']) ? @$data['appoint_date'] : "";
			$appoint_time = (@$data['appoint_time']) ? @$data['appoint_time'] : "";
			$data['appoint_date'] = formatToSqlDate($appoint_date);
			$data['appoint_time'] = formatToSqlTime($appoint_time);
			return true;
		}
		return true;
	}

	public function validateScheduleData(&$data,$type,&$message){
		if($type == 'insert' || $type == 'update'){
			$start_time = @$data['available_time_start'];
			$end_time = @$data['available_time_end'];
			$data['available_time_start'] = formatToSqlTime($start_time);
			$data['available_time_end'] = formatToSqlTime($end_time);
			return true;
		}
		return true;
	}

	public function validateHealth_conditionData(&$data,$type,&$message){
		if($type == 'insert'){
			$year = @$data['health_year'];
			$month = @$data['health_month'];
			$day = @$data['health_day'];
			$data['diagnosed_period'] = ($year."-".$month."-".$day);
			unset($data['health_year'],$data['health_month'],$data['health_day']);
			return true;
		}
		return true;
	}

	public function validateBlood_sugarData(&$data,$type,&$message){
		if($type == 'insert'){
			$bsTime = @$data['bs_time'];
			$bsDate = @$data['bs_date'];
			$bsTime = formatToSqlTime($bsTime);
			$data['date_created'] = ($bsDate ." ". $bsTime);
			unset($data['bs_time'],$data['bs_date']);
			return true;
		}
		return true;
	}
	public function validateExerciseData(&$data,$type,&$message){
		if($type == 'insert'){
			$exTime = @$data['ex_time'];
			$exDate = @$data['ex_date'];
			$exTime = formatToSqlTime($exTime);
			$data['date_created'] = ($exDate ." ". $exTime);
			unset($data['ex_time'],$data['ex_date']);
			return true;
		}
		return true;
	}
	public function validateHeightData(&$data,$type,&$message){
		if($type == 'insert'){
			$hTime = @$data['h_time'];
			$hDate = @$data['h_date'];
			$hTime = formatToSqlTime($hTime);
			$data['date_created'] = ($hDate ." ". $hTime);
			unset($data['h_time'],$data['h_date']);
			return true;
		}
		return true;
	}
	public function validateWaist_measurementData(&$data,$type,&$message){
		if($type == 'insert'){
			$wmTime = @$data['wm_time'];
			$wmDate = @$data['wm_date'];
			$wmTime = formatToSqlTime($wmTime);
			$data['date_created'] = ($wmDate ." ". $wmTime);
			unset($data['wm_time'],$data['wm_date']);
			return true;
		}
		return true;
	}
	public function validateWeightData(&$data,$type,&$message){
		if($type == 'insert'){
			$weightTime = @$data['weight_time'];
			$weightDate = @$data['weight_date'];
			$weightTime = formatToSqlTime($weightTime);
			$data['date_created'] = ($weightDate ." ". $weightTime);
			unset($data['weight_time'],$data['weight_date']);
			return true;
		}
		return true;
	}
	public function validateMood_trackerData(&$data,$type,&$message){
		if($type == 'insert'){
			$mtTime = @$data['mt_time'];
			$mtDate = @$data['mt_date'];
			$mtTime = formatToSqlTime($mtTime);
			$data['date_created'] = ($mtDate ." ". $mtTime);
			unset($data['mt_time'],$data['mt_date']);
			return true;
		}
		return true;
	}
	public function validateSleepData(&$data,$type,&$message){
		if($type == 'insert'){
			$sTime = @$data['s_time'];
			$sDate = @$data['s_date'];
			$sTime = formatToSqlTime($sTime);

			$wTime = @$data['w_time'];
			$wDate = @$data['w_date'];
			$wTime = formatToSqlTime($wTime);
			$data['time_to_bed'] = ($sDate ." ". $sTime);
			$data['wakeup_time'] = ($wDate ." ". $wTime);
			unset($data['s_time'],$data['s_date'],$data['w_time'],$data['w_date']);
			return true;
		}
		return true;
	}
}
 ?>
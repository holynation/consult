<?php 
/**
* This is the class that manages all information and data retrieval needed by the admin section of this application.
*/
class AdminData extends CI_Model
{
	private $admin;
	function __construct()
	{
		parent::__construct();
		$this->loadAdmin();
	}

	private function loadAdmin()
	{
		$id = $this->webSessionManager->getCurrentUserProp('user_table_id');
		loadClass($this->load,'admin');
		$this->admin = new Admin(array('ID'=>$id));
		$this->admin->load();
	}

	public function loadDashboardData()
	{
		$result = array();
		// loadClass($this->load,'member');
		// loadClass($this->load,'agent');
		// loadClass($this->load,'company');
		// loadClass($this->load,'transaction_history');
		// $where='';
		// date_default_timezone_set("Africa/Lagos");
		// $currentDate = date('Y-m-d');

		// $result['countData']=array('member'=>$this->member->totalCount(),'agent'=>$this->agent->totalCount(),'company'=>$this->company->totalCount("where status = '1'"),'transaction_history'=>$this->transaction_history->totalCount("where status = '1' and cast(date_created as date) = '$currentDate' "));
		// $result['companyDistribution']=$this->company->getCompanyDistributionByDepartment();

		return $result;
	}

	public function getAdminSidebar()
	{
		loadClass($this->load,'role');
		$role = new Role();
		return $role->getModules();
	}
	public function getCanViewPages($role)
	{
		$result =array();
		$allPages =$this->getAdminSidebar();
		$permissions = $role->getPermissionArray();
		foreach ($allPages as $module => $pages) {
			$has = $this->hasModule($permissions,$pages,$inter);
			$allowedModule =$this->getAllowedModules($inter,$pages['children']);
			$allPages[$module]['children']=$allowedModule;
			$allPages[$module]['state']=$has;
		}
		return $allPages;
	}

	private function getAllowedModules($includesPermission,$children)
	{
		$result = $children;
		$result=array();
		foreach($children as $key=>$child){
			if(is_array($child)){
				foreach($child as $childKey => $childValue){
					if (in_array($childValue, $includesPermission)) {
						$result[$key]=$child;
					}
				}
			}else{
				if (in_array($child, $includesPermission)) {
					$result[$key]=$child;
				}
			}
			
		}
		return $result;
	}

	private function hasModule($permission,$module,&$res)
	{
		if(is_array(array_values($module['children']))){
			$res =array_intersect(array_keys($permission), array_values_recursive($module['children']));
		}else{
			$res =array_intersect(array_keys($permission), array_values($module['children']));
		}
		
		if (count($res)==count($module['children'])) {
			return 2;
		}
		if (count($res)==0) {
			return 0;
		}
		else{
			return 1;
		}

	}

}

 ?>
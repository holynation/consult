<?php 
/**
* This is the class that manages all information and data retrieval needed by the member section of this application.
*/
class PatientData extends CI_Model
{
	private $patient;
	function __construct()
	{
		parent::__construct();
		$this->loadPatient();
	}

	private function loadPatient()
	{
		$id = $this->webSessionManager->getCurrentUserProp('user_table_id');
		loadClass($this->load,'patient');
		$this->patient = new Patient(array('ID'=>$id));
		$this->patient->load();
	}

	public function loadDashboardInfo()
	{
		//get the information needed
		$result = array();
		loadClass($this->load,'doctor');
		$result['doctors'] = $this->doctor->all();
		
		return $result;
	}

	public function loadChatHistory($patientID,$doctorID){
		$result = array();
		loadClass($this->load,'chat_history');
    	$chatResult = $this->chat_history->getChatHistory($patientID,$doctorID,'patient');
    	if(!$chatResult){
    		return false;
    	}
    	foreach($chatResult as $key => $chat){
    		$current = $chat['message_side'];
    		$chat['message_side'] = ($current == 1) ? true : false;
			$result[] = $chat;
    	}
    	// print_r($result);exit;
    	return $result;
	}

	public function getJournal(){
		$result = array();
		loadClass($this->load,'settings');
		$id = $this->webSessionManager->getCurrentUserProp('user_table_id');
		$setting = $this->settings->getWhereNonObject(array('patient_id'=>$id),$totalRow,0,1);
		$result['settings']  = (!$setting) ? false : $setting[0];
		$result['journals'] = $this->patient->getJournals();
		return $result;
	}

	public function getAppoint(){
		$result = array();
		$result['appoints'] = $this->patient->getAppoints();
		return $result;
	}


}
<?php 
/**
* This is the class that manages all information and data retrieval needed by the member section of this application.
*/
class DoctorData extends CI_Model
{
	private $doctor;
	function __construct()
	{
		parent::__construct();
		$this->loadDoctor();
	}

	private function loadDoctor()
	{
		$id = $this->webSessionManager->getCurrentUserProp('user_table_id');
		loadClass($this->load,'doctor');
		$this->doctor = new Doctor(array('ID'=>$id));
		$this->doctor->load();
	}

	public function loadDashboardData()
	{
		$result = array();

		return $result;
	}

	public function loadChatHistory($patientID,$doctorID){
		$result = array();
		loadClass($this->load,'chat_history');
    	$chatResult = $this->chat_history->getChatHistory($patientID,$doctorID,'doctor');
    	if(!$chatResult){
    		return false;
    	}
    	foreach($chatResult as $chat){
    		$current = $chat['message_side'];
    		$chat['message_side'] = ($current == 0) ? true : false;
			$result[] = $chat;
    	}
    	// print_r($result);exit;
    	return $result;
	}

	public function getNearestAppoint($patientID = ''){
		$result = array();
		if($patientID != ''){
			return $this->doctor->getPatientAppoint($patientID);
		}else{
			$result['docPatients'] = $this->doctor->getPatientAppoint();
		}
		
		return $result;
	}
}
<?php $base=base_url(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta content="Login to the access the online medical platform" name="description" />
    <meta content="Online Medical Consultation" name="Technode Solutions" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo getTitlePage('login'); ?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/global/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
</head>

<body>
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark">
        <div class="navbar-brand">
            <a href="<?php echo base_url(); ?>" class="d-inline-block">
                <img src="<?php echo base_url(); ?>assets/global/images/logo_light.png" alt="">
            </a>
        </div>
        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-mobile">
            <span class="navbar-text ml-md-3 mr-md-auto">
                <span class="badge bg-success"></span>
            </span>
            <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="btn btn-info">Home</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->

    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">
                <!-- Login card -->
                <!-- <form class="login-form form-validate" action="index.html"> -->
                    <?php echo form_open('auth/web', array('class'=> 'login-form form-validate')); ?>
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Login to your account</h5>
                                <span class="d-block text-muted">Your credentials</span>
                                <!-- this is the notification section -->
                                <div id="notify"></div> 
                                <!-- end notification -->
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email" autocomplete="on" required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>
                            <div class="form-group d-flex align-items-center">
                                <div class="form-check mb-0">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="remember" class="form-input-styled" checked data-fouc>
                                        Remember
                                    </label>
                                </div>
                                <a href="<?php echo base_url('auth/forget'); ?>" class="ml-auto">Forgot password?</a>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
                            </div>
                            <div class="form-group text-center text-muted content-divider">
                                <span class="px-2">or sign in with</span>
                            </div>
                            <div class="form-group text-center">
                                <button type="button" class="btn btn-outline bg-indigo border-indigo text-indigo btn-icon rounded-round border-2"><i class="icon-facebook"></i></button>
                                <button type="button" class="btn btn-outline bg-pink-300 border-pink-300 text-pink-300 btn-icon rounded-round border-2 ml-2"><i class="icon-dribbble3"></i></button>
                                <button type="button" class="btn btn-outline bg-slate-600 border-slate-600 text-slate-600 btn-icon rounded-round border-2 ml-2"><i class="icon-github"></i></button>
                                <button type="button" class="btn btn-outline bg-info border-info text-info btn-icon rounded-round border-2 ml-2"><i class="icon-twitter"></i></button>
                            </div>
                            <div class="form-group text-center text-muted content-divider">
                                <span class="px-2">Don't have an account?</span>
                            </div>
                            <div class="form-group">
                                <div class="spinner-grow text-info m-2" role="status" id="signUpSpinner" style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                <a href="<?php echo base_url('auth/register'); ?>" class="btn btn-light btn-block">Sign up</a>
                            </div>
                            <span class="form-text text-center text-muted">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>
                        </div>
                    </div>
                    <input type="hidden" name="isajax">
                    <input type="hidden" id='base_path' value="<?= $base ?>">
                    <?php echo form_close(); ?>
                <!-- </form> -->
                <!-- /login card -->
            </div>
            <!-- /content area -->

            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; <?php echo date('Y'); ?> <a href="#"></a>Online Medical Consultation by <a href="" target="_blank">Technode Solutions</a>
                    </span>
                </div>
            </div>
            <!-- /footer -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

<!-- Core JS files -->
<script src="<?php echo base_url(); ?>assets/global/js/main/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/main/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="<?php echo base_url(); ?>assets/global/js/plugins/forms/validation/validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/forms/styling/uniform.min.js"></script>

<script src="<?php echo base_url(); ?>assets/private/js/app.js"></script>
<script src="<?php echo base_url(); ?>assets/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/demo_pages/login_validation.js"></script>
    <!-- /theme JS files -->
<script>
  $(function () {
    var form =$('form');
    form.submit(function(event) {
      event.preventDefault();
      var note = $("#notify");
      note.text('').hide();
      $('#signUpSpinner').show();
      setTimeout(function(){
        submitAjaxForm(form);
      }, 2000);
      
    });
  });

  function ajaxFormSuccess(target,data){
    if (data.status) {
      var path = data.message;
      location.assign(path);
    }
    else{
      $("#notify").show();
      $('#signUpSpinner').hide();
      $("#notify").text(data.message).addClass("alert alert-danger alert-dismissible fade show").append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    }
  }
</script>

</body>
</html>
<?php include_once 'template/header.php'; ?>
<style type="text/css" media="screen">
.message_input{
    box-sizing: border-box;
    border-radius: 50px;
    background-color: #546E7A;
    border: 2px solid #546E7A;
    width: calc(100% - 40px);
    position: absolute;
    outline-width: 0;
    color:#fff;
    font-size: 15px;
    height:48px;
}    
</style>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item"><?php echo removeUnderscore($model); ?></a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->
            <div>
                <?php
                    $chatType = ($chat_type) ? $chat_type : false;
                    $user_id = ($doctor) ? $doctor->ID : "";
                    $doctorImg = ($doctor->doctor_path) ? $doctor->doctor_path : "";
                    $patientID = (isset($patient_id)) ? $patient_id : "";
                    $patientImg = (isset($patient_img)) ? $patient_img : "";
                    $patientName = (isset($patient_name)) ? $patient_name : "";
                    if(!$chatType){
                        exit("Oops, you can't perform the following operation...");
                    }
                 ?>
            </div>

            <?php
                $class='';
                switch (($chatType == 'chat_form') && $currentAppoint['appoint_status']) {
                  case 'pending':
                    $class = "btn-default";
                    break;
                  case 'active':
                    $class='btn-danger';
                    break;
                  case 'success':
                    $class='btn-success';
                    break;
                  default:
                    # code...
                    break;
                }
            ?>
            
            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Chat with Patient <?php echo $patientName; ?></h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                            </div>
                            <?php if($chatType == 'chat_form'): ?>
                            <button type="button" class="btn <?php echo $class; ?> status-btn ml-1" data-target='<?php echo $currentAppoint['id']; ?>' data-status='<?php echo $currentAppoint['appoint_status']; ?>'><i class="icon icon-database-edit2 "> Status</i>
                            </button>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php if($chatType == 'start_chat'): ?>
                        <div class="card-body">
                            <h2>START A CHAT WITH YOUR PATIENT</h2>
                        </div>
                    <?php endif; ?>
                    <!-- this is the form to chat with the doctor -->
                    <?php if($chatType == 'chat_form'): ?>
                    <div class="card-body">
                        <ul class="message_template media-list media-chat media-chat-scrollable mb-3">
                            <div id="messages"></div>
                        </ul>
                        <input type="text" name="message_input" id="message_input" class="form-control mb-3 border-teal border-1 message_input" rows="3" cols="1" placeholder="Enter your message..." />
                        <div class="d-flex align-items-center">
                            <input type="hidden" name="recipient_id" id="recipient_id" value="<?php echo $patientID; ?>">
                            <span style="display: none;" id="token"></span>
                            <div class="btn bg-dark-400 btn-labeled ml-auto mt-1"><i class="icon-paperplane" id="send_message"></i></div>
                        </div>
                    </div>
                    <?php
                        $formContent='';
                        $statusID = $currentAppoint['id'];
                        $exclude = array('patient_id','doctor_id','health_services_id','meetup_location','appoint_date','appoint_time','appoint_reason');
                        $formContent= $this->modelFormBuilder->start('appointment_status')
                            ->appendUpdateForm('appointment',true,$statusID,$exclude)
                            ->addSubmitLink(null,false)
                            ->appendSubmitButton('Update','btn btn-success')
                            ->build();
                    ?>
                    <div id="small_modal" class="modal fade animated" role="dialog">
                      <div class="modal-dialog modal-sm">
                          <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Change Appointment status</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              <div class="modal-body">
                                <div class="alert alert-info">
                                <b>Note: </b> <span class="text-muted"><label class="text-danger">Active:Live</label>, <label class="text-info">Pending: Pend</label>, <label class="text-success">Success: Resolved</label></span></div>
                                <p>
                                  <?php 
                                    echo $formContent; 
                                  ?>
                                </p>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                  </button>
                              </div>
                          </div>
                      </div>
                    </div>
                    <?php endif; ?>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>

 <?php if($chatType == 'chat_form'): ?>
<script type="text/javascript" charset="utf-8" async defer>
// Socket Server
// i wanna find a way to know when users are disconnected and queue any message sent or send to db after disconnecting 
// i wanna fixed the timezone of this for firefox browser
    var _chats = JSON.parse('<?php echo ($chat_history); ?>');
    var conn = new WebSocket('ws://127.0.0.1:8080');
    var client = {
        user_id: <?php echo $user_id; ?>,
        recipient_id: null, 
        type: 'socket',
        token: null,
        time: null,
        message: null,
        msg_sender: null,
    };

    conn.onopen = function(e) {
        conn.send(JSON.stringify(client));
       console.log("Connection established!");
       // this is where to load chat history
        loadHandleBar(_chats,'load');
    };

    Handlebars.registerHelper("each", function(context, options) {
        var listVal= "";
        _.forEach(context, function(value, key){
            if(key != 'track'){
                listVal += "<li class='media content-divider justify-content-center text-muted mx-0'><span class='text-muted px-2 font-weight-semibold font-italic'>"+ timeAgoFormat(key) +"</span></li>";
            }
            _.forEach(value, function(item,ind){
                if(item.message_side){
                    listVal += "<li class='media media-chat-item-reverse'><div class='media-body'><div class='media-chat-item'>" + (item.text)+ "</div><div class='font-size-sm text-muted mt-1'>"+ (item.time) +"<a href='#'><i class='icon-pin-alt ml-2 text-muted'></i></a></div></div><div class='ml-3'></div></li>"
                }else{
                    listVal += "<li class='media'><div class='mr-3'><a href='<?php echo base_url($patientImg); ?>'><img src='<?php echo base_url($patientImg); ?>' class='rounded-circle' width='40' height='40' alt=''></a></div><div class='media-body'><div class='media-chat-item'>" + (item.text)+ "</div><div class='font-size-sm text-muted mt-1'>"+ (item.time) +"<a href='#'><i class='icon-pin-alt ml-2 text-muted'></i></a></div></div><div class='ml-3'></div></li>"
                }
            });
        });
        return listVal;
    });

    function loadHandleBar(_messages,type){
        var _type = (type == 'append') ? 'counter' : 'date_created';
        const groupByDate = _.groupBy(_messages, _type);

        // var messages_template = Handlebars.compile($('#messages-template').html());
        var messages_template = Handlebars.templates['chats'];
        var messages_html = messages_template({'messages': groupByDate});

        if(type == 'append'){
            $('#messages').append(messages_html);
        }
        
        if(type == 'load'){
            $('#messages').html(messages_html);
        }

        $(".message_template").animate({ scrollTop: $('#messages').prop('scrollHeight')}, 1000);
        return true;
    }

    function playSound(name){
        var src = "<?php echo base_url(); ?>assets/sound/"+ name + ".mp3";
        var audio = new Audio(src);
        audio.play();
    }

    function statusRun(){
        $('.status-btn').click(function(event) {
            var target = $(this).attr('data-target'); //returns  the id
            var form = $('#appointment_status');
            var action =form.attr('action');
            var str = 'appointment';
            var len = str.length;
            var pos = action.indexOf(str);
            if (pos===false) {
              return;
            }
            var concern = action.substring(0,pos+len);
            var link = concern+'/'+target+'/1';
            form.attr('action', link);
            $('#small_modal').modal();
        });
    }

    function ajaxFormSuccess(target,data) {
        if (data.status) {
          location.reload();
        }
        showNotification(data.status,data.message);
    }

    (function () {

        statusRun();

        $(function () {
           var getMessageText, messageTemplate;
           let message_side = true;
           // this func is to send message out to recipient
           getMessageText = function () {
               var $message_input,msg;
               let currentTime = moment().format('hh:mm a');
                $message_input = $('.message_input').val();
                if ($message_input == "") {
                    return false;
                }
                client.message = $message_input;
                client.token = $('#token').text().split(': ')[1];
                client.type = 'chat';
                client.time = currentTime;
                client.msg_sender = 'doctor';

                if ($('#recipient_id').val()) {
                    client.recipient_id = $('#recipient_id').val();
                }

                conn.send(JSON.stringify(client));
                return client;
            };

            // this is template message function
            messageTemplate = function (text, msg_side) {
                var message,textMessage,messageTime;
                var currentTime = moment().format('hh:mm a');
                    textMessage = (text.message != undefined) ? text.message : null;
                    messageTime = (text.time != undefined) ? text.time : currentTime;
                var messages = [];

                if(text.type === 'error'){
                    showNotification(false,'error sending message,try again later.');
                }
                if (text.type === 'token') {
                    $('#token').html('JWT Token : ' + text.token);
                }
                if (textMessage === null) {
                  return;
                }
                $('.message_input').val('');

                // using this approach to send client message back to the browser
                message_side = (msg_side != undefined) ? msg_side : message_side;
                var objDate = moment().format("YYYY-MM-DD");
                message = {
                    'text' : textMessage,
                    'message_side' : message_side,
                    'time': messageTime,
                    'date_created': objDate,
                    'counter': 'track' // using this to track current date and use it to eliminate duplicate current date
                }
                messages.push(message);
                message_side = true;

                loadHandleBar(messages,'append');
            };

            $('#send_message').click(function (e) {
                return messageTemplate(getMessageText());
            });
            $('.message_input').keyup(function (e) {
               if (e.which === 13) {
                   return messageTemplate(getMessageText());
                }
            });

            conn.onmessage = function(e) {
                console.log(e);
                // playSound('popcorn');
                var msg = JSON.parse(e.data);
                messageTemplate(msg, false);
            };

            conn.onerror = function(e){
                console.log(e)
                if(e.type === 'error'){
                    showNotification(false,'error in establishing a connection, try again by refreshing...');
                }
            }

            conn.onclose = function(e){
                console.log('connection closed')
            }
        });
    }.call(this));
 </script>
 <?php endif; ?>
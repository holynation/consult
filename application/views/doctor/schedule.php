<?php include_once 'template/header.php'; 

$showStatus = false;
$submitLabel = "Submit Schedule";
$headerTitle = removeUnderscore($model);
$exclude = array();
$appendExtra="";
$hidden = array();
$tableExclude = array();
$tableAction= array('delete' => 'delete/schedule', 'edit' => 'edit/schedule');

$formContent= $this->modelFormBuilder->start($model.'_table')
->appendInsertForm($model,true,$hidden,'',$showStatus,$exclude)
->appendExtra($appendExtra)
->addSubmitLink()
->appendResetButton('Reset Schedule','btn btn-outline bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg')
->appendSubmitButton($submitLabel,'btn btn-outline bg-pink-400 text-pink-400 border-pink-400 rounded-round btn-lg')
->build();

 $tableData= $this->tableViewModel->getTableHtml($model,$count,$tableExclude,$tableAction,true,0,null,true);
?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item"><?php echo removeUnderscore($model); ?></a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><?php echo $headerTitle; ?></h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="row card-body">
                        <div class="row">
                            <div class="col-lg-12 col-sm-6">
                                <a href="javascript:void(0);" data-toggle='modal' data-target='#myModal'>
                                    <button class="btn btn-labeled btn-labeled-right bg-primary waves-effect width-md float-right">
                                        <i class="icon icon-list"></i>  Add new Schedule(s)
                                    </button>
                                </a>
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">View Schedule(s)</h3>
                                    </div>
                                    <div class="panel-body">
                                        <?php echo $tableData; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

            <!-- this is add modal -->
              <div>
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel"><?php echo removeUnderscore($model);  ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      </div>
                      <div class="modal-body">
                        <p><?php echo $formContent; ?></p>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
              </div>

              <!-- this is for the edit -->
              <div class="row">
                <div id="modal-edit" class="modal fade animated" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <p id="edit-container">

                          </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
<script>
    var inserted=false;
    $(document).ready(function($) {
      $('.modal').on('hidden.bs.modal', function (e) {
        if (inserted) {
          inserted = false;
          location.reload();
        }
    });
    $('.close').click(function(event) {
      if (inserted) {
        inserted = false;
        location.reload();
      }
    });
      $('li[data-ajax-edit=1] a').click(function(event){
        event.preventDefault();
        var link = $(this).attr('href');
        var action = $(this).text();
        sendAjax(null,link,'','get',showUpdateForm);
      });
    });
    function showUpdateForm(target,data) {
      var data = JSON.parse(data);
      if (data.status==false) {
        showNotification(false,data.message);
        return;
      }
       var container = $('#edit-container');
       container.html(data.message);
       //rebind the autoload functions inside
       $('#modal-edit').modal();
    }
    function ajaxFormSuccess(target,data) {
      if (data.status) {
        inserted=true;
      }
      showNotification(data.status,data.message);
      if (typeof target ==='undefined') {
        location.reload();
      }
    }
  </script>
 <?php include_once 'template/footer.php'; ?>
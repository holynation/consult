<?php include_once 'template/header.php'; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item">Doctor Profile</a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Doctor Profile Page</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <!-- Right Sidebar -->
                            <div class="col-lg-12">
                                <div class="card-box row justify-content-between">
                                    <!-- Left sidebar -->
                                    <div class="col-4">
                                        <?php if (@$doctor->doctor_path): ?>
                                        <img src="<?php echo base_url(@$doctor->doctor_path); ?>" class="img-lg rounded-circle mb-2 ml-4" alt="profile picture">
                                        <?php else: ?>
                                        <img class="img-lg rounded-circle mb-2 ml-4" src="<?php echo base_url('assets/images/default-profile.jpg'); ?>" alt="profile picture">
                                        <br /> 
                                        <?php endif; ?>

                                        <?php if ($userType == 'doctor' && @$doctor->doctor_path): ?>
                                        <div class="showupload btn btn-primary btn-block btn-sm mt-3 mb-4" style="cursor: pointer;text-transform: uppercase;">change photo</div>
                                        <div class="form mb-2">
                                            <?php $userID = (isset($data['id']) && $data['id'] != '') ? $data['id'] : $doctor->ID; ?>
                                            <div class="upload-control" style="display: none; width: 205px;margin:0 auto;">
                                              <form id="data_profile_change" method="post" enctype="multipart/form-data" action="<?php echo base_url('mc/update/doctor/'.$userID.'/1') ?>">
                                              <label for="">
                                                choose file to upload <br>
                                                <input type="file" name="doctor_path" id="doctor_path" class="form-control">
                                              </label>
                                              <input type="submit" value="Upload Photo" name="submit-btn" class="btn btn-primary">
                                              </form>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                        <div class="row border-top border-bottom pt-3 mb-1">
                                            <div class="col-12">
                                              <h6 class="font-weight-medium">Email</h6>
                                              <p><?php echo @$doctor->email; ?></p>
                                            </div>
                                         </div>
                                         <?php if($userType == 'patient'): ?>
                                         <div class="row pt-3 mb-1">
                                            <div>
                                                <?php $doctorID = rndEncode(@$doctor->ID);?>
                                                <ul class="media-list">
                                                    <li class="mb-1">
                                                        <a href="<?php echo base_url("vc/create/text_consultation/single/$doctorID/question"); ?>" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-comment"></i></b> Question Consultation</a>
                                                    </li>
                                                    <li class="mb-1">
                                                        <a href="<?php echo base_url("vc/create/appointment/single/$doctorID/chat?book_type=checkout"); ?>" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-comment-discussion"></i></b> Chat Consultation</a>
                                                    </li>
                                                    <li class="mb-1">
                                                        <a href="" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-video-camera "></i></b> Video Consultation</a> 
                                                    </li>
                                                    <li class="mb-1">
                                                        <a href="" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-phone"></i></b> Phone Consultation</a>  
                                                    </li>
                                                </ul>   
                                            </div>
                                         </div>
                                        <?php endif; ?>
                                        <?php if($userType == 'doctor'): ?>
                                        <div class="mt-2">
                                            <a href="javascript:void(0);" class="btn btn-danger btn-block waves-effect waves-light" data-toggle='modal' data-target='#myModalPassword'>Change Password</a>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <!-- End Left sidebar -->
                                    <div class="col-7">
                                        <div>
                                            <div class="col-md-12 col-lg-12 grid-margin stretch-card">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Personal Biodata</h4>
                                                        <div class="table-responsive">
                                                          <table class="table table-hover">
                                                            <tbody>
                                                            <tr>
                                                              <td>Surname</td>
                                                              <td><?php echo $doctor->lastname; ?></td>
                                                            </tr>
                                                            <tr>
                                                              <td>Firstname</td>
                                                              <td><?php echo $doctor->firstname; ?></td>
                                                            </tr>
                                                            <tr>
                                                              <td>Middlename</td>
                                                              <td><?php echo $doctor->middlename; ?></td>
                                                            </tr>
                                                            <tr>
                                                              <td>Phone Number</td>
                                                              <td><?php echo format_phone('nig',$doctor->phone_num); ?></td>
                                                            </tr>
                                                            <tr>
                                                              <td>Address</td>
                                                              <td><?php echo format_phone('nig',$doctor->address); ?></td>
                                                            </tr>
                                                            </tbody>
                                                          </table>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>
                                        <!-- end row-->
                                    </div> 
                                    <!-- end inbox-rightbar-->

                                    <div class="clearfix"></div>
                                </div> <!-- end card-box -->
                            </div> <!-- end Col -->
                        </div><!-- End row -->
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>
<?php $base=base_url(); ?>
        
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta content="Register to the access the online medical platform" name="description" />
    <meta content="Online Medical Consultation" name="Technode Solutions" />
    <title><?php echo getTitlePage('register'); ?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/global/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
</head>
<body>
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark">
        <div class="navbar-brand">
            <a href="<?php echo base_url(); ?>" class="d-inline-block">
                <img src="<?php echo base_url(); ?>assets/global/images/logo_light.png" alt="">
            </a>
        </div>
        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="<?php echo base_url(); ?>" class="navbar-nav-link">
                        <i class="icon-display4"></i>
                        <span class="d-md-none ml-2">Go to website</span>
                    </a>                    
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->

    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">
                <!-- Registration form -->
                <!-- <form action="index.html" class="flex-fill"> -->
                    <?php echo form_open("auth/register?_=".time(), array('class'=> 'flex-fill','id'=>'signForm')); ?>
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                            <div class="card mb-0">
                                <div class="card-body">
                                    <div class="text-center mb-3">
                                        <i class="icon-plus3 icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1"></i>
                                        <h5 class="mb-0">Create account</h5>
                                        <span class="d-block text-muted">All fields are required</span>
                                        <!-- this is the notification section -->
                                        <div id="notify"></div> 
                                        <!-- end notification -->
                                    </div>
                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                        <input type="text" class="form-control" placeholder="Enter valid phone number" name="phone_num" id="phone_num">
                                        <div class="form-control-feedback">
                                            <i class="icon-phone text-muted"></i>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-feedback form-group-feedback-right">
                                                <input type="text" class="form-control" placeholder="First name" name="firstname" id="firstname">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-check text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-feedback form-group-feedback-right">
                                                <input type="text" class="form-control" placeholder="Last name" name="lastname" id="lastname">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-check text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-feedback form-group-feedback-right">
                                                <input type="password" class="form-control" placeholder="Create password" name="password" id="password">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-lock text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-feedback form-group-feedback-right">
                                                <input type="password" class="form-control" placeholder="Repeat password" name="conf_password" id="conf_password">
                                                <div class="form-control-feedback">
                                                    <i class="icon-user-lock text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-feedback form-group-feedback-right">
                                                <input type="email" class="form-control" placeholder="Your email" name="email" id="email">
                                                <div class="form-control-feedback">
                                                    <i class="icon-mention text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-feedback form-group-feedback-right">
                                                <input type="text" class="form-control" placeholder="Your Gender" name="gender" id="gender">
                                                <div class="form-control-feedback">
                                                    <i class="icon-person text-muted"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-input-styled" name="checkboxSignup" id="checkboxSignup" data-fouc>
                                                Accept <a href="#">Terms of service</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="spinner-grow text-info m-2" role="status" id="signUpSpinner" style="display: none;">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                        <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b> Create account</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /registration form -->
            </div>
            <!-- /content area -->

            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; <?php echo date('Y'); ?> <a href="#"></a>Online Medical Consultation by <a href="" target="_blank">Technode Solutions</a>
                    </span>
                </div>
            </div>
            <!-- /footer -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->


<!-- Core JS files -->
<script src="<?php echo base_url(); ?>assets/global/js/main/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/main/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="<?php echo base_url(); ?>assets/global/js/plugins/forms/validation/validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/forms/styling/uniform.min.js"></script>

<script src="<?php echo base_url(); ?>assets/private/js/app.js"></script>
<script src="<?php echo base_url(); ?>assets/custom.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/demo_pages/login.js"></script>
<!-- /theme JS files -->
<script>
  $(function () {
    var form =$('form');
    form.submit(function(event) {
      event.preventDefault();
      var note = $("#notify");
      note.text('').hide();
      $('#signUpSpinner').show();
      setTimeout(function(){
        submitAjaxForm(form);
      }, 1200);
    });

  });

  function ajaxFormSuccess(target,data){
    if (data.status) {
        var loginUrl = "<?php echo base_url('auth/web'); ?>";
      $("#notify").show();
      $('#signUpSpinner').hide();
      $("#notify").html("<p class='text-center'>" +data.message+ "</p>").addClass("alert alert-success alert-dismissible fade show text-center").append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $('#signForm').trigger('reset');
      setTimeout(function(){
        // redirect('/auth/web/','refresh');
        location.assign(loginUrl);
      },2000)
    }
    else{
      $("#notify").show();
      $('#signUpSpinner').hide();
      $("#notify").html("<p class='text-center'>" + data.message + "</p>").addClass("alert alert-danger alert-dismissible fade show text-center").append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    }
  }
</script>

</body>
</html>
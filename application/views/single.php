<?php include_once 'template/header.php'; 

$sideParam1="";
$sideParam2="";
$param = rndDecode($param);
$service = ($service_id) ? $service_id : "";
$serviceFee = ($service_fee) ? $service_fee : "";
$hidden = array('doctor_id'=>$param,'patient_id'=>$this->webSessionManager->getCurrentUserProp('user_table_id'));
$showStatus = ($configData && array_key_exists('show_status', $configData))?$configData['show_status']:false;
$submitLabel = ($configData && array_key_exists('submit_label', $configData))?$configData['submit_label']:"Submit";
$headerTitle = ($configData && array_key_exists('headerTitle', $configData))?$configData['headerTitle']:removeUnderscore($model);
$exclude = ($configData && array_key_exists('exclude', $configData))?$configData['exclude']:array();
$appendExtra="";

if($model == 'text_consultation'){
    $sideParam1=$this->lang->line('side_text1');
    $sideParam2=$this->lang->line('side_text2');
    $appendExtra="<input type='hidden' name='service_id' id='service_id' value='$service' /><input type='hidden' name='service_fee' id='service_fee' value='$serviceFee' />";

}else if($model == 'appointment'){
    $sideParam1=$this->lang->line('side_text3');
    $sideParam2=$this->lang->line('side_text4');
    $hidden = array_merge($hidden,array('health_services_id'=>$service));
    $appendExtra="<input type='hidden' name='service_fee' id='service_fee' value='$serviceFee' />";
}

$formContent= $this->modelFormBuilder->start($model.'_table')
->appendInsertForm($model,true,$hidden,'',$showStatus,$exclude)
->appendExtra($appendExtra)
->addSubmitLink()
->appendSubmitButton($submitLabel,'btn btn-outline bg-pink-400 text-pink-400 border-pink-400 rounded-round btn-lg')
->build();
?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item"><?php echo removeUnderscore($model); ?></a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><?php echo $headerTitle; ?></h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="row card-body">
                        <div class="col-7">
                            <?php if($model == 'text_consultation'): ?>
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label d-block font-weight-semibold">Asking question for</label>
                                <div class="col-lg-9">
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" name="styled_inline_radio" id="someElseRadio" class="form-input-styled" required data-fouc checked>
                                            Myself
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" name="styled_inline_radio" id="someElseRadio1" class="form-input-styled" data-fouc>
                                            Someone i know
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>

                            <?php echo $formContent; ?>

                            <span class="form-text text-muted"><b>NOTE:</b> On Click Submit Question to Pay, you will be directed to the secure payment page of our payment service provider. <br />
                            Attention: Your payment is being submitted. Please do not close this window or click the Back button on your browser.
                            </span>
                        </div>
                        <div class="col-4 ml-4 border-left">
                            <div class="card">
                                <div class="card-header bg-transparent header-elements-inline">
                                    <h4 class="card-title font-weight-semibold">What you need to know</h4>
                                </div>
                                <div class="card-body">
                                    <ul class="media-list" style="font-size: 16.8px;">
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-pink text-pink rounded-round border-2 btn-icon">
                                                    <i class="icon-starburst"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                You will be charged NGN <?php echo $service_fee; ?> which can be paid by credit card,debit card or internet banking.
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon">
                                                    <i class="icon-comment"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <?php echo $sideParam1; ?>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-info text-info rounded-round border-2 btn-icon">
                                                    <i class="icon-sort-time-desc"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <?php echo $sideParam2; ?>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-danger text-danger rounded-round border-2 btn-icon">
                                                    <i class="icon-comment"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                Every information supplied is secure and confidential.
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- <div class="card">
                                <div class="card-header bg-transparent header-elements-inline">
                                    <h4 class="card-title font-weight-semibold"><legend class="font-weight-semibold"><i class="icon-truck mr-2"></i> Shipping details</legend></h4>
                                </div>
                                <div class="card-body">
                                    <?php
                                        // $fullname = $this->webSessionManager->getCurrentUserProp('firstname')." ". $this->webSessionManager->getCurrentUserProp('lastname');
                                        // $phone = $this->webSessionManager->getCurrentUserProp('phone_num');
                                        // $email = $this->webSessionManager->getCurrentUserProp('email');
                                    ?>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Your name:</label>
                                        <div class="col-lg-9">
                                            <input type="text" placeholder="Fullname" class="form-control" value="<?php //echo $fullname; ?>" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Email:</label>
                                        <div class="col-lg-9">
                                            <input type="text" placeholder="eugene@kopyov.com" class="form-control" value="<?php //echo $email; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Phone #:</label>
                                        <div class="col-lg-9">
                                            <input type="text" placeholder="+234-9999-999-9999" class="form-control" value="<?php //echo $phone; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label">Address:</label>
                                        <div class="col-lg-9">
                                            <input type="text" placeholder="Your address line" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>
 <script type="text/javascript" charset="utf-8" async defer>
    function ajaxFormSuccess(target,data){
        if (data.status) {
          var path = data.message;
          location.assign(path);
        }
        else{
          showNotification(false,data.message);
        }
    }
 </script>
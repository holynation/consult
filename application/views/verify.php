<?php $base=base_url(); 
// echo md5(mailConfig('salt') . 'test@gmail.com');exit;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8" />
      <title><?php echo getTitlePage('verification'); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta content="Login to the access the platform" name="description" />
      <meta content="Qucik bet" name="Company Login" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <!-- App favicon -->
      <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

      <!-- App css -->
      <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url(); ?>assets/css/app.min.css" rel="stylesheet" type="text/css" />

  </head>

<body class="authentication-bg authentication-bg-pattern d-flex align-items-center">
  <div class="account-pages w-100 mt-5 mb-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
          <div class="card">
            <div class="card-body p-4">
              <div class="text-center mb-4">
                  <a href="<?php echo base_url(); ?>">
                      <span><img src="<?php echo base_url(); ?>assets/images/logo-dark.png" alt="" height="28"></span>
                  </a>
              </div>
              <!-- this is the notification div -->
              <?php  if(isset($type) && $type == 'register'){ ?>
              <h3 class="text-center">Account Verification Page</h3><br/>
              <?php if(isset($success)): ?>
              <div class="alert alert-success">
                <p class="text-center mt-2"><?php echo $success; ?></p>
              </div>
              <?php endif; } ?>

              <?php if(isset($error)): ?>
              <div class="alert alert-danger">
                <p class="text-center mt-3"><?php echo $error; ?></p>
              </div>
              <?php endif;  ?>

              <!-- this is for the forget password page -->
              <?php if(isset($type) && $type == 'forget'){ ?>
              <h3 class="text-center">Reset Password</h3><br/>
              <!-- this is the notification section -->
                <div id="notify"></div> 
              <!-- end notification -->
              <form action="<?php echo base_url('auth/forgetPassword'); ?>" method="post" role="form" id="signForm">
                <?php if(isset($email_hash, $email_code)) { ?>
                <input type="hidden" name="email_hash" id="email_hash" value="<?php echo $email_hash; ?>" />
                <input type="hidden" name="email_code" id="email_code" value="<?php echo $email_code; ?>" />
                <?php  } ?>
                <div class="form-group">
                  <label class="label">Email</label>
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo (isset($email)) ? $email : '';?>" readonly>
                  </div>
                </div>
                <div class="form-group mb-3">
                    <label for="password">New Password</label>
                    <input class="form-control" type="password" required name="password" id="password" placeholder="Enter your new password">
                </div>
                <div class="form-group mb-3">
                    <label for="conf_password">Confirm Password</label>
                    <input class="form-control" type="password" required name="confirm_password" id="confirm_password" placeholder="Enter your password again">
                </div>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn btn-block">Reset</button>
                </div>
                <input type="hidden" name="isajax">
                <input type="hidden" id='base_path' value="<?php echo $base; ?>">
                <input type="hidden" name="task" value="update">
              </form>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <p class="footer-text text-center">copyright © <?php echo date('Y'); ?> <a href="<?php echo base_url(); ?>" target="_blank"><?php echo appConfig('company_name'); ?></a>. All rights reserved.</p>
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  
<script src="<?php echo base_url(); ?>assets/js/vendor.min.js"></script>
<!-- App js -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
  <!-- endinject -->
<script>
  $(function () {
    var form =$('form');
    form.submit(function(event) {
      event.preventDefault();
      var note = $("#notify");
      note.text('');
      note.hide();
      $('button').addClass('disabled');
      $('button').prop('disabled', true);
      submitAjaxForm(form);
    });
  });

  function ajaxFormSuccess(target,data){

    if (data.status) {
      $("#notify").show();
      $("#notify").removeClass('alert alert-warning');
      $("#notify").html("<p>"+data.message+"</p>").addClass("alert alert-success alert-dismissible fade show").append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>').delay(3000);
      $('#signForm').trigger('reset');
      location.assign("<?php echo base_url('auth/login'); ?>");
    }
    else{
      $("#notify").show();
      $("#notify").removeClass('alert alert-success');
      $("#notify").html("<p>"+data.message+"</p>").addClass("alert alert-danger alert-dismissible fade show").append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $('button').removeClass('disabled');
      $('button').prop('disabled', false);
    }
  }
</script>
</body>
</html>
<?php $base=base_url(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta content="Login to the access the online medical platform" name="description" />
    <meta content="Online Medical Consultation" name="Technode Solutions" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo getTitlePage('Reset'); ?></title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/global/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
</head>
<body>
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark">
        <div class="navbar-brand">
            <a href="<?php echo base_url(); ?>" class="d-inline-block">
                <img src="<?php echo base_url(); ?>assets/global/images/logo_light.png" alt="">
            </a>
        </div>
        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-display4"></i>
                        <span class="d-md-none ml-2">Go to website</span>
                    </a>                    
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-user-tie"></i>
                        <span class="d-md-none ml-2">Contact admin</span>
                    </a>                    
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-cog3"></i>
                        <span class="d-md-none ml-2">Options</span>
                    </a>                    
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->

    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">
                <!-- Password recovery form -->
                <!-- <form class="login-form" action="index.html"> -->
                    <?php echo form_open('auth/web', array('class'=> 'login-form')); ?>
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-spinner11 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Password recovery</h5>
                                <span class="d-block text-muted">We'll send you instructions in email</span>
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-right">
                                <input type="email" class="form-control" placeholder="Your email">
                                <div class="form-control-feedback">
                                    <i class="icon-mail5 text-muted"></i>
                                </div>
                            </div>
                            <div>
                                <div class="spinner-grow text-info m-2" role="status" id="signUpSpinner" style="display: none;">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                <button type="submit" class="btn bg-blue btn-block"><i class="icon-spinner11 mr-2"></i> Reset password</button>
                            </div>
                        </div>
                    </div>
                    <?php form_close(); ?>
                <!-- </form> -->
                <!-- /password recovery form -->
            </div>
            <!-- /content area -->

            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text">
                        &copy; <?php echo date('Y'); ?> <a href="#"></a>Online Medical Consultation by <a href="" target="_blank">Technode Solutions</a>
                    </span>
                </div>
            </div>
            <!-- /footer -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->

<!-- Core JS files -->
<script src="<?php echo base_url(); ?>assets/global/js/main/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/main/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="<?php echo base_url(); ?>assets/private/js/app.js"></script>
<script src="<?php echo base_url(); ?>assets/custom.js"></script>
<!-- /theme JS files -->
<script>
  $(function () {
    var form =$('form');
    form.submit(function(event) {
      event.preventDefault();
      var note = $("#notify");
      note.text('').hide();
      $('#signUpSpinner').show();
      setTimeout(function(){
        submitAjaxForm(form);
      }, 2000);
      
    });
  });

  function ajaxFormSuccess(target,data){
    if (data.status) {
      $("#notify").show();
      $("#notify").text(data.message).addClass("alert alert-success alert-dismissible fade show").append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');;
      $('#signForm').trigger('reset');
    }
    else{
      $("#notify").show();
      $('#signUpSpinner').hide();
      $("#notify").text(data.message).addClass("alert alert-danger alert-dismissible fade show").append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    }
  }
</script>
</body>
</html>
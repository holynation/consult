<?php include_once 'template/header.php'; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item"><?php echo removeUnderscore($model); ?></a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->
            <div>
                <?php 
                // print_r($doctor);exit;
                    $payType = ($chat_type) ? $chat_type : false;
                    if(!$payType){
                        exit("Oops, you can't perform the following operation...");
                    }
                    $docFirstname = $doctor->firstname;
                    $docLastname = $doctor->lastname;
                 ?>
            </div>
            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Chat Consultation</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <?php if($payType == 'chat_checkout'): ?>
                    <div class="row card-body">
                        <div class="col-7">
                            <div class="card card-body border-top-primary">
                                <div class="media flex-column flex-sm-row">
                                    <div class="mr-sm-3 mb-2 mb-sm-0">
                                        <a href="#">
                                            <img src="<?php echo base_url($doctor->doctor_path); ?>" class="rounded" width="44" height="44" alt="">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-title font-weight-semibold">
                                            <a href="#"><?php echo $docFirstname." ". $docLastname; ?></a>
                                        </h6>
                                        <ul class="list-inline list-inline-dotted text-muted mb-2">
                                            <li class="list-inline-item"><a href="#" class="text-muted"><?php echo $doctor->department->department_name; ?></a></li>
                                            <li class="list-inline-item"><?php echo $doctor->address; ?></li>
                                        </ul>
                                        <p>You are about to chat with Dr <?php echo $docFirstname." ". $docLastname; ?><p>
                                            <!-- here is the payment form -->
                                        <form action="chat_submit" method="post" accept-charset="utf-8">
                                            <input type="submit" name="btnSubmit" value="Proceed to Pay" class="btn btn-info">
                                        </form>

                                    </div>
                                    <div class="ml-sm-3 mt-2 mt-sm-0">
                                        <span class="badge bg-blue">New</span>
                                    </div>
                                </div>
                            </div>
                            <span class="form-text text-muted"><b>NOTE:</b> On Click Submit Question to Pay, you will be directed to the secure payment page of our payment service provider. <br />
                            Attention: Your payment is being submitted. Please do not close this window or click the Back button on your browser.
                            </span>
                        </div>
                        <div class="col-4 ml-4 border-left">
                            <div class="card">
                                <div class="card-header bg-transparent header-elements-inline">
                                    <h4 class="card-title font-weight-semibold">What you need to know</h4>
                                </div>
                                <div class="card-body">
                                    <ul class="media-list" style="font-size: 16.8px;">
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-pink text-pink rounded-round border-2 btn-icon">
                                                    <i class="icon-starburst"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                You will be charged NGN <?php echo $service_fee; ?> which can be paid by credit card,debit card or internet banking.
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon">
                                                    <i class="icon-comment"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                Be Specific when chatting with the Doctor and ask things that are specific
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-info text-info rounded-round border-2 btn-icon">
                                                    <i class="icon-sort-time-desc"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                The Doctor would respond quickly as the time for chatting is due
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="mr-3">
                                                <a href="#" class="btn bg-transparent border-danger text-danger rounded-round border-2 btn-icon">
                                                    <i class="icon-comment"></i>
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                Every information supplied is secure and confidential.
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>
 <script type="text/javascript" charset="utf-8" async defer>
    function ajaxFormSuccess(target,data){
        if (data.status) {
          var path = data.message;
          location.assign(path);
        }
        else{
          showNotification(false,data.message);
        }
    }
 </script>
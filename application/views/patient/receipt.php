<?php 
$getMsg = (urldecode(@$_GET['msg'])) ? urldecode(@$_GET['msg']) : "";
$successMessage = ($getMsg != '' && $getMsg == 'okay') ? true :false;
include_once 'template/header.php'; ?>

<!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

    <div class="wrapper">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="page-title-alt-bg"></div>
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="<?php echo base_url("vc/$userType/dashboard"); ?>"><?php echo mailConfig('company_name'); ?></a></li>
                        <li class="breadcrumb-item active">Receipt</li>
                    </ol>
                </div>
                <h4 class="page-title">Receipt Page</h4>
            </div> 
            <!-- end page title -->

            <div class="row">
            	<?php
            		// print_r($receipts);exit;
                if(empty(@$receipts)){ 
                    log_message('info',"there is no receipts found...");
                    ?>
                    <div class="row card-box">
                        <h4 class="alert alert-info">Sorry no receipt was found...</h4>
                    </div>
               <?php  return;}
                // print_r($receipts);exit;
            	 if(isset($receipts) && !empty(@$receipts)): ?>
            		<?php foreach($receipts as $key => $value){
                        ?>
                        <!-- this is the success message -->
                        <?php if($successMessage && $this->webSessionManager->getFlashMessage('success')): ?>
                        <div class="col-md-5">
                            <div class="alert alert-success alert-dismissible fade show ml-4">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4><?php echo $this->webSessionManager->getFlashMessage('success'); ?></h4>
                            </div>
                        </div>
                        <?php endif; ?>

                        <!-- this is where i design the receipts page for printing -->
                        <div class="col-md-5" style="margin:0 auto;">
                            <div class="card-box">
                                <!-- start print container -->
                                <div class="print-content" style="font-weight:bold;">
                                    <div class="clearfix">
                                        <div class="text-center border-bottom">
                                            <h4 class="m-0 d-print-none">
                                                PAYWINDAILY
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div>
                                                <p><b>Invoice No:</b>&nbsp;<?php echo $value['receipt_ref']; ?></p>
                                                <p style="margin-top:-15px;"><b>Name:</b>&nbsp;<?php echo $value['customer_name']; ?></p>
                                            </div>
                                        </div><!-- end col -->
                                        <div class="col-md-6">
                                            <div class="float-right">
                                                <p><strong>Bet Date : </strong> <span class="float-right">&nbsp; <?php echo dateFormatter($value['bet_date']);  ?></span></p>
                                                <?php if($userType == 'agent'): ?>
                                                <p style="margin-top: -15px;"><strong>Agent ID: </strong> <span class="float-right"><?php echo $value['agent_key']; ?></span></p>
                                                <?php endif; ?>
                                                <p style="margin-top:-15px;"><strong>Print Date : </strong> <span class="float-right"><span class="badge badge-danger"><?php date_default_timezone_set("Africa/Lagos"); echo dateFormatter(date('Y-m-d')); ?></span></span></p>
                                            </div>
                                        </div><!-- end col -->
                                    </div>
                                    <!-- end row -->

                                    <div class="row" style="margin-top: -18px;">
                                        <div class="col-12">
                                            <h6>Bet Details</h6>
                                            <div class="table-responsive">
                                                <table class="table table-centered">
                                                    <thead>
                                                        <tr>
                                                            <th class="no-padding">Qty</th>
                                                            <th class="no-padding">Company</th>
                                                            <th class="no-padding">Unit Price</th>
                                                            <th class="no-padding">Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><?php echo $value['qty'] ; ?></td>
                                                        <td><?php echo $value['company_name']; ?></td>
                                                        <td><?php echo $value['bet_unit']; ?></td>
                                                        <td><?php echo $value['amount']; ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div> <!-- end table-responsive -->
                                        </div> <!-- end col -->
                                    </div>
                                    <!-- end row -->

                                    <div class="row" style="margin-top:-35px;">
                                        <div class="col-md-6" style="margin-top: -10px;">
                                            <div class="clearfix">
                                                <h5>Amount in words:</h5>
                                                <p class="text-muted"><?php echo strtoupper(amountInWords($value['amount'])); ?> NAIRA ONLY</p>
                                                <?php if($userType != 'member'): ?>
                                                <p style="margin-top:-15px;">Issued by: <b><em><?php echo strtoupper($value['agent_name']); ?></em></b></p>
                                                <?php endif; ?>
                                            </div>
                                        </div> <!-- end col -->
                                        <div class="col-md-6" style="margin-top: -10px;">
                                            <div class="float-right">
                                                <!-- <p style="margin-top: 0;">Discount: <span>&nbsp; 0.00 </span></p> -->
                                                <p style="margin-top:0px;">Total Amount:</p><h4><?php echo getLocalCurrency(); ?><?php echo number_format($value['amount'],2); ?> </h4>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div> <!-- end col -->
                                    </div>
                                    <p class="text-center">Terms and conditions apply</p>
                                    <p class="text-center">Thank you for betting with us!</p>
                                    
                                    <style>
                                        .no-padding{
                                            padding:0;
                                        }
                                        @media print{
                                            body{
                                                margin-left:10px;
                                                margin-right:10px;
                                            }
                                        }
                                    </style>
                                    <!-- end row -->
                                <?php } ?>
                                </div>
                                <!-- end print container -->

                                <div class="mt-4 mb-1">
                                    <div class="text-right d-print-none">
                                        <a href="javascript:void(0);" class="btn btn-primary waves-effect waves-light print"><i class="mdi mdi-printer mr-1"></i> Print</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php else: ?>
                	<div class="alert alert-info alert-dismissible fade show ml-4">
                		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                		<h4>Sorry there seems not to be a record for the receipts...</h4>
                	</div>
            	<?php endif; ?>
            </div>
        </div>
   	</div>
<?php include_once 'template/footer.php'; ?>
<?php include_once 'template/header.php';

$showStatus = false;
$submitLabel = "Submit";
$exclude = array();
$healthModel = (isset($_GET['m'])) ? @$_GET['m'] : false;
if(!$healthModel){
    show_404();
}
$patientId = ($userType == 'patient') ? $this->webSessionManager->getCurrentUserProp('user_table_id') : "";
if($patientId == ''){
    exit("Sorry the patient doesn't exists");
}
$hidden = array('patient_id'=>$patientId);
?>
<?php if($healthModel == 'medication' || $healthModel == 'allergy' || @$healthModel == 'hospitalization' || @$healthModel == 'surgery' || @$healthModel == 'vaccination'): ?>
<link href="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<?php endif; ?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item">Health Profile</a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card border-left-blue">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Health Profile</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="media-list media-list-linked media-list-bordered mt-1 mb-1">
                        <li>
                            <?php
                                $profileImg = 'assets/images/users/avatar-4.jpg';
                                $profileImg = ($patient->patient_path != '') ? $patient->patient_path : $profileImg;
                            ?>
                            <a href="<?php echo base_url("vc/patient/profile_details/$patientId"); ?>" class="media">
                                <div class="mr-3">
                                    <img src="<?php echo base_url($profileImg); ?>" class="rounded-circle" width="44" height="44" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <h6 class="media-title"><?php echo $patient->firstname ." ".$patient->lastname; ?></h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item">Age: <?php 
                                                $yearDiff = getDateDifference($patient->dob,date('Y-m-d'));
                                                echo $yearDiff->format('%y') . "Years";
                                                 echo " (". $patient->dob .")" ; ?>, Sex: <?php $patient->gender; ?> </li>
                                            </ul>
                                        </div>
                                        <div class="col-6">
                                            <h6 class="media-title">Monitored Under: Not Assigned</h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item">Patient ID: <?php echo $patient->patient_key; ?> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                            </a>
                        </li>
                        <div class="card-footer bg-dark text-white d-flex justify-content-between align-items-center ml-1">
                            <div>
                                <label class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-envelop5"></i></b> <?php echo $patient->email; ?></label>      
                                <label class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-phone"></i></b> <?php echo format_phone('nig',$patient->phone_num); ?></label>      
                            </div>
                        </div>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-10 col-sm-12 col-xs-12" style="margin:0 auto;">
                                <div class="panel">
                                    <div class="panel-header border-bottom-1 mb-4">
                                        <h5>Add New <?php echo removeUnderscore($healthModel); ?></h5>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-lg-10">
                                            <?php
                                            $modelPath = ($healthModel == 'health_condition') ? base_url("mc/add/health_condition/1/null/1?a=1") : null;
                                            echo $this->modelFormBuilder->start($healthModel.'_table')
                                            ->appendInsertForm($healthModel,true,$hidden,'',$showStatus,$exclude)
                                            ->addSubmitLink($modelPath)
                                            ->appendResetButton('Reset','btn btn-outline bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg')
                                            ->appendSubmitButton($submitLabel,'btn btn-outline bg-pink-400 text-pink-400 border-pink-400 rounded-round btn-lg')
                                            ->build();
                                        ?>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <a href="<?php echo base_url('vc/patient/health_profile'); ?>" class="btn btn-dark float-right">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- this is add modal -->
                    <div class="row">
                        <div id="myModal_medication" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Medication Entry Data</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <?php 
                                                echo $this->modelFormBuilder->start('medication_table')
                                                ->appendInsertForm('medication',true,$hidden,'',$showStatus,$exclude)
                                                ->addSubmitLink()
                                                ->appendResetButton('Reset','btn btn-outline bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg')
                                                ->appendSubmitButton($submitLabel,'btn btn-outline bg-pink-400 text-pink-400 border-pink-400 rounded-round btn-lg')
                                                ->build();
                                            ?>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>

                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>

 <?php if($healthModel == 'medication' || $healthModel == 'allergy' || @$healthModel == 'hospitalization' || @$healthModel == 'surgery' || @$healthModel == 'vaccination'): ?>
 <script src="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php endif; ?>

<?php if($healthModel == 'hospitalization'): ?>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
<?php endif; ?>

 <script type="text/javascript" charset="utf-8" async defer>
    var inserted =false;
    function addMoreEvent(){
        loadFIleInput();
    }

    $('.modal').on('hidden.bs.modal', function (e) {
        if (inserted) {
          inserted = false;
          location.reload();
        }
    });
    $('.close').click(function(event) {
      if (inserted) {
        inserted = false;
        location.reload();
      }
    });

    function ajaxFormSuccess(target,data){
        if (data.status) {
          var path = data.message;
          inserted=true;
          showNotification(true,path);
          // location.reload(path);
        }
        else{
          showNotification(false,data.message);
        }
    }
 </script>
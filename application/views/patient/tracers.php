<?php include_once 'template/header.php'; 

$showStatus = false;
$submitLabel = "Submit";
$headerTitle = 'Health Tracers Monitor';
$exclude = array();
$tableExclude = array('patient_id','date_created');
$tableAttr = array();
$trackerModel = (isset($_GET['m'])) ? @$_GET['m'] : "blood_pressure";
$orderBy=' order by ID desc';
$patientId = ($userType == 'patient') ? $this->webSessionManager->getCurrentUserProp('user_table_id') : "";
if($patientId == ''){
    exit("Sorry the patient doesn't exists");
}
$tableAction = array("delete"=>"delete/$trackerModel","edit"=>"edit/$trackerModel");
$patientHash = rndEncode($patientId);
$hidden = array('patient_id'=>$patientId);
$where="where patient.id = '$patientId'";
?>

<?php if(@$trackerModel == 'blood_sugar' || @$trackerModel == 'exercise' || @$trackerModel == 'height' || @$trackerModel == 'mood_tracker' || @$trackerModel == 'sleep' || @$trackerModel == 'waist_measurement' || @$trackerModel == 'weight'): ?>
<link href="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<?php endif; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item"><?php echo removeUnderscore($trackerModel); ?></a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><?php echo $headerTitle; ?></h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">View <?php echo removeUnderscore($trackerModel); ?>(s)</h3>
                                    </div>
                                    <div class="panel-body">
                                        <a href="javascript:void(0);" data-toggle='modal' data-target='#myModal'>
                                            <button class="btn btn-labeled btn-labeled-right bg-primary waves-effect width-md float-right col-lg-4 col-sm-12 col-xs-12">
                                                <i class="icon icon-list"></i>  Add new <?php echo removeUnderscore($trackerModel); ?>(s)
                                            </button>
                                        </a>
                                        <div class="card card-table table-responsive shadow-0 mb-2">
                                            <?php
                                            if($trackerModel == 'blood_sugar' || $trackerModel == 'exercise' || $trackerModel == 'mood_tracker'){
                                                $tableExclude = array('patient_id');
                                            }
                                            echo $this->tableViewModel->getTableHtml($trackerModel,$count,$tableExclude,$tableAction,true,0,null,true,$orderBy,$where,array(),false,$tableAttr);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

            <!-- this is add modal -->
              <div class="row">
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"><?php echo removeUnderscore($trackerModel);  ?> Entry Form</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <p>
                                  <?php 
                                    $modelPath = null;
                                    if($trackerModel == 'blood_sugar'){
                                        $modelPath = base_url("mc/add/blood_sugar/1/null/1?a=1");
                                    }else if($trackerModel == 'exercise'){
                                        $modelPath = base_url("mc/add/exercise/1/null/1?a=1");
                                    }
                                    else if($trackerModel == 'height'){
                                        $modelPath = base_url("mc/add/height/1/null/1?a=1");
                                    }
                                    else if($trackerModel == 'mood_tracker'){
                                        $modelPath = base_url("mc/add/mood_tracker/1/null/1?a=1");
                                    }
                                    else if($trackerModel == 'sleep'){
                                        $modelPath = base_url("mc/add/sleep/1/null/1?a=1");
                                    }
                                    else if($trackerModel == 'waist_measurement'){
                                        $modelPath = base_url("mc/add/waist_measurement/1/null/1?a=1");
                                    }
                                  echo $this->modelFormBuilder->start($trackerModel.'_table')
                                    ->appendInsertForm($trackerModel,true,$hidden,'',$showStatus,$exclude)
                                    ->addSubmitLink($modelPath)
                                    ->appendResetButton('Reset','btn btn-outline bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg')
                                    ->appendSubmitButton($submitLabel,'btn btn-outline bg-pink-400 text-pink-400 border-pink-400 rounded-round btn-lg')
                                    ->build(); ?>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
              </div>
              <!-- this is for the edit -->
                <div class="row">
                  <div id="modal-edit" class="modal fade animated" role="dialog">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Update Entry</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                              </div>
                              <div class="modal-body">
                                  <p id="edit-container">

                                </p>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
                                  </button>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>

 <?php include_once 'template/footer.php'; ?>

<?php if(@$trackerModel == 'blood_sugar' || @$trackerModel == 'exercise' || @$trackerModel == 'height' || @$trackerModel == 'mood_tracker' || @$trackerModel == 'sleep' || @$trackerModel == 'waist_measurement' || @$trackerModel == 'weight'): ?>
 <script src="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php endif; ?>

<?php if($trackerModel == 'blood_pressure' || @$trackerModel == 'blood_sugar' || @$trackerModel == 'oxygen_level' || @$trackerModel == 'pulse' || @$trackerModel == 'temperature' || @$trackerModel == 'cholesterol'): ?>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
<?php endif; ?>

<?php if($trackerModel == 'mood_tracker'): ?>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/sliders/ion_rangeslider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/ui/moment/moment_locales.min.js"></script>
<script type="text/javascript" charset="utf-8" async defer>
    $('#mood_status').ionRangeSlider({
        grid: true,
        from: 0,
        step: 1,
        values: [
            'Depressed','Sad','Unhappy','Fine','Happy','Excellent'
        ]
    });
</script>
<?php endif; ?>

 <script type="text/javascript" charset="utf-8" async defer>
    var inserted =false;
    var fileTypeAllow = ["jpeg","pdf","png", "txt","doc",'xls','ppt'];

    function addMoreEvent(){
        loadSugarTime();
        loadFIleInput(fileTypeAllow);
    }

    function loadSugarTime(){
        $('#bs_time').pickatime({
            clear: '',
            interval: 30,
            formatSubmit: 'HH:i',
            editable:true,
        });
        $('#ex_time').pickatime({
            clear: '',
            interval: 30,
            formatSubmit: 'HH:i',
            editable:true,
        });
        $('#h_time').pickatime({
            clear: '',
            interval: 30,
            formatSubmit: 'HH:i',
            editable:true,
        });
        $('#s_time').pickatime({
            clear: '',
            interval: 30,
            formatSubmit: 'HH:i',
            editable:true,
        });
        $('#w_time').pickatime({
            clear: '',
            interval: 30,
            formatSubmit: 'HH:i',
            editable:true,
        });
        $('#weight_time').pickatime({
            clear: '',
            interval: 30,
            formatSubmit: 'HH:i',
            editable:true,
        });
        // var currentTime = new Date().getTime();
        // tpicker = timepicker.pickatime('picker');
        // tpicker.set('view', '4:30 am', { format: 'hh-i' })
    }

    $('.modal').on('hidden.bs.modal', function (e) {
        if (inserted) {
          inserted = false;
          location.reload();
        }
    });
    $('.close').click(function(event) {
      if (inserted) {
        inserted = false;
        location.reload();
      }
    });

    $('li[data-ajax-edit=1] a').click(function(event){
        event.preventDefault();
        var link = $(this).attr('href');
        var action = $(this).text();
        sendAjax(null,link,'','get',showUpdateForm);
    });

    function showUpdateForm(target,data) {
      var data = JSON.parse(data);
      if (data.status==false) {
        showNotification(false,data.message);
        return;
      }
       var container = $('#edit-container');
       container.html(data.message);
       //rebind the autoload functions inside
       $('#modal-edit').modal();
    }

    function ajaxFormSuccess(target,data){
        if (data.status) {
          var path = data.message;
          inserted=true;
          showNotification(true,path);
          // location.reload(path);
        }
        else{
          showNotification(false,data.message);
        }
    }
 </script>
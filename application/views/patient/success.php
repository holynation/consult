<?php 
$successMessage = ($this->webSessionManager->getFlashMessage('success')) ? $this->webSessionManager->getFlashMessage('success') :"";
include_once 'template/header.php'; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item">Success Page</a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Success Page</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="text-center">
                            <i class="icon-checkmark-circle2" style="font-size:50px;"></i>
                            <h3><?php echo $successMessage; ?></h3>
                            <?php
                            $getModel = (isset($_GET['m'])) ? @$_GET['m'] : "";
                            $getRef = (isset($_GET['r'])) ? urlencode(@$_GET['r']) : "";
                            $getUser = (isset($_GET['u'])) ? rndDecode(@$_GET['u']) : "";
                            $getAppointID = (isset($_GET['book_id'])) ? urlencode(@$_GET['book_id']) : "";
                            $paramRef = ("$getUser?book_id=$getAppointID&ref=$getRef");
                            if($getModel != '' && $getModel == 'appointment'){ ?>
                                <a href="<?php echo base_url("vc/patient/chat/$paramRef/"); ?>" class="btn btn-labeled bg-primary waves-effect"> Proceed to Chat
                                </a>
                           <?php  } ?>

                        </div>
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>
<?php include_once 'template/header.php'; 

$showStatus = false;
$submitLabel = "Submit";
$headerTitle = removeUnderscore($model);
$exclude = array();
$tableExclude = array('patient_id','date_created');
$tableAttr = array();
$orderBy=' order by ID desc';
$healthPofile = array('health_condition','medication','allergy','social_history','family_history','contraindications','hospitalization','surgery','vaccination');
$patientId = ($userType == 'patient') ? $this->webSessionManager->getCurrentUserProp('user_table_id') : "";
if($patientId == ''){
    exit("Sorry the patient doesn't exists");
}
$patientHash = rndEncode($patientId);
$hidden = array('patient_id'=>$patientId);
$where="where patient.id = '$patientId'";
?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item">Health Profile</a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card border-left-blue">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Health Profile</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="media-list media-list-linked media-list-bordered mt-1 mb-1">
                        <li>
                            <?php
                                $profileImg = 'assets/images/users/avatar-4.jpg';
                                $profileImg = ($patient->patient_path != '') ? $patient->patient_path : $profileImg;
                            ?>
                            <a href="<?php echo base_url("vc/patient/profile/$patientId"); ?>" class="media">
                                <div class="mr-3">
                                    <img src="<?php echo base_url($profileImg); ?>" class="rounded-circle" width="44" height="44" alt="">
                                </div>
                                <div class="media-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <h6 class="media-title"><?php echo $patient->firstname ." ".$patient->lastname; ?></h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item">Age: <?php 
                                                $yearDiff = getDateDifference($patient->dob,date('Y-m-d'));
                                                echo $yearDiff->format('%y') . "Years";
                                                 echo " (". $patient->dob .")" ; ?>, Sex: <?php $patient->gender; ?> </li>
                                            </ul>
                                        </div>
                                        <div class="col-6">
                                            <h6 class="media-title">Monitored Under: Not Assigned</h6>
                                            <ul class="list-inline list-inline-dotted text-muted mb-2">
                                                <li class="list-inline-item">Patient ID: <?php echo $patient->patient_key; ?> </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                            </a>
                        </li>
                        <div class="card-footer bg-dark text-white d-flex justify-content-between align-items-center ml-1">
                            <div>
                                <label class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-envelop5"></i></b> <?php echo $patient->email; ?></label>      
                                <label class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-phone"></i></b> <?php echo format_phone('nig',$patient->phone_num); ?></label>      
                            </div>
                        </div>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                        <?php foreach($healthPofile as $health): ?>
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel">
                                    <div class="panel-body">
                                        <a href="<?php echo base_url("vc/patient/health_profile_create?m=$health&u=$patientHash"); ?>" class="btn btn-labeled btn-labeled-right bg-primary waves-effect width-md float-right mb-1 col-lg-4 col-sm-12 col-xs-12">
                                            <i class="icon icon-add"></i>  <?php echo removeUnderscore($health); ?> (s)
                                        </a>
                                        <div class="card card-table table-responsive shadow-0 mb-2">
                                            <?php
                                            if($health == 'medication'){
                                                $tableExclude = (!empty($tableExclude)) ? array_merge(array('side_effects','medicine_form_id'),$tableExclude) : $tableExclude;
                                            }
                                            // else if($health == 'hospitalization'){
                                            //     $tableExclude = (!empty($tableExclude)) ? array_merge(array('hospitalization_path','medicine_form_id'),$tableExclude) : $tableExclude;
                                            // }
                                            $tableAction = array("delete"=>"delete/$health","edit"=>"edit/$health");
                                            echo $this->tableViewModel->getTableHtml($health,$count,$tableExclude,$tableAction,true,0,null,true,$orderBy,$where,array(),false,$tableAttr);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- this is for the edit -->
            <div class="row">
              <div id="modal-edit" class="modal fade animated" role="dialog">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title">Update Entry</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                          </div>
                          <div class="modal-body">
                              <p id="edit-container">

                            </p>
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>
 <script type="text/javascript" charset="utf-8" async defer>
    var inserted =false;
    function addMoreEvent(){
        // loadDataTable();
    }
    function ajaxFormSuccess(target,data){
        if (data.status) {
          var path = data.message;
          inserted=true;
          showNotification(true,path);
          // location.reload(path);
        }
        else{
          showNotification(false,data.message);
        }
    }

    $('.modal').on('hidden.bs.modal', function (e) {
        if (inserted) {
          inserted = false;
          location.reload();
        }
    });
    $('.close').click(function(event) {
      if (inserted) {
        inserted = false;
        location.reload();
      }
    });
    $('li[data-ajax-edit=1] a').click(function(event){
        event.preventDefault();
        var link = $(this).attr('href');
        var action = $(this).text();
        sendAjax(null,link,'','get',showUpdateForm);
    });

    function showUpdateForm(target,data) {
      var data = JSON.parse(data);
      if (data.status==false) {
        showNotification(false,data.message);
        return;
      }
       var container = $('#edit-container');
       container.html(data.message);
       //rebind the autoload functions inside
       $('#modal-edit').modal();
    }

    function loadDataTable(){
        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            paginate: false,
            columnDefs: [{ 
                orderable: false,
                width: 100,
                targets: [ 5 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_', 
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });
        // Scrollable datatable
        var table = $('.datatable-scroll-y').DataTable({
            autoWidth: true,
            scrollY: 200
        });
    }
 </script>
<?php include_once 'template/header.php'; 

$showStatus = false;
$submitLabel = "Submit";
$headerTitle = 'Health Journal';
$exclude = array();
$tableExclude = array('patient_id','date_created');
$tableAttr = array();
$trackerModel = (isset($_GET['m'])) ? @$_GET['m'] : "health_journal";
$orderBy=' order by ID desc';
$patientId = ($userType == 'patient') ? $this->webSessionManager->getCurrentUserProp('user_table_id') : "";
if($patientId == ''){
    exit("Sorry the patient doesn't exists");
}
$tableAction = array("delete"=>"delete/$trackerModel","edit"=>"edit/$trackerModel");
$patientHash = rndEncode($patientId);
$hidden = array('patient_id'=>$patientId);
$where="where patient.id = '$patientId'";
?>

<?php if(@$trackerModel == 'health_journal'): ?>
<link href="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<?php endif; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item"><?php echo removeUnderscore($trackerModel); ?></a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <?php
              // print_r($settings);exit;
              $journalSetting=false;
              $journalSettingVal = 1;
              $journalAccess = 'Public';
              $journalText = "By making your health journal public, healthcare providers will be able to view your new updates and the details you already entered.";
              $journalTitle = "This health journal updates are currently private and viewable by you only.You can change it here.";
              $journalIcon = "icon-lock5";
              $journalClick = "";
              if(isset($settings)){
                $journalSetting = ($settings['health_journal'] == 1) ? true: false;
                if($journalSetting){
                  $journalSettingVal = 0;
                  $journalAccess = 'Private';
                  $journalText = "Your health journal updates will be visible to you only and not to your healthcare providers.";
                  $journalTitle = "Your health journal updates are currently visible to all your healthcare providers.";
                  $journalIcon = "icon-eye2";
                  $journalClick = "to make your updates private again.";
                }
              }
            ?>

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><?php echo $headerTitle; ?></h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <p class="panel-title"><i class="<?php echo $journalIcon; ?>"></i> <?php echo $journalTitle; ?><a href="javascript:void(0);" data-toggle="modal" data-target="#small_modal">Click Here </a><?php echo $journalClick; ?></p>
                                    </div>
                                    <div class="panel-body">
                                        <div class="card shadow-0 mb-3">
                                          <div class="card-body">
                                            <ul class="media-list">
                                              <li class="media">
                                                <div class="mr-3">
                                                  <a href="#">
                                                    <img src="<?php echo base_url($profileImage); ?>" class="rounded-circle" width="80" height="80" alt="patient image">
                                                  </a>
                                                </div>
                                                <div class="media-body">
                                                  <div style="margin-top:-10px;">
                                                    <?php 
                                                      $modelPath = null;
                                                    echo $this->modelFormBuilder->start($trackerModel.'_table')
                                                      ->appendInsertForm($trackerModel,true,$hidden,'',$showStatus,$exclude)
                                                      ->addSubmitLink($modelPath)
                                                      ->appendSubmitButton($submitLabel,'btn btn-outline bg-pink-400 text-pink-400 border-pink-400 rounded-round btn-lg float-right')
                                                      ->build(); 
                                                    ?>
                                                    </div>
                                                </div>
                                              </li>
                                            </ul>
                                          </div>
                                        </div>
                                        <?php
                                          if(isset($journals) && !empty($journals)){ 
                                              foreach($journals as $journal){
                                          ?> 
                                        <div class="card shadow-0 mb-3">
                                          <div class="card-body">
                                            <ul class="media-list media-chat mb-3">
                                              <li class="media">
                                                <div class="mr-3">
                                                  <a href="javascript:void(0)">
                                                    <img src="<?php echo base_url($profileImage); ?>" class="rounded-circle" width="40" height="40" alt="">
                                                  </a>
                                                </div>

                                                <div class="media-body">
                                                  <div class="media-chat-item"><?php echo $journal['description']; ?></div>
                                                  <div class="font-size-sm text-muted mt-0"><?php echo dateTimeFormatter($journal['date_created'],12); ?> <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a>&nbsp;&nbsp; ||
                                                  <?php
                                                    $journalId = $journal['ID'];
                                                    $link = 'javascript:void(0);';
                                                    $fileMsg = '';
                                                    $icon = "icon-files-empty";
                                                    if(!empty($journal['health_journal_path'])){
                                                      $typeExt = getFileExtension($journal['health_journal_path'],true);
                                                      $link = $journal['health_journal_path'];
                                                      print_r($typeExt);
                                                      switch ($typeExt) {
                                                        case 'txt':
                                                          $fileMsg = "Text";
                                                          $icon = "icon-file-text";
                                                          break;
                                                        case 'doc';
                                                        case 'docx';
                                                          $fileMsg = "Doc";
                                                          $icon = "icon-file-word";
                                                          break;
                                                        case 'pdf':
                                                          $fileMsg = "Pdf";
                                                          $icon = "icon-file-pdf";
                                                          break;
                                                        case 'jpeg':
                                                          $fileMsg = "Image";
                                                          $icon = "icon-file-picture";
                                                          break;
                                                        
                                                        default:
                                                          # code...
                                                          break;
                                                      }
                                                    }
                                                  ?>
                                                    <a href='<?php echo base_url($link); ?>' target='_blank' class="ml-1"><?php echo "View ". $fileMsg. " File"; ?> <i class="<?php echo $icon; ?> ml-2 text-muted"></i></a>
                                                  </div>
                                                  <div class="float-right">
                                                    <a class="btn btn-dark bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg float-right" href="<?php echo base_url("edit/$trackerModel/$journalId"); ?>" id="journal_edit_<?php echo $journalId; ?>">EDIT</a>
                                                    <a class="btn btn-dark bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg float-right" href="<?php echo base_url("delete/$trackerModel/$journalId"); ?>" id="journal_delete_<?php echo $journalId; ?>">DELETE</a>
                                                  </div>
                                                </div>
                                              </li>
                                            </ul>
                                          </div>
                                        </div>
                                        <script type="text/javascript" charset="utf-8" async defer>
                                          function journalAction(){
                                            $('#journal_edit_<?php echo $journalId; ?>').click(function(event){
                                              event.preventDefault();
                                              var link = $(this).attr('href');
                                              var action = $(this).text();
                                              sendAjax(null,link,'','get',showUpdateForm);
                                            });

                                            $('#journal_delete_<?php echo $journalId; ?>').click(function(event){
                                                event.preventDefault();
                                                var link = $(this).attr('href');
                                                var action = $(this).text();
                                                if (confirm("are you sure you want to "+action+" item?")) {
                                                  sendAjax(null,link,'','get');
                                                }
                                              });
                                            }
                                          journalAction();
                                        </script>
                                        <hr class="border-teal" />
                                      <?php  } } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

              <!-- this is for the edit -->
                <div class="row">
                  <div id="modal-edit" class="modal fade animated" role="dialog">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Update Entry</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                              </div>
                              <div class="modal-body">
                                  <p id="edit-container"></p>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
                                  </button>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>

                <div id="small_modal" class="modal fade animated" role="dialog">
                  <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Health Journal Privacy Settings</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body">
                            <p><?php echo $journalText; ?></p>
                            <form action="<?php echo base_url("mc/update/settings/$patientId/1"); ?>" method="post" accept-charset="utf-8" id="journalForm">
                              <input type="hidden" name="health_journal" id="health_journal" value="<?php echo $journalSettingVal; ?>">
                              <input type="hidden" name="patient_id" id="patient_id" value="<?php echo $patientId; ?>">
                              <button type="submit" class="btn btn-primary" id="makePubic">Make It <?php echo $journalAccess ;?>
                              </button>
                              <button type="button" class="btn btn-default" data-dismiss="modal">Don't Change
                              </button>
                            </form>
                          </div>
                      </div>
                  </div>
                </div>

 <?php include_once 'template/footer.php'; ?>

<?php if(@$trackerModel == 'health_journal'): ?>
 <script src="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php endif; ?>

<?php if($trackerModel == 'health_journal'): ?>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
<?php endif; ?>

 <script type="text/javascript" charset="utf-8" async defer>
    var inserted =false
    function addMoreEvent(){
        loadSugarTime();
        loadFIleInput(['txt','jpeg','doc','docx','pdf']);
        changeDesc();
    }

    $('#journalForm').submit(function(e){
        e.preventDefault();
        submitAjaxForm($(this));
    });

    function loadSugarTime(){
        $('#bs_time').pickatime({
            clear: '',
            interval: 30,
            formatSubmit: 'HH:i',
            editable:true,
        });
    }

    function settingsRun(){
        $('.status-btn').click(function(event) {
            var target = $(this).attr('data-target'); //returns  the id
            var form = $('#appointment_status');
            var action =form.attr('action');
            var str = 'appointment';
            var len = str.length;
            var pos = action.indexOf(str);
            if (pos===false) {
              return;
            }
            var concern = action.substring(0,pos+len);
            var link = concern+'/'+target+'/1';
            form.attr('action', link);
            $('#small_modal').modal();
        });
    }

    function changeDesc(){
      $('#journal_description').on('focusin', function(){
        var $this = $(this);
        $this.css({
          'font-size': 12.9,
          'height': 120,
          'font-weight': 'normal'
        }).addClass('border-teal border-1');
      });

      $('#journal_description').on('focusout', function(){
        var $this = $(this);
        $this.css({
          'font-size': 20,
          'height': 45,
          'font-weight': 'bold'
        }).removeClass('border-teal border-1');
      });
    }

    $('.modal').on('hidden.bs.modal', function (e) {
        if (inserted) {
          inserted = false;
          location.reload();
        }
    });
    $('.close').click(function(event) {
      if (inserted) {
        inserted = false;
        location.reload();
      }
    });
    
    function showUpdateForm(target,data) {
      var data = JSON.parse(data);
      if (data.status==false) {
        showNotification(false,data.message);
        return;
      }
       var container = $('#edit-container');
       container.html(data.message);
       //rebind the autoload functions inside
       $('#modal-edit').modal();
    }

    function ajaxFormSuccess(target,data){
      if (data.status) {
        var path = data.message;
        inserted=true;
        location.reload();
      }
      else{
        showNotification(false,data.message);
      }
    }
 </script>
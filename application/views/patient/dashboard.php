<?php include_once 'template/header.php'; ?>

     <!-- i wanna show here any appointment a patient had scheduled on the system with a doctor,
    so that the chat can be initiated, meaning i have to also perform validation to check if their is a schedule of patient that is still pending or not responded too yet.
      -->
        
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url('vc/patient/dashboard'); ?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item">Link</a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h6 class="card-title">Showing: Healthcare Providers</h6>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <?php if($doctors): ?>
                    <ul class="media-list media-list-linked media-list-bordered">
                        <?php foreach($doctors as $doc): ?>
                        <li>
                            <?php
                                $profileImg = 'assets/images/users/avatar-4.jpg';
                                $profileImg = ($doc->doctor_path != '') ? $doc->doctor_path : $profileImg;
                                $doctorID = rndEncode($doc->ID);
                            ?>
                            <a href="<?php echo base_url("vc/doctor/profile_details/$doctorID"); ?>" class="media">
                                <div class="mr-3">
                                    <img src="<?php echo base_url($profileImg); ?>" class="rounded-circle" width="44" height="44" alt="">
                                </div>
                                <div class="media-body">
                                    <h6 class="media-title"><?php echo $doc->firstname ." ".$doc->lastname; ?></h6>
                                    <ul class="list-inline list-inline-dotted text-muted mb-2">
                                        <li class="list-inline-item"><i class="icon-office mr-2"></i><?php echo $doc->department_id; ?></li>
                                    </ul>
                                    <?php echo $doc->specialty_id; ?>
                                </div>
                            </a>
                        </li>
                        <div class="card-footer bg-dark text-white d-flex justify-content-between align-items-center">
                            <div>
                                <a href="<?php echo base_url("vc/create/text_consultation/single/$doctorID/question"); ?>" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-comment"></i></b> Question Consultation</a>
                                <a href="<?php echo base_url("vc/create/appointment/single/$doctorID/chat?book_type=checkout"); ?>" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-comment-discussion"></i></b> Chat Consultation</a>          
                                <a href="" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-video-camera "></i></b> Video Consultation</a>             
                                <a href="" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-phone"></i></b> Phone Consultation</a>        
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

 <?php include_once 'template/footer.php'; ?>
<?php include_once 'template/header.php'; 

$showStatus = false;
$submitLabel = "Submit";
$headerTitle = 'Appointments';
$exclude = array();
$tableExclude = array('patient_id','date_created');
$tableAttr = array();
$trackerModel = (isset($_GET['m'])) ? @$_GET['m'] : "appointment";
$orderBy=' order by ID desc';
$patientId = ($userType == 'patient') ? $this->webSessionManager->getCurrentUserProp('user_table_id') : "";
if($patientId == ''){
    exit("Sorry the patient doesn't exists");
}
$tableAction = array("delete"=>"delete/$trackerModel","edit"=>"edit/$trackerModel");
$patientHash = rndEncode($patientId);
$hidden = array('patient_id'=>$patientId);
$where="where patient.id = '$patientId'";
?>

<?php if(@$trackerModel == 'appointment'): ?>
<link href="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<?php endif; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <a href="#" class="breadcrumb-item"><?php echo removeUnderscore($trackerModel); ?></a>
                            <span class="breadcrumb-item active">Current</span>
                        </div>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
                <!-- Basic card -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><?php echo $headerTitle; ?></h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="reload"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <div class="panel">
                                    <div class="panel-body">
                                        <?php
                                        // print_r($appoints);exit;
                                          if(isset($appoints) && !empty($appoints)){ 
                                              foreach($appoints as $appoint){
                                                $appointTime = $appoint['appoint_date'].' '.$appoint['appoint_time'];
                                                $appointId = $appoint['ID'];
                                                $appointStatus = $appoint['appoint_status'];
                                                $docID = rndEncode($appoint['doc_id']);
                                          ?> 
                                        <div class="card shadow-0 mb-3">
                                          <div class="card-body">
                                            <ul class="media-list media-chat mb-3">
                                              <li class="media">
                                                <div class="mr-3">
                                                  <a href="javascript:void(0)">
                                                    <img src="<?php echo base_url($appoint['doctor_path']); ?>" class="rounded-circle" width="50" height="50" alt="">
                                                  </a>
                                                </div>

                                                <div class="media-body">
                                                  <h6 class="media-title"><a href="<?php echo base_url("vc/doctor/profile_details/$docID"); ?>"><b>Dr. <?php echo $appoint['doc_name']; ?></b></a></h6>
                                                  <div class="media-chat-item"><?php echo $appoint['name']; ?></div>
                                                  <div class="font-size-sm text-muted mt-0"><?php echo localTimeRead($appointTime,12); ?> <a href="#"><i class="icon-pin-alt ml-2 text-muted"></i></a>
                                                  </div>
                                                <?php if($appointStatus == 'pending'): ?>
                                                  <div class="float-right">
                                                    <a class="btn btn-dark bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg float-right" href="<?php echo base_url("edit/$trackerModel/$appointId"); ?>" id="appoint_edit_<?php echo $appointId; ?>">EDIT</a>
                                                    <a class="btn btn-dark bg-dark-400 text-dark-400 border-dark-400 rounded-round btn-lg float-right" href="<?php echo base_url("delete/$trackerModel/$appointId"); ?>" id="appoint_delete_<?php echo $appointId; ?>">DELETE</a>
                                                  </div>
                                                <?php endif; ?>
                                                </div>
                                                <div class="ml-3">
                                                  <?php
                                                    $spanClass='';
                                                    $priceStatus='Paid';
                                                    $hrStatus='';
                                                    switch ($appointStatus) {
                                                      case 'pending':
                                                        $spanClass = 'bg-dark border-dark';
                                                        $hrStatus = 'border-dark';
                                                        break;
                                                      case 'active':
                                                        $spanClass = 'bg-success border-success';
                                                        $hrStatus = 'border-success';
                                                        break;
                                                        case 'cancelled':
                                                        $spanClass = 'bg-danger border-danger';
                                                        $price = 'UnPaid';
                                                        $hrStatus = 'border-danger';
                                                        break;
                                                        case 'success':
                                                        $spanClass = 'bg-primary border-primary';
                                                        $hrStatus = 'border-primary';
                                                        break;
                                                    }
                                                  ?>
                                                  <span class="badge badge-mark <?php echo $spanClass; ?>"></span> <?php echo ucfirst($appointStatus); ?>
                                                  <span class="text-muted"><?php echo $priceStatus ?> NGN <?php echo $appoint['price']; ?></span>
                                                </div>
                                              </li>
                                            </ul>
                                          </div>
                                        </div>
                                        <script type="text/javascript" charset="utf-8" async defer>
                                          function journalAction(){
                                            $('#appoint_edit_<?php echo $appointId; ?>').click(function(event){
                                              event.preventDefault();
                                              var link = $(this).attr('href');
                                              var action = $(this).text();
                                              sendAjax(null,link,'','get',showUpdateForm);
                                            });

                                            $('#appoint_delete_<?php echo $appointId; ?>').click(function(event){
                                              event.preventDefault();
                                              var link = $(this).attr('href');
                                              console.log(link)
                                              var action = $(this).text();
                                              if (confirm("are you sure you wannna to "+action+" item?")) {
                                                sendAjax(null,link,'','get');
                                              }
                                            });
                                          }

                                          journalAction();
                                        </script>
                                        <hr class="<?php echo $hrStatus; ?>" />
                                      <?php  } }else{  ?>
                                          <div class="alert alert-info">
                                              <p>Sorry you have no appointment at the moment....</p>
                                          </div>
                                      <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /basic card -->
            </div>
            <!-- /content area -->

              <!-- this is for the edit -->
                <div class="row">
                  <div id="modal-edit" class="modal fade animated" role="dialog">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title">Update Entry</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                              </div>
                              <div class="modal-body">
                                  <p id="edit-container"></p>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
                                  </button>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>

 <?php include_once 'template/footer.php'; ?>

<?php if(@$trackerModel == 'appointment'): ?>
 <script src="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<?php endif; ?>

<?php if($trackerModel == 'appointment'): ?>
<script src="<?php echo base_url(); ?>assets/global/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
<?php endif; ?>

 <script type="text/javascript" charset="utf-8" async defer>
    var inserted =false
    function addMoreEvent(){
        loadFIleInput();
    }

    $('.modal').on('hidden.bs.modal', function (e) {
        if (inserted) {
          inserted = false;
          location.reload();
        }
    });
    $('.close').click(function(event) {
      if (inserted) {
        inserted = false;
        location.reload();
      }
    });
    
    function showUpdateForm(target,data) {
      var data = JSON.parse(data);
      if (data.status==false) {
        showNotification(false,data.message);
        return;
      }
       var container = $('#edit-container');
       container.html(data.message);
       //rebind the autoload functions inside
       $('#modal-edit').modal();
    }

    function ajaxFormSuccess(target,data){
      if (data.status) {
        var path = data.message;
        inserted=true;
        location.reload();
      }
      else{
        showNotification(false,data.message);
      }
    }
 </script>
<?php $base=base_url();  
include_once 'template/header.php';
?>

<!-- Main content -->
<div class="content-wrapper">
    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                    <a href="#" class="breadcrumb-item">Patient Profile</a>
                    <span class="breadcrumb-item active">Current</span>
                </div>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Patient Profile</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
            	<div class="row">
            		<div class="col-lg-12 col-sm-12 col-xs-12">
            			<ul class="nav nav-tabs nav-tabs-highlight nav-justified mb-0">
							<li class="nav-item"><a href="#bordered-justified-tab1" class="nav-link active" data-toggle="tab">Personal Profile</a></li>
							<li class="nav-item"><a href="#bordered-justified-tab2" class="nav-link" data-toggle="tab">Relative Details</a></li>
						</ul>
						<div class="tab-content card card-body border-top-0 rounded-0 rounded-bottom mb-0">
							<div class="tab-pane fade show active" id="bordered-justified-tab1">
								<div class="row">
									<!-- <div class="col-md-6"> -->
										<!-- Left sidebar -->
			                            <div class="col-md-6">
			                            	<?php if (@$patient->patient_path): ?>
			                            	<img src="<?php echo base_url(@$patient->patient_path); ?>" class="img-lg rounded-circle mb-2 ml-4" alt="profile picture">
			                            	<?php else: ?>
							                <img class="img-lg rounded-circle mb-2 ml-4" src="<?php echo base_url('assets/images/default-profile.jpg'); ?>" alt="profile picture">
							                <br /> 
							              	<?php endif; ?>
							              	<div class="showupload btn btn-primary btn-block btn-sm mt-3 mb-4" style="cursor: pointer;text-transform: uppercase;">change photo</div>
							              	<div>
							              		<p>Image size should not be nothing greater than 150 X 150</p>
							              	</div>
						                  	<div class="form mb-2">
						                  		<?php $userID = (isset($data['id']) && $data['id'] != '') ? $data['id'] : $patient->ID; ?>
							                    <div class="upload-control" style="display: none; width: 205px;margin:0 auto;">
							                      <form id="data_profile_change" method="post" enctype="multipart/form-data" action="<?php echo base_url('mc/update/patient/'.$userID.'/1') ?>">
							                      <label for="">
							                        choose file to upload <br>
							                        <input type="file" name="patient_path" id="patient_path" class="form-control">
							                      </label>
							                      <input type="submit" value="Upload Photo" name="submit-btn" class="btn btn-primary">
							                      </form>
							                    </div>
							                </div>
			                                <div class="border-top border-bottom pt-3 mb-0">
							                    <div class="col-6">
							                      <h6 class="font-weight-medium">Email</h6>
							                      <p><?php echo @$patient->email; ?></p>
							                    </div>
							                 </div>
			                                <div class="mt-2">
			                                	<a href="javascript:void(0);" class="btn btn-danger btn-block waves-effect waves-light" data-toggle='modal' data-target='#myModalPassword'>Change Password</a>
			                            	</div>
			                            </div>
			                            <!-- End Left sidebar -->

			                            <div class="col-md-6">
			                            	<div class="grid-margin stretch-card">
									        	<div class="card">
									        		<div class="card-body">
											        	<h4 class="card-title">Personal Biodata</h4>
											        	<div class="table-responsive">
											              <table class="table table-hover">
											              	<tbody>
											                <tr>
											                  <td>Surname</td>
											                  <td><?php echo $patient->lastname; ?></td>
											                </tr>
											                <tr>
											                  <td>Firstname</td>
											                  <td><?php echo $patient->firstname; ?></td>
											                </tr>
											                <tr>
											                  <td>Middlename</td>
											                  <td><?php echo $patient->middlename; ?></td>
											                </tr>
											                <tr>
											                  <td>Phone Number</td>
											                  <td><?php echo format_phone('nig',$patient->phone_num); ?></td>
											                </tr>
											                </tbody>
											              </table>
											            </div>
											        </div> 
									        	</div> 
									        </div>
			                            <!-- end row-->
			                            </div>
									<!-- </div> -->
								</div>
			                    <!-- end inbox-rightbar-->
			                    <div class="clearfix"></div>
							</div>

							<div class="tab-pane fade" id="bordered-justified-tab2">
								Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid laeggin.
							</div>
						</div>
            		</div>
            	</div>
				
			</div>

	        <!-- this is for the change password -->
	        <div class="row">
	          <div id="myModalPassword" class="modal fade animated" role="dialog">
	              <div class="modal-dialog">
	                  <div class="modal-content">
	                      <div class="modal-header">
	                          <h4 class="modal-title">Change your password</h4>
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                          <span aria-hidden="true">&times;</span></button>
	                      </div>
	                      <div class="modal-body">
	                      	<!-- this is the notification section -->
				                <div id="data_notify"></div> 
				              <!-- end notification -->
				              <form action="<?php echo base_url('vc/changePassword'); ?>" method="post" role="form" id="form_change_password" name="form_change_password">
				                <div class="form-group mb-3">
				                    <label for="current_password">Current Password</label>
				                    <input class="form-control border-dark border-1" type="password" required name="current_password" id="current_password" placeholder="Enter your current password">
				                </div>
				                <div class="form-group mb-3">
				                    <label for="password">New Password</label>
				                    <input class="form-control border-teal border-1" type="password" required name="password" id="password" placeholder="Enter your new password">
				                </div>
				                <div class="form-group mb-3">
				                    <label for="confirm_password">Confirm Password</label>
				                    <input class="form-control border-teal border-1" type="password" required name="confirm_password" id="confirm_password" placeholder="Enter your password again">
				                </div>
				                <div class="form-group">
				                  <button class="btn btn-primary submit-btn btn-block">Update</button>
				                </div>
				                <input type="hidden" name="userID" id="userID" value="<?php echo $userID; ?>" />
				                <input type="hidden" name="isajax">
				                <input type="hidden" id='base_path' value="<?php echo $base; ?>">
				                <input type="hidden" name="task" value="update">
				              </form>
	                      </div>
	                      <div class="modal-footer">
	                          <button type="button" class="btn btn-dark" id="close" data-dismiss="modal">Close
	                          </button>
	                      </div>
	                  </div>
	              </div>
	          </div>
	        </div>
        </div>
        <!-- /basic card -->
    </div>
    <!-- /content area -->
<script>
    function addMoreEvent() {
      $('.showupload').click(function(event) {
        $(this).hide();
        $('.upload-control').show();
      });

      $("#data_profile_change").submit(function(e){
        e.preventDefault();
        submitAjaxForm($(this));
       });

       var data_notify = $('#data_notify');
        $('#form_change_password').submit(function(e){
          e.preventDefault();
          var password = $('#password').val(),
              confirm_password = $('#confirm_password').val(),
              current_password = $('#current_password').val();

              if(password == '' || confirm_password == '' || password == ''){
                  data_notify.html('<p class="alert alert-danger" style="width:100%;margin:0 auto;">All Field is required...</p>');
                  return false;
              }
              else if(password != confirm_password ){
                data_notify.html('<p class="alert alert-danger" style="width:100%;margin:0 auto;">new password must match confirm password...</p>');
                return false;
              }else{
                submitAjaxForm($(this));
              }

        });
    }
  	function ajaxFormSuccess(target,data) {
  		showNotification(data.status,data.message);
  		if(data.status){
  			if (typeof target ==='undefined') {
	        	location.reload();
	    	}
  		}
  	}
</script>
<?php include_once 'template/footer.php'; ?>
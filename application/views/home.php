<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?php echo getTitlePage('Welcome'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Welcome to company betting" name="description" />
        <meta content="Qucik bet" name="Company bet" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

        <!-- App css -->
        <link href="<?php echo base_url(); ?>assets/private/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/custom.css" rel="stylesheet" type="text/css" />
    </head>
<body>
  <section id="banner">
      <!--
        ".inner" is set up as an inline-block so it automatically expands
        in both directions to fit whatever's inside it. This means it won't
        automatically wrap lines, so be sure to use line breaks where
        appropriate (<br />).
      -->
      <div class="inner">

        <header>
          <h2>Online <br/>Medical<br /> Consultation</h2>
        </header>
        <p>We <strong>provide</strong> you
        <br />
        the best platform
        <br />
        to consult medically online.</p>
        <footer>
          <ul class="buttons vertical">
            <li><a href="<?php echo base_url('auth/web'); ?>" class="btn btn-outline-info btn-block waves-effect waves-light width-md">Get started</a></li>
          </ul> 
        </footer>
      </div>
  </section>
       <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php echo date('Y'); ?> &copy; <a href="javascript:void(0);"><?php echo appConfig('company_name'); ?></a> 
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

        <!-- Vendor js -->
      <script src="<?php echo base_url(); ?>assets/global/js/main/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/global/js/main/bootstrap.bundle.min.js"></script>
    </body>
</html>

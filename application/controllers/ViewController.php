<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ViewController extends CI_Controller{

//field definition section
  private $needId= array();

  private $needMethod=array();
  private $errorMessage; // the error message currently produced from this cal if it is set, it can be used to produce relevant error to the user.
  private $access = array();
  private $schoolData;

  function __construct(){
		parent::__construct();
		$this->load->model("modelFormBuilder");
		$this->load->model("tableViewModel");
    $this->load->helper('string');
    $this->load->helper('array');
    $this->load->model('webSessionManager');
    $this->load->model('queryHtmlTableModel');
    $this->load->library('hash_created');
    $this->lang->load('payment');
    if (!$this->webSessionManager->isSessionActive()) {
      header("Location:".base_url());exit;
    }
	}
// bootsrapping functions 
  public function view($model,$page='index',$third='',$fourth=''){
    if ( !(file_exists("application/views/$model/") && file_exists("application/views/$model/$page".'.php')))
    {
      show_404();
    }

    // this number is the default arg that ID is the last arg i.e 3 = id
    $defaultArgNum =4; 
    $tempTitle = removeUnderscore($model);
    $title = $page=='index'?$tempTitle:ucfirst($page)." $tempTitle";
    //$schoolName = empty($this->session->userdata('schoolName'))?//till the school name getter is added
    $modelID = ($fourth == '') ? $third : $fourth;
    $data['id'] = urlencode($modelID);
    if (func_num_args() > $defaultArgNum) {
      $args = func_get_args();
      $this->loadExtraArgs($data,$args,$defaultArgNum);
    }

    // if ($this->webSessionManager->getCurrentUserProp('user_type')=='admin') {
    //   //check for the permission for the admin here
    // }

    $exceptions = array();//pages that does not need active session
    if (!in_array($page, $exceptions)) {
      if (!$this->webSessionManager->isSessionActive()) {
        redirect(base_url());exit;
      }
    }

    //get the name of the school set on the system
    $data['school']=$this->schoolData;
    if (method_exists($this, $model)) {
      $this->$model($page,$data);
    }
    $methodName = $model.ucfirst($page);

    if (method_exists($this, $model.ucfirst($page))) {
      $this->$methodName($data);
    }

    $data['model'] = $page;
    $data['message']=$this->session->flashdata('message');
    sendPageCookie($model,$page);

    return $this->load->view("$model/$page", $data);
  }

  private function admin($page,&$data)
  {
    $role_id=$this->webSessionManager->getCurrentUserProp('role_id');
    if (!$role_id) {
      show_404();
    }

    $this->load->model('custom/adminData');
    $role=false;
    if ($this->webSessionManager->getCurrentUserProp('user_type')=='doctor') {
      loadClass($this->load,'member');
      $this->member->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->member->load();
      $data['doctor']=$this->member;
      $role = $data['doctor']->role;
    }else {
      loadClass($this->load,'admin');
      $this->admin->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->admin->load();
      $data['admin']=$this->admin;
      $role = $this->admin->role;
    }
    $data['currentRole']=$role;
    if (!$role) {
      show_404();exit;
    }
    $path ='vc/admin/'.$page;

    // this approach is use so as to allow this page pass through using a page that is already permitted
    if($page=='permission' || $page == 'customer') {
      $path ='vc/add/role';
    }

    if($page == 'create'){
      $path = 'vc/create/events';
    }

    // end the permitted page already loaded
    if (!$role->canView($path)) {
      show_access_denied();exit;
    }


    $sidebarContent=$this->adminData->getCanViewPages($role);
    // print_r($sidebarContent);exit;
    $data['canView']=$sidebarContent;

  }

  private function adminDashboard(&$data)
  {
   $data = array_merge($data,$this->adminData->loadDashboardData());
  }

  private function adminPermission(&$data)
  {
    $data['id'] = urldecode($data['id']);
    if (!isset($data['id']) || !$data['id'] || $data['id']==1) {
      show_404();exit;
    }
    $newRole = new Role(array('ID'=>$data['id']));
    $newRole->load();
    $data['role']=$newRole;
    $data['allPages']=$this->adminData->getAdminSidebar();
    $sidebarContent=$this->adminData->getCanViewPages($data['role']);
    // print_r($sidebarContent);exit;
    $data['permitPages']=$sidebarContent;
    $data['allStates']=$data['role']->getPermissionArray();
  }

  private function adminProfile(&$data)
  {
    loadClass($this->load,'admin');
    $admin = new Admin();
    $admin->ID=$this->webSessionManager->getCurrentUserProp('user_table_id');
    $admin->load();
    $data['admin']=$admin;
  }

  private function adminReport(&$data)
  {
    // if($data['id']){
      $betYear = (@$_GET['bet_year']) ? urldecode(trim(@$_GET['bet_year'])) : "";
      $dateFilter = (@$_GET['date_filter']) ? urldecode(trim(@$_GET['date_filter'])) : "";
      if($dateFilter == '' || $betYear == ''){
        $backLink = base_url('vc/admin/report_option');
        exit("Please fill all the option field...<h4><a href='$backLink'>Go back</a></h4>");
      }
      $result = $this->adminData->getReports($dateFilter,$betYear,$configReport);
      // print_r($configReport);exit;
      if($result){
        $data['reports'] = $result;
        $data['configReport'] = $configReport;
      }else{
        $goBack = base_url('vc/admin/report_option');
        exit("NO BET YET, THUS REPORT CAN NOT BE DETERMINE... <a href='$goBack'>Go Back</a>");
      }
     
    // }
  }

  private function patient($page,&$data){
    $data['id'] = urldecode($data['id']);
    $this->load->model('custom/patientData');
    loadClass($this->load,'patient');
    $id = $this->webSessionManager->getCurrentUserProp('user_table_id');
    $this->patient = new Patient(array('ID'=>$id));
    $this->patient->load();
    $data['patient'] = $this->patient;
  }

  private function patientDashboard(&$data){
    if ($this->webSessionManager->getCurrentUserProp('user_type')=='admin') {
      $this->admin('dashboard',$data);
      if (!isset($data['id']) || !$data['id']) {
        show_404();exit;
      }
      $patient = new Patient(array('ID'=>$data['id']));
      if (!$patient->load()) {
        show_404();exit;
      }
      $data['patient']=$patient;
    }else if($this->webSessionManager->getCurrentUserProp('user_type')=='doctor'){
      $this->doctor('dashboard',$data);
      if (!isset($data['id']) || !$data['id']) {
        show_404();exit;
      }
      $patient = new Patient(array('ID'=>$data['id']));
      if (!$patient->load()) {
        show_404();exit;
      }
      $data['patient']=$patient;
    }
     // print_r($data);exit;
    loadClass($this->load, 'user');
    $data = array_merge($data,$this->patientData->loadDashboardInfo());
  }

  private function patientReceipt(&$data){
    $userType = $this->webSessionManager->getCurrentUserProp('user_type');
    $agentKey="";
    $agentName="";
    if($userType == 'admin'){
      $this->admin('receipt',$data);
      $agentKey = "";
      $agentName = $this->admin->firstname;
    }else if($userType == 'doctor'){
      $this->doctor('receipt',$data);
      $doctorKey = $this->doctor->doctor_key;
      $doctorName = $this->doctor->firstname;
    }else if($userType == 'patient'){
      // do something here
    }
    $data = array_merge($data,$this->patientData->loadReceipt($data['id'],$userType,$doctorKey,$doctorName));
  }

  private function patientSuccess(&$data){
    return $data;
  }

  private function patientPay_view(&$data){
    // i wanna check first whether the user had any payment available first against a doctor for some duration(if other doctor,they have to pay as long the appointment is not the doctor they pay initially to) and check if they wanna do it at that time or extend the chat time. There is time limit to their chat.
    $type = (@$_GET['book_type'] == 'chat_checkout') ? @$_GET['book_type'] : "";
    $service = (@$_GET['appointment'] == 'chat') ? @$_GET['appointment'] : "";
    if(!$type){
      die("Sorry you can't perform the operation...");
    }
    $arr = array();
    $arr['chat_type'] = $type;
    $data['id'] = rndDecode($data['id']);
    if($data['id']){
      $doctorID = $data['id'];
      loadClass($this->load,'doctor');
      $doc = $this->doctor->getWhere(array('ID'=> $doctorID),$count,0,1,false);
      $data['doctor'] = $doc[0];
    }
    if($service){
      $param = ($service == 'chat') ? "chat" : "";
      loadClass($this->load,'health_services');
      $service = $this->health_services->getService($param);
      $data['service_id'] = $service[0]['ID'];
      $data['service_fee'] = $service[0]['price'];
    }
    // print_r($data);exit;
    $data = array_merge($data,$arr);
  }

  private function patientProfile(&$data){
    if ($this->webSessionManager->getCurrentUserProp('user_type')=='admin') {
      $this->admin('profile',$data);
      if (!isset($data['id']) || !$data['id']) {
        show_404();exit;
      }
      $patient = new Patient(array('ID'=>$data['id']));
      if (!$patient->load()) {
        show_404();exit;
      }
      $data['patient']=$patient;
      $data['extra']=true;
    }
  }

  private function patientPatientCreate(&$data){
    $this->load->model("tableWithHeaderModel");
  }

  private function patientList_view(&$data){
    return $data = array_merge($data,$this->patientData->getJournal());
  }

  private function patientAppointment(&$data){
    return $data = array_merge($data,$this->patientData->getAppoint());
  }

  private function patientChat(&$data){
    // check here the ref in the transact history
    $getAppointID = (isset($_GET['book_id'])) ? rndDecode(urldecode(@$_GET['book_id'])) : "";
    $getRefID = (isset($_GET['ref'])) ? urldecode(@$_GET['ref']) : "";
    $userID = ($data['id']);
    if(!$getAppointID && $getRefID){
      exit("Sorry, something went wrong with the operation...");
    }
    loadClass($this->load,'appointment');
    $appointment = $this->appointment->getWhere(array('ID'=> $getAppointID),$count,0,1,false);
    if(!$appointment){
      exit("Sorry you don't have an appointment yet...");
    }
    $appointment = $appointment[0];
    $result = $this->patientData->loadChatHistory($userID,$appointment->doctor_id);
    if(!$result){
      echo "";
    }
    $data['chat_history'] = json_encode($result);
    $data['doctor_id'] = $appointment->doctor_id;
    $data['doctor_name'] = $appointment->doctor->firstname ." ".getFirstString($appointment->doctor->lastname,true)."." .getFirstString($appointment->doctor->middlename,true);
    $data['doctor_img'] = $appointment->doctor->doctor_path;
    $data['chat_type'] = "chat_form";
    // print_r($data);exit;
  }

  private function doctor($page,&$data){
    $data['id'] = urldecode($data['id']);
    $this->load->model('custom/doctorData');
    loadClass($this->load,'doctor');
    $id = $this->webSessionManager->getCurrentUserProp('user_table_id');
    $this->doctor = new Doctor(array('ID'=>$id));
    $this->doctor->load();
    $data['doctor'] = $this->doctor;
  }

  private function doctorDashboard(&$data)
  {
   $data = array_merge($data,$this->doctorData->loadDashboardData());
  }

  private function doctorProfile(&$data){
    if ($this->webSessionManager->getCurrentUserProp('user_type')=='admin') {
      $this->admin('profile',$data);
      if (!isset($data['id']) || !$data['id']) {
        show_404();exit;
      }
      $doctor = new Doctor(array('ID'=>$data['id']));
      if (!$doctor->load()) {
        show_404();exit;
      }
      $data['doctor']=$doctor;
      $data['extra']=true;
    }
  }
  private function doctorChat(&$data){
    $data['chat_type'] = "start_chat";
    if(isset($_GET['p']) && $_GET['p'] != ''){
      $userID = ($data['id']);
      $patientID = rndDecode($_GET['p']);
      $result = $this->doctorData->loadChatHistory($patientID,$userID);
      if(!$result){
        echo "";
      }
      $data['chat_history'] = json_encode($result);
      $patientData = $this->doctor->getPatientData($patientID);
      $data['patient_id'] = $patientID;
      $data['patient_img'] = $patientData['patient_path'];
      $data['patient_name'] = $patientData['firstname']." ".getFirstString($patientData['lastname'],true).".".getFirstString($patientData['middlename'],true);
      $data['chat_type'] = "chat_form";
      $data['currentAppoint'] = $this->doctorData->getNearestAppoint($patientID);
    }
    $data = array_merge($data,$this->doctorData->getNearestAppoint());
  }
  private function doctorProfile_details(&$data){
    $userType = $this->webSessionManager->getCurrentUserProp('user_type');
    if ($userType=='admin' || $userType == 'patient') {
      $modelClass = ($userType == 'admin') ? 'admin' : 'patient';
      $this->$modelClass('profile_details',$data);
      if (!isset($data['id']) || !$data['id']) {
        show_404();exit;
      }
      $decodeID = ($userType == 'patient') ? rndDecode($data['id']) : $data['id'];
      $doctor = new Doctor(array('ID'=>$decodeID));
      if (!$doctor->load()) {
        show_404();exit;
      }
      $data['doctor']=$doctor;
    }
  }

  private function doctorSchedule(&$data){
    return $data;
  }

  private function investor($page,&$data){
    $data['id'] = urldecode($data['id']);
    $this->load->model('custom/investorData');
    loadClass($this->load,'investor');
    $id = $this->webSessionManager->getCurrentUserProp('user_table_id');
    $this->investor = new Investor(array('ID'=>$id));
    $this->investor->load();
    $data['investor'] = $this->investor;
  }

  private function investorDashboard(&$data)
  {
    $id = $this->investor->ID;
   $data = array_merge($data,$this->investorData->loadDashboardData($id));
  }

  //function for loading edit page for general application
  function edit($model,$id){
    $userType=$this->webSessionManager->getCurrentUserProp('user_type');
    if($userType == 'admin'){
      loadClass($this->load,'admin');
      $this->admin->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->admin->load();
      $role = $this->admin->role;
      $role->checkWritePermission();
    }else{
      $role = true;
    }
    
    $ref = @$_SERVER['HTTP_REFERER'];
    if ($ref&&!startsWith($ref,base_url())) {
      show_404();
    }
    $this->webSessionManager->setFlashMessage('prev',$ref);
    $exceptionList= array('user');//array('user','applicant','student','staff');
    if (empty($id) || in_array($model, $exceptionList)) {
      show_404();exit;
    }
    $this->load->model('formConfig');
    $formConfig = new formConfig($role);
    $configData=$formConfig->getUpdateConfig($model);
    $exclude = ($configData && array_key_exists('exclude', $configData))?$configData['exclude']:array();
     $formContent= $this->modelFormBuilder->start($model.'_edit')
        ->appendUpdateForm($model,true,$id,$exclude,'')
        ->addSubmitLink(null,false)
        ->appendSubmitButton('Update','btn btn-success')
        ->build();
        echo createJsonMessage('status',true,'message',$formContent);exit;
  }

  function extra($model,$id,$_1){
    $role = true;

    $userType=$this->webSessionManager->getCurrentUserProp('user_type');
    if ($userType=='lecturer') {
      loadClass($this->load,'lecturer');
      $this->lecturer->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->lecturer->load();
    }
    else{
      loadClass($this->load,'admin');
      $this->admin->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->admin->load();
    }

    $ref = @$_SERVER['HTTP_REFERER'];
    if ($ref&&!startsWith($ref,base_url())) {
      show_404();
    }

    $this->webSessionManager->setFlashMessage('prev',$ref);
    $exceptionList= array('user');//array('user','applicant','student','staff');
    if (empty($id) || in_array($model, $exceptionList)) {
      show_404();exit;
    }
    // this is to set the form id of the original form
    $extraParam = array(
        'extra_model' => $id,
        'extra_id'    => $_1
    );
    // $this->webSessionManager->setContent('extra_id',$id);
    $this->webSessionManager->setArrayContent($extraParam);
    $this->load->model('formConfig');
    $formConfig = new formConfig($role);
    $configData=$formConfig->getUpdateConfig($model);
    $exclude = ($configData && array_key_exists('exclude', $configData))?$configData['exclude']:array();
    $hidden = ($configData && array_key_exists('hidden', $configData))?$configData['hidden']:array();
    $showStatus = ($configData && array_key_exists('show_status', $configData))?$configData['show_status']:false;
    $submitLabel = ($configData && array_key_exists('submit_label', $configData))?$configData['submit_label']:"Save";

     $formContent= $this->modelFormBuilder->start($model.'_table')
        ->appendInsertForm($model,true,$hidden,'',$showStatus,$exclude)
        ->addSubmitLink()
        ->appendSubmitButton($submitLabel,'btn btn-success')
        ->build();
        echo createJsonMessage('status',true,'message',$formContent);exit;
  } 

  // this method is for creation of form either in single or combine based on the page desire
  public function create($model,$type='add',$data=null,$service=null){
    if(!empty($type)){
      if($type=='add'){
        // this is useful for a page that doesn't follow normal procedure of a modal page
        $this->add($model,'add');
      }else{
        // this uses modal to show it content
        $this->add($model,$type,$data,$service);
      }
    }
    return "please specify a type to be created (single page or combine page with view inclusive...)";
  }

  private function add($model,$type,$param=null,$service=null)
  {
    if (!$this->webSessionManager->isSessionActive()) {
      header("Location:".base_url());exit;
    }

    $role_id=$this->webSessionManager->getCurrentUserProp('role_id');
    $userType=$this->webSessionManager->getCurrentUserProp('user_type');
    if($userType == 'admin'){
      if (!$role_id) {
        show_404();
      }
    }
    $role =false;
    if ($userType=='doctor') {
      loadClass($this->load,'agent');
      $this->agent->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->agent->load();
      $role = $this->agent->role;
      $data['agent']=$this->agent;
    }
    else if($userType == 'admin'){
      loadClass($this->load,'admin');
      $this->admin->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->admin->load();
      $role = $this->admin->role;
      $data['admin']=$this->admin;
      $data['currentRole']=$role;
      $type = ($type == 'add') ? 'create' : $type;
      $path ="vc/$type/".$model;

      if (!$role->canView($path)) {
        show_access_denied($this->load);exit;
      }
      $type = ($type == 'create') ? 'add' : $type;
      $this->load->model('custom/adminData');
      $sidebarContent=$this->adminData->getCanViewPages($role);
      $data['canView']=$sidebarContent;
    }else if($userType == 'patient'){
      loadClass($this->load,'patient');
      $this->patient->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
      $this->patient->load();
      $role = true;
      $data['patient']=$this->patient;
    }

    if ($model==false) {
      show_404();
    }

    loadClass($this->load,$model);
    $modelClass = new $model();
    $this->load->model('Crud');
    $this->load->model('modelFormBuilder');
    if (!is_subclass_of($modelClass ,'Crud')) {
      show_404();exit;
    }
    $this->load->model('formConfig');
    $formConfig = new formConfig($role);
    $data['configData']=$formConfig->getInsertConfig($model);
    $data['model']=$model;
    if($param){
      $data['param'] = $param;
    }
    if($service){
      $param = "";
      if($service == 'question'){
        $param = "question";
      }else if($service == 'chat'){
        $param = "chat";
      }
      loadClass($this->load,'health_services');
      $service = $this->health_services->getService($param);
      $data['service_id'] = $service[0]['ID'];
      $data['service_fee'] = $service[0]['price'];
    }
    $this->load->view("$type",$data);
  }

  function changePassword()
  {
    if(isset($_POST) && count($_POST) > 0 && !empty($_POST)){
      $curr_password = trim($_POST['current_password']);
      $new = trim($_POST['password']);
      $confirm = trim($_POST['confirm_password']);

      if (!isNotEmpty($curr_password,$new,$confirm)) {
        echo createJsonMessage('status',false,'message',"empty field detected.please fill all required field and try again");
        return;
      }
      
      $id= ($_POST['userID'] != '') ? $_POST['userID'] : $this->webSessionManager->getCurrentUserProp('ID');
      // print_r($id);exit;
      $this->load->model('entities/user');
      if($this->user->findUserProp($id)){
        // $check = md5(trim($curr_password)) == $this->user->data()[0]['password'];
        $check = $this->hash_created->decode_password(trim($curr_password), $this->user->data()[0]['password']);
        if(!$check){
          echo createJsonMessage('status',false,'message','please type in your password correctly...','flagAction',false);
          return;
        }
      }

      if ($new !==$confirm) {
        echo createJsonMessage('status',false,'message','new password does not match with the confirmation password','flagAction',false);exit;
      }
      // $new = md5($new);
      $new = $this->hash_created->encode_password($new);
        $query = "update user set password = '$new' where ID=?";
        if ($this->db->query($query,array($id))) {
          $arr['status']=true;
          $arr['message']= 'operation successfull';
          $arr['flagAction'] = true;
          echo json_encode($arr);
          return;
        }
        else{
          $arr['status']=false;
          $arr['message']= 'error occured during operation...';
          $arr['flagAction'] = false;
          echo json_encode($arr);
          return;
        }
    }
    return false;
  }

  public function secure($page=null,$param1=null,$param2=null){
    if(!$page){
      exit("The page doesn't exists");
    }
    $data = array();
    $this->load->view("secure/$page",$data);
  }

}

?>

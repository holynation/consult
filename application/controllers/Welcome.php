<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('string');
	}

	public function index()
	{
		$this->load->helper('string');
		$this->load->view('home');
	}

	public function login(){
		$this->load->view('login');
	}

	public function termCondition(){
		$this->load->view('terms');
	}

	public function blank(){
		$this->load->view('blank');
	}
}

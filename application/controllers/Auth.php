<?php 
/**
* 
*/
class Auth extends CI_Controller
{
	// I WANNA ADD THE SECURITY CHECK TO SOME MODEL IN THE CODE BASE,USING NERMES THAT I READ IN THE FIREFOX BOOKMARK
	// I ALSO WANNA LOOK INTO SOME MINOR ISSUE RELATION TO REDIRECTION IN JAVASCRIPT

	function __construct()
	{
		parent::__construct();
		$this->load->model('entities/user');
		$this->load->model('webSessionManager');
		$this->load->library('hash_created');
	}

	public function signup($data = ''){
		$this->load->view('register', $data);
	}

	public function login($data = ''){
		$this->load->view('login', $data );
	}

	public function forget($data = ''){
		$this->load->view('forget_password',$data);
	}

	public function register(){
		if(isset($_POST) && count($_POST) > 0 && !empty($_POST)){
			$data = $this->input->post(null, true);
			$firstname = trim($data['firstname']);
			$lastname = trim($data['lastname']);
			$email = (trim(@$data['email']) != "") ? @$data['email'] : "";
			$phone_num = trim($data['phone_num']);
			$gender = trim($data['gender']);
			$password = trim($data['password']);
			$conf_password = trim($data['conf_password']);

			// print_r($data);exit;
			
			if(!isset($data['checkboxSignup'])){
				$arr['status'] = false;
				$arr['message'] = 'please check the terms and condition...';
				echo json_encode($arr);
				return;
			}

			if (!isNotEmpty($firstname,$gender,$phone_num,$password,$conf_password,$email)) {
				echo "empty field detected . please fill all required field and try again";
				return;
			}

			if(!empty($email)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE){
					$arr['status'] = false;
					$arr['message'] = 'email is not valid';
					echo json_encode($arr);
					return;
				}
			}

			if($password != $conf_password){
				$arr['status'] = false;
				$arr['message'] = 'confirm Password should match with password';
				echo json_encode($arr);
				return;
			}

			$result = $this->user->postUser($firstname,$lastname,$email,$password,$phone_num,$gender);

			switch ($result) {
				case 1:
					$arr['status'] = true;
					$arr['message']= 'You have successfully created an account.You can now login...';
					echo json_encode($arr);
					return;
				break;

				case 3:
					$arr['status'] = false;			
					$arr['message']= "email already exists.Try another email.";
					echo json_encode($arr);
					return;
				break;

				case 4:
					$arr['status'] = false;			
					$arr['message']= "error occurred while performing the operation";
					echo json_encode($arr);
					return;
				break;

				case 2:
				$arr['status'] = false;
					$arr['message']= "error occurred while performing the operation";
					echo json_encode($arr);
					return;
				break;
			}
		}
		$this->signup();
	}
	// $2y$10$gxRfymE.QsnSwCwuh6QG2.ZBGx33uW56AmjRvQaZM3utic1OSzdLa
	public function web(){
		$isAjax =  isset($_POST['isajax']);
		if(isset($_POST) && count($_POST) > 0 && !empty($_POST)){
			$email = $this->input->post('email',true);
			$password = $this->input->post('password',true);

			if (!isNotEmpty($email,$password)) {
				echo createJsonMessage('status',false,'message',"please fill all required field and try again");
				return;
			}
			
			$find = $this->user->find($email);
			// $checkPass = md5(trim($password)) == $this->user->data()[0]['password'];
			if($find){
				$checkPass=$this->hash_created->decode_password(trim($password), $this->user->data()[0]['password']);
				if(!$checkPass){
					if ($isAjax) {
						$arr['status']=false;
						$arr['message']= "invalid username or password";
						echo  json_encode($arr);
						return;
					}
					else{
						redirect(base_url());
					}
				}
				$array = array('username'=>$email,'status'=>1);
				$user = $this->user->getWhere($array,$count,0,null,false," order by field('user_type','admin')");
				if($user == false) {
					if ($isAjax) {
						$arr['status']=false;
						$arr['message']= "invalid email or password";
						echo json_encode($arr);
						return;
					}
					else{
						redirect(base_url());
					}
				}
				else{
					$user = $user[0];
					$baseurl = base_url();
					$this->webSessionManager->saveCurrentUser($user,true);
					$baseurl.=$this->getUserPage($user);
					if ($isAjax) {
						$arr['status']=true;
						$arr['message']= $baseurl;
						echo  json_encode($arr);
						return;
					}else{
						redirect($baseurl);exit;
					}
				}
			}else{
				$arr['status']=false;
				$arr['message'] = 'invalid email or password';
				echo json_encode($arr);exit;
			}
		}

		$this->login();
	}

	public function forgetPassword(){
		if(isset($_POST) && count($_POST) > 0 && !empty($_POST)){
			if($_POST['task'] == 'reset'){
				$email = trim($this->input->post('email',true));
				if (!isNotEmpty($email)) {
			        echo createJsonMessage('status',false,"message","empty field detected.please fill all required field and try again");
			        return;
			    }
				if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE){
					$arr['status'] = false;
					$arr['message'] = 'email is not valid';
					echo json_encode($arr);
					return;
				}
				$find = $this->user->find($email);
				if(!$find){
					$arr['status'] = false;
					$arr['message'] = 'the email address appears not to be on our platform...';
					echo json_encode($arr);
					return;
				}
				$this->load->library('email');
				$emailObj = $this->email;
				$sendMail = (sendMailToRecipient($emailObj,$email,'Request to Reset your Password. !',2)) ? true : false;
				$message = "A link to reset your password has been sent to $email.If you don't see it, be sure to check your spam folders too!";
				$arr['status'] = ($sendMail) ? true : false;
				$arr['message'] = ($sendMail) ? $message : 'error occured while performing the operation,please check your network and try again later.';
				echo json_encode($arr);
				return;
			}
			else if ($_POST['task'] == 'update'){
				if(isset($_POST['email'], $_POST['email_hash']))
                {
                	if($_POST['email_hash'] !== sha1($_POST['email'] . $_POST['email_code'])){
                		// either a hacker or they changed their mail in the mail field, just die
	                    $arr['status'] = false;
						$arr['message'] = 'Oops,error updating your password';
						echo json_encode($arr);
						return;
                	}
                	$new = trim($_POST['password']);
				    $confirm = trim($_POST['confirm_password']);
				    $email = trim($_POST['email']);
				    $dataID = $email;

				    if (!isNotEmpty($new,$confirm)) {
				        echo createJsonMessage('status',false,"message","empty field detected.please fill all required field and try again");
				        return;
				    }

				    if ($new !== $confirm) {
				       echo createJsonMessage('status',false,'message','new password does not match with the confirmation password');return;
				    }
				    $this->load->model('entities/user');
				    // i wanna look into this later to find a way for member to reset password
				    if($this->webSessionManager->getCurrentUserProp('user_type') == 'member'){
				    	loadClass($this->load, 'member');
				    	$userData = $this->member->getWhere(array('email' => $email),$count,0,1,false);
				    	if($userData){
				    		$userData = $userData[0];
				    		$dataID = $userData->ID;
				    	}else{
				    		exit("Sorry, it seems the user doesn't have an email account...");
				    	}
				    }
				    $updatePassword = $this->user->updatePassword($dataID,$new);
				    if($updatePassword){
				    	$arr['status'] = true;
						$arr['message'] = 'your password has been reset! You may now login.';
						echo json_encode($arr);
						return;
				    }else{
				    	$senderEmail = appConfig('company_email');
						$arr['status'] = false;
						$arr['message'] = "error occured while updating your password. Please contact Administrator {$senderEmail}";
						echo json_encode($arr);
						return;
				    }
                    
                }
                
			}
		}
		$this->forget();
	}

	public function verify($email,$hash,$type){
		if(isset($email,$hash,$type)){
			$email = urldecode(trim($email));
			$email = str_replace(array('~az~','~09~'),array('@','.com'),$email);
			$hash = urldecode(trim($hash));
			$email_hash = sha1($email . $hash);

			$user = $this->user->find($email);
			$data = array();
			if(!$user){
				$data['error'] = 'sorry we don\'t seems to have that email account on our platform.';
				$this->load->view('verify',$data);return;
			}

			$check = md5(appConfig('salt') . $email) == $hash;
			if(!$check){
				$data['error'] = 'there seems to be an error in validating your email account,try again later.';
				$this->load->view('verify',$data);return;
			}

			if($user && $check){
				$mailType = appConfig('type');
				if($mailType[$type] == 'register'){
					$id = $this->user->data()[0]['ID'];
					$result = $this->user->updateStatus($id,$email);
					$data['type'] = $mailType[$type];
					if($result){
						//sending the login details to users
						$this->load->library('email');
						$emailObj = $this->email;
						$accSubject = appConfig('company_name'). " authentication summary details...";
						$sendMail = (sendMailToRecipient($emailObj,$email,$accSubject,3)) ? true : false;
						if($sendMail){
							$data['success'] = "Your Account has been verified.Welcome on board.<br />Thank you!";
						}else{
							$data['error'] = 'There is an error in network connection, please try again later...';
						}
						 
					}else{
						$data['error'] = 'There seems to be an error in performing the operation...';
					}
				}else if($mailType[$type] == 'forget'){
					$data['type'] = $mailType[$type];
					$data['email_hash'] = $email_hash;
					$data['email_code'] = $hash;
					$data['email'] = $email;
				}
				$this->load->view('verify',$data);return;
			}
			
		}
	}

	private function getUserMail(){
		// trying to use the email of the user type that register if customer has no email
		$userMail=appConfig('company_email');
		$userType = $this->webSessionManager->getCurrentUserProp('user_type');
		if($userType == 'admin'){
			loadClass($this->load, 'admin');
			$this->admin->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
			$this->admin->load();
			$userMail = $this->admin->email;
		}else if ($userType == 'doctor'){
			loadClass($this->load,'doctor');
			$this->doctor->ID = $this->webSessionManager->getCurrentUserProp('user_table_id');
			$this->doctor->load();
			$userMail = $this->doctor->email;
		}else if($userType == 'member'){
			// do something later
		}
		return $userMail;
	}

	private function generateHashRef($type=''){
		$hash = "#".randStrGen(3).randStrGen(5).date("s"); //  the total should be 10 in character
		$ref = randStrGen(10);
		$result = array('receipt' => $hash,'reference' => $ref);
		return $result[$type];
	}

	public function verifyTransaction($serviceId=null,$model=null,$modelID=null){
		$serviceId = urldecode($serviceId);
		date_default_timezone_set("Africa/Lagos");
		$curl = curl_init();
		$reference = isset($_GET['reference']) ? @$_GET['reference'] : "";
		if(!$reference){
		  die('No reference supplied');
		}
		$this->db->trans_begin(); // start transaction
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($reference),
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_HTTPHEADER => [
		    "accept: application/json",
		    "authorization: Bearer sk_test_bf08ff698ef666e2b9e4248ddab8c801bd5070ea",
		    "cache-control: no-cache"
		  ],
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		if($err){
			$this->db->trans_rollback();
		    // there was an error contacting the Paystack API
		  die('Curl returned error: ' . $err);
		}

		if(!$response){
			$this->db->trans_rollback();
			// var_dump($request);
			die("Something went wrong while executing curl.");
		}
		$tranx = json_decode($response,true);
		if(!$tranx){
			$this->db->trans_rollback();
			// print_r($tranx);
			die("Something went wrong while trying to convert the request variable to json.");
		}

		if(!$tranx['data']){
			$this->db->trans_rollback();
		  // there was an error from the API
		  die('API returned error: ' . $tranx['message']);
		}

		if($tranx['data']['status'] === 'success'){
		  // transaction was successful...
			$this->postTransactionHistory($serviceId,$reference,$model,$modelID);
		}else{
			$this->db->trans_rollback();
			log_message("debug","Transaction was not successful: Last gateway response was: " .$tranx['data']['gateway_response']);
			// print_r($tranx);
			exit("Transaction was not successful: Last gateway response was: ".$tranx['data']['gateway_response']);
		}
		
	}

	private function postTransactionHistory($serviceId,$reference,$modelParam=null,$modelID=null){
		$userId = $this->webSessionManager->getCurrentUserProp('user_table_id');
		date_default_timezone_set("Africa/Lagos");
		log_message("info","Transaction was successful: $serviceId/$reference");
		$currentTimeStamp = date("Y-m-d H:i:s");
		$receiptHash = $this->generateHashRef('receipt');
		$serviceField = "health_services_id";
		$model=null;
		$appointID="";
		if(!is_null($modelParam) && !is_null($modelID)){
			$model = $modelParam;
			$appointID = $modelID;
		}
		$param = array(
			'patient_id'=>$userId,
			"$serviceField" => $serviceId,
			'appointment_id' => $appointID,
			'reference_id' => $reference,
			'receipt_ref' => $receiptHash,
			'status' => '1',
			'date_created'=> $currentTimeStamp
		);
		$entityClass = "payment_history";
		loadClass($this->load,"$entityClass");
		$ucFirstClass = ucfirst($entityClass);
		$th = new $ucFirstClass($param);
		if ($th->insert($this->db,$message)) {
			$this->db->trans_commit();//end the transaction
			$this->webSessionManager->setFlashMessage('success',"Your transaction was successful");
			$appointID = rndEncode($appointID);
			$userIDHash = rndEncode($userId);
			$paramRef = (is_null($model)) ? "" : "&m=$model&r=$reference&book_id=$appointID&u=$userIDHash";
	  		redirect("/vc/patient/success/?msg=okay$paramRef",'refresh');
		}else{
			$this->db->trans_rollback();
			exit('something went wrong while performing the operation.');
		}
	}

	private function getUserPage($user){
		$link= array('patient'=>'vc/patient/dashboard','admin'=>'vc/admin/dashboard','doctor'=>'vc/doctor/dashboard');
		$roleName = $user->user_type;
		return $link[$roleName];
	}

	public function logout1(){
		$this->user->logout();
		redirect('/welcome/','refresh');
	}

	public function logout(){
		$link ='';
		$base = base_url();
		$this->webSessionManager->logout();
		$path = $base.$link;
		header("location:$path");exit;
	}

}
 ?>
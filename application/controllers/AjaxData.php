<?php
	/**
	* model for loading extra data needed by pages through ajax
	*/
	class AjaxData extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
			$this->load->model("modelFormBuilder");
			$this->load->database();
			$this->load->model('webSessionManager');
			// $this->load->model('entities/application_log');
			$this->load->helper('string');
			$this->load->helper('array');
			$this->load->helper('date');
			date_default_timezone_set("Africa/Lagos"); // setting the default date timestamp
			if (!$this->webSessionManager->isSessionActive()) {
				echo "session expired please re login to continue";
				exit;
			}
			$exclude=array('changePassword','savePermission','approve','disapprove');
			$page = $this->getMethod($segments);
			if ($this->webSessionManager->getCurrentUserProp('user_type')=='admin' && in_array($page, $exclude)) {
				loadClass($this->load,'role');
				$this->role->checkWritePermission();
			}
		}

		private function getMethod(&$allSegment)
		{
			$path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$base = base_url();
			$left = ltrim($path,$base);
			$result = explode('/', $left);
			$allSegment=$result;
			return $result[0];
		}

		public function lga($state){
			$state = urldecode($state);
			$result = loadLga($state);
			echo $this->returnJSONFromNonAssocArray($result);
		}

		public function checkDoctorSchedule($appointDate,$doctorID){
			$day = dayOfWeek(formatToSqlDate(urldecode($appointDate)));
			$doctorID = urldecode($doctorID);
			$currentTime = time();
			$currentTimeFormat = date('H:i:s',$currentTime);
			$query = "SELECT schedule.id,available_time_start from schedule where '$currentTimeFormat' between cast(available_time_start as time) and cast(available_time_end as time) and schedule.doctor_id = ? and schedule.working_days_id in (select id from working_days where name = '$day')";
			$result = $this->db->query($query,array($doctorID));
			echo $this->returnJSONTransformArray($query,array($doctorID),'',"Warning:The Doctor is not available at the moment.Choose another day/time.Thank you");
		}

		public function checkAppointTime($appointTime,$doctorID,$appointDate){
			$appointTime = formatToSqlTime(urldecode($appointTime));
			$appointDate = formatToSqlDate(urldecode($appointDate));
			$doctorID = urldecode($doctorID);
			$currentTime = time();
			$currentTimeFormat = date('H:i:s',$currentTime);
			$currentDate = date('Y-m-d');
			if(checkTimeGreater($currentTimeFormat,$appointTime) && !isDateGreater($appointDate,$currentDate)){
				//this means that the time has passed not users can't chat in a past time but greater time ahead
				echo createJsonMessage('status',false,'message',"Sorry you can't choose a time that had past",'flagAction',false);return;
			}
			$query = "select id,doctor_id as value from appointment where doctor_id = ? and cast(appoint_time as time) = ? and cast(appoint_date as date) = ? limit 1";
			echo $this->returnJSONTransformArray($query,array($doctorID,$appointTime,$appointDate),"There's no free alloted time at the moment.Choose another day/time...");
		}

		private function returnJSONTransformArray($query,$data=array(),$valMessage='',$errMessage=''){
			$newResult=array();
			$result = $this->db->query($query,$data);
			if($result->num_rows() > 0){
				$result = $result->result_array();
				if($valMessage != ''){
					$result[0]['value'] = $valMessage;
				}
				return json_encode($result[0]);
			}else{
				if($errMessage != ''){
					$dataParam = array('value' => $errMessage);
					return json_encode($dataParam);
				}
				return json_encode(array());
			}
		}

		private function returnJSONFromNonAssocArray($array){
			//process if into id and value then
			$result =array();
			for ($i=0; $i < count($array); $i++) {
				$current =$array[$i];
				$result[]=array('id'=>$current,'value'=>$current);
			}
			return json_encode($result);
		}

		protected function returnJsonFromQueryResult($query,$data=array(),$valMessage='',$errMessage=''){
			$result = $this->db->query($query,$data);
			if ($result->num_rows() > 0) {
				$result = $result->result_array();
				if($valMessage != ''){
					$result[0]['value'] = $valMessage;
				}
				// print_r($result);exit;
				return json_encode($result);
			}
			else{
				if($errMessage != ''){
					$dataParam = array('value' => $errMessage);
					return json_encode($dataParam);
				}
				return "";
			}
		}


		//function for changing the password for user.
		function changePassword(){
			// $this->application_log->log('profile module','changing password');
			if (isset($_POST['ajax-sub'])) {
				$old = $_POST['oldpassword'];
				$new = $_POST['newpassword'];
				$confirm = $_POST['confirmPassword'];
				if ($new !==$confirm) {
					// $this->application_log->log('profile module','password does not match');
					echo createJsonMessage('status',false,'message','new password does not match with the confirmaton','flagAction',false);exit;
				}
				//check that this user owns the password
				loadClass($this->load,'user');
				$this->user->user_ID = $this->webSessionManager->getCurrentUserProp('ID');
				$result = $this->user->changePassword($old,$new,$message);
				// $this->application_log->log('profile module',$message);
				echo createJsonMessage('status',$result,'message',$message,'flagAction',true);
			}
		}

		public function savePermission()
		{
			
			if (isset($_POST['sub'])) {
				$role = $_POST['role'];
				if (!$role) {
					echo createJsonMessage('status',false,'message','error occured while saving permission','flagAction',false);
				}
				loadClass($this->load,'role');
				try {
					$removeList = json_decode($_POST['remove'],true);
					$updateList = json_decode($_POST['update'],true);
					$this->role->ID=$role;
					$result=$this->role->processPermission($updateList,$removeList);
					echo createJsonMessage('status',$result,'message','permission updated successfully','flagAction',true);
				} catch (Exception $e) {
					echo createJsonMessage('status',false,'message','error occured while saving permission','flagAction',false);
				}
				
			}
		}

	}
 ?>

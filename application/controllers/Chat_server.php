<?php 
/**
* 
*/
use WebSocket\Client;
class Chat_server extends CI_Controller
{
	public function index()
    {
        // Load package path
        // $this->load->add_package_path(FCPATH.'vendor/romainrg/ratchet_client');
        // $this->load->library('ratchet_client');
        // $this->load->remove_package_path(FCPATH.'vendor/romainrg/ratchet_client');

        // // Run server
        // $this->ratchet_client->set_callback('auth', array($this, '_auth'));
        // $this->ratchet_client->set_callback('event', array($this, '_event'));
        // $this->ratchet_client->run();

        $this->load->add_package_path(FCPATH . 'vendor/takielias/codeigniter-websocket');
        $this->load->library('Codeigniter_websocket');
        $this->load->remove_package_path(FCPATH . 'vendor/takielias/codeigniter-websocket');
        $this->load->model('webSessionManager');
        // Run server
        $this->codeigniter_websocket->set_callback('auth', array($this, '_auth'));
        $this->codeigniter_websocket->set_callback('event', array($this, '_event'));
        $this->codeigniter_websocket->set_callback('close', array($this, '_close'));
        $this->codeigniter_websocket->run();
    }

    public function _auth($datas = null)
    {
        // Here you can verify everything you want to perform user login.
        // However, method must return integer (client ID) if auth succedeed and false if not.
        // this is called first on connection to the network.
        // NOTE: return value of $data =>
        //stdClass Object
		// (
		//     [user_id] => 612 (i.e the client ID)
		//     [recipient_id] => 
		//     [message] => 
		// )
        // if(!isSessionActive()){
        //     return false;
        // }
        return (!empty($datas->user_id)) ? $datas->user_id : false;
    }

    public function _event($datas = null)
    {
        // Here you can do everyting you want, each time message is received
        // i can use a message tone received here,send a notification too.
        //Note the event callback is called first before message is received at any end
        // print_r($datas);
        echo 'Hey ! I\'m an EVENT callbacks'.PHP_EOL;
            // $client = new Client('ws://127.0.0.1:8080');

            // $client->send(json_encode(array('user_id' => 1, 'message' => null)));
            // $client->send(json_encode(array('user_id' => 1, 'message' => 'Super cool message to myself!')));
        // An error has occurred: Connection to 'ws://127.0.0.1/' failed: Server sent invalid upgrade response:

        if($datas->type == 'chat'){
            $this->insertChat($datas);
        }
        
    }

    private function insertChat($datas = array()){
        loadClass($this->load,'chat_history');
        $dateTime = formatToSqlTime($datas->time,true);
        $message = trim($datas->message);
        $msgSender = $datas->msg_sender;
        $sender = ($msgSender == 'patient') ? 1 : 0;
        $user_id="";$recipient_id="";

        // i am agreeing that user_id is patient while recipient_id is doctor irrespective it origin
        if($msgSender == 'patient'){
            $user_id = $datas->user_id;
            $recipient_id = $datas->recipient_id;
        }else if($msgSender == 'doctor'){
            $user_id = $datas->recipient_id;
            $recipient_id = $datas->user_id;
        }

        $param = array(
            'patient_id' => $user_id,
            'doctor_id' => $recipient_id,
            'message' => $message,
            'message_side' => $sender,
            'date_created' => $dateTime
        );

        $ch = new Chat_history($param);
        if($ch->insert($this->db,$message)){
            if(!empty($message)){
                echo $message . PHP_EOL;
            }
            // $user_id="";$recipient_id="";
            echo "message sent" . PHP_EOL;
        }
        
    }

    public function _close($connection = null)
    {
        if($connection->WebSocket->closing == 1){
            print_r($connection->send(json_encode(array('user_id' => 1, 'message' => null))));
            print_r($connection->send(json_encode(array('user_id' => 1, 'message' => 'Super cool message to myself!'))));
        }
    }
}
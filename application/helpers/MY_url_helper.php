<?php 

//function for sending post http request using curl
function sendPost($url,$post,&$errorMessage,$returnResult=false){
	$res = curl_init($url);
	curl_setopt($res, CURLOPT_POST,true);
	curl_setopt($res, CURLOPT_POSTFIELDS, $post);
	$certPath =str_replace( "application\helpers\MY_url_helper.php",'cacert.pem', __FILE__);
	curl_setopt($res, CURLOPT_CAINFO, $certPath);
	if ($returnResult) {
		curl_setopt($res, CURLOPT_RETURNTRANSFER, true);
	}
	$referer = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	curl_setopt($res, CURLOPT_REFERER, $referer);
	$result = curl_exec($res);
	$errorMessage = curl_error($res);
	curl_close($res);
	return $result;
}

function initializeTransact($serviceID,$priceInKobo,$email,$phone=null,$fullname=null,$modelParam=null){
	$param = (is_null($modelParam)) ? "$serviceID" : "$serviceID/$modelParam";
	$callbackUrl = base_url("auth/verifyTransaction/$param");
	$curlParam = array(
	'amount'=>$priceInKobo,
	'email'=>$email,
	'callback_url' => $callbackUrl,
	'metadata'=> json_encode(
		[
	    	"custom_fields" => [
	    		[
	    			'display_name'=>"Phone Number",
		    		'variable_name'=>"phone_number",
		    		"value"=> $phone
	    		],
	    		[
	    			'display_name'=>"Full Name",
		    		'variable_name'=>"full_name",
		    		"value"=> $fullname
	    		]
	    	]
		])
	);
	$curlParam = json_encode($curlParam);
	return $curlParam;
}

function makeOnlineTransaction($postParam,&$errorMessage=''){
	$curl = curl_init();
	$curlParam = $postParam;

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => $curlParam,
	  CURLOPT_HTTPHEADER => [
	    "authorization: Bearer sk_test_bf08ff698ef666e2b9e4248ddab8c801bd5070ea", //replace this with your own test key
	    "content-type: application/json",
	    "cache-control: no-cache"
	  ],
	));

	$response = curl_exec($curl);
	$errorMsg = curl_error($curl);
	if($errorMsg){
		// die('Curl returned error: ' . $errorMessage);
		$errorMessage = 'Curl returned error: ' . $errorMsg;
	}
	$tranx = json_decode($response, true);

	if(!$tranx['status']){
	  // there was an error from the API
	  // print_r('API returned error: ' . $tranx['message']);
	  $errorMessage = 'API returned error: ' . $tranx['message'];
	}
	$checkOut = $tranx['data']['authorization_url'];
	return $checkOut;
	// header('Location: ' . $tranx['data']['authorization_url']);
}

?>
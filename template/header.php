<?php $userType = $this->webSessionManager->getCurrentUserProp('user_type'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?php $modelPage = (@$model) ? removeUnderscore(@$model): 'Lookout'; 
        echo getTitlePage($modelPage); ?>
    </title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/global/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/private/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/global/js/plugins/toastr/toastr.css" rel="stylesheet" type="text/css">
    <?php if(@$model == 'doctor' || @$model == 'schedule'): ?>
    <link href="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/global/js/plugins/fancy-time-picker/timedropper.css" rel="stylesheet" type="text/css">
    <?php endif; ?>
    <!-- /global stylesheets -->
    <script src="<?php echo base_url(); ?>assets/global/js/main/jquery.min.js"></script>
</head>

<body class="navbar-top">
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark fixed-top">
        <div class="navbar-brand">
            <a href="<?php echo base_url(); ?>" class="d-inline-block">
                <img src="<?php echo base_url(); ?>assets/global/images/logo_light.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        Text link
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-bell2"></i>
                        <span class="d-md-none ml-2">Notifications</span>
                        <span class="badge badge-mark border-white ml-auto ml-md-0"></span>
                    </a>                    
                </li>
                 <?php
                    $profileLink = '';
                    $profileImage = 'assets/images/users/avatar-4.jpg';
                    if($userType == 'doctor'){
                        $profileLink = 'vc/doctor/profile';
                        $profileImage = (@$doctor->doctor_path) ? @$doctor->doctor_path : $profileImage;
                    }
                    if($userType == 'admin'){
                        $profileLink = 'vc/admin/profile';
                        $profileImage = (@$admin->admin_path) ? @$admin->admin_path : $profileImage;
                    }
                    if($userType == 'patient'){
                        $profileLink = 'vc/patient/profile';
                        $profileImage = (@$patient->patient_path) ? @$patient->patient_path : $profileImage;
                    }
                ?>
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link dropdown-toggle mr-0 waves-effect waves-light" data-toggle="dropdown">
                        <img src="<?php echo base_url($profileImage); ?>" class="rounded-circle" alt="user-image">
                        <span>
                            <?php 
                            $firstname = $this->webSessionManager->getCurrentUserProp('firstname');
                            $lastname = $this->webSessionManager->getCurrentUserProp('lastname');
                            echo $firstname .' '. Ucfirst(getFirstString($lastname)); ?>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="<?php echo base_url($profileLink); ?>" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
                        <a href="<?php echo base_url('auth/logout'); ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->

    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-dark sidebar-main sidebar-fixed sidebar-expand-md">
            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                Navigation
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->

            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="card-body">
                        <div class="media">
                            <div class="mr-3">
                                <a href="#"><img src="<?php echo base_url($profileImage); ?>" width="38" height="38" class="rounded-circle" alt=""></a>
                            </div>

                            <div class="media-body">
                                <div class="media-title font-weight-semibold"><?php echo $firstname .' '. $lastname; ?></div>
                                <div class="font-size-xs opacity-50">
                                    <i class="icon-pin font-size-sm"></i> &nbsp;<?php echo ucfirst($userType); ?> Dash
                                </div>
                            </div>

                            <div class="ml-3 align-self-center">
                                <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->

                <!-- Main navigation -->
            <?php include_once 'template/nav.php'; ?>
                <!-- /main navigation -->
            </div>
            <!-- /sidebar content -->
        </div>
        <!-- /main sidebar -->

        <?php if(($userType == 'doctor' && @$model == 'chat') || ($userType == 'patient' && @$model == 'tracers') ): ?>
        <!-- Secondary sidebar -->
        <div class="sidebar sidebar-light sidebar-secondary sidebar-expand-md">
            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-secondary-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                <span class="font-weight-semibold">Sidebar</span>
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->

            <?php if(@$userType == 'doctor'): ?>
            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Start chat -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Start chat</span>
                        <div class="header-elements">
                            <span class="badge bg-success badge-pill">+32</span>
                        </div>
                    </div>
                    <ul class="media-list media-list-linked my-2">
                        <?php if(isset($docPatients) && $docPatients): ?>
                            <?php foreach($docPatients as $docPatient): ?>
                            <li>
                                <?php 
                                $patientID = rndEncode($docPatient['id']);
                                $patientName = $docPatient['firstname']." ". $docPatient['lastname'];
                                $doctorID = ($doctor) ? $doctor->ID : "";
                                ?>
                                <a href="<?php echo base_url("vc/doctor/chat/$doctorID?p=$patientID"); ?>" class="media text-grey">
                                    <div class="mr-3">
                                        <img src="<?php echo base_url($docPatient['patient_path']); ?>" class="rounded-circle" width="40" height="40" alt="">
                                    </div>
                                    <div class="media-body">
                                    
                                        <div class="media-title font-weight-semibold"><?php echo $patientName; ?></div>
                                        <span class="text-muted font-size-sm">Patient</span>
                                    </div>
                                    <div class="align-self-center ml-3">
                                        <span class="badge badge-pill bg-info border-success border-2 border-white">2</span>
                                    </div>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <!-- /start chat -->
            </div>
            <!-- /sidebar content -->
            <?php endif; ?>

            <?php if($userType == 'patient'): ?>
            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Start chat -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Start chat</span>
                        <div class="header-elements">
                            <span class="badge bg-success badge-pill">+32</span>
                        </div>
                    </div>
                    <?php
                        $tracersArr = array(
                            'Vital Signs' => array(
                                'Blood Pressure' => 'vc/patient/tracers/?m=blood_pressure',
                                'Blood Sugar' => 'vc/patient/tracers/?m=blood_sugar',
                                'Oxygen Level' => 'vc/patient/tracers/?m=oxygen_level',
                                'Pulse' => 'vc/patient/tracers/?m=pulse',
                                'Temperature' => 'vc/patient/tracers/?m=temperature',
                            ),
                            'Lipid Profile' => array(
                                'Cholesterol' => 'vc/patient/tracers/?m=cholesterol',
                            ),
                            'Fitness and Well Being' => array(
                                'Excercise' => 'vc/patient/tracers/?m=exercise',
                                'Height' => 'vc/patient/tracers/?m=height',
                                'Mood Tracker' => 'vc/patient/tracers/?m=mood_tracker',
                                'Sleep' => 'vc/patient/tracers/?m=sleep',
                                'Waist Measurement' => 'vc/patient/tracers/?m=waist_measurement',
                                'Weight' => 'vc/patient/tracers/?m=weight',
                            ),
                        );
                    ?>
                    <div class="card-body p-0">
                        <ul class="nav nav-sidebar" data-nav-type="accordion">
                            <?php foreach($tracersArr as $key => $value){ ?>
                            <li class="nav-item-header"><?php echo ucfirst($key); ?></li>
                            <?php foreach($value as $label => $link): ?>
                            <li class="nav-item">
                                <a href="<?php echo base_url($link); ?>" class="nav-link"><span class="badge badge-mark border-danger align-self-center mr-3"></span> <?php echo $label ?></a>
                            </li>
                            <?php endforeach; ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!-- /start chat -->
            </div>
            <!-- /sidebar content -->
            <?php endif; ?>
        </div>
        <!-- /secondary sidebar -->
        <?php endif; ?>

<style>
  #notification{
      display: none;
      position: absolute;
      width: 50%;
      z-index: 4000;
  }
  .modal-dialog {
      margin-top: 15px ;
  }
  select{
    wdith:100%;
    display: block;
  }
</style>

<div id="notification" class="alert alert-dismissable text-center"></div>
<input type="hidden" value="<?php echo base_url(); ?>" id='baseurl'>
<?php $userType = (!isset($userType)) ? $this->webSessionManager->getCurrentUserProp('user_type') : $userType ;  ?>
<!-- this is the navbar -->
<div class="card card-sidebar-mobile">
    <ul class="nav nav-sidebar" id="nav-sidebarNav" data-nav-type="accordion">
        <!-- Main -->
        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
        <?php
            $dashLink = '';
            $dashName = "Dashboard";
            $dashIcon = "icon-home4";
            if($userType == 'patient'){
                $dashLink = 'vc/patient/dashboard';
                $dashName = 'Health Providers';
                $dashIcon = "icon-user-tie";
            } else if($userType == 'admin'){
                $dashLink = 'vc/admin/dashboard';
            }else{
                $dashLink = 'vc/doctor/dashboard';
            }
        ?>
        <li class="nav-item">
            <a href="<?php echo base_url($dashLink); ?>" class="nav-link">
                <i class="<?php echo $dashIcon; ?>"></i>
                <span><?php echo $dashName; ?></span>
            </a>
        </li>
        <!-- this is the doctor nav -->
        <?php  if($userType == 'doctor'): ?>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/create/patient/'); ?>" class="nav-link">
                    <i class="icon-user"></i>
                    <span>Health Patient</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/doctor/schedule'); ?>" class="nav-link">
                    <i class="icon-calendar2"></i>
                    <span>Appointment</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/doctor/chat'); ?>" class="nav-link">
                    <i class="icon-comment-discussion"></i>
                    <span>Chat</span>
                </a>
            </li>
        <?php endif; ?>

        <!-- this is the patient nav -->
        <?php  if($userType == 'patient'): ?>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/health_profile'); ?>" class="nav-link">
                    <i class="icon-user"></i>
                    <span>Health Profile</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/tracers'); ?>" class="nav-link">
                    <i class="icon-stats-growth"></i>
                    <span>Health Tracers</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url("vc/patient/patientCreate?m=medical_reports"); ?>" class="nav-link">
                    <i class="icon-stats-dots"></i>
                    <span>Medical Reports</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/list_view?m=health_journal'); ?>" class="nav-link">
                    <i class="icon-book"></i>
                    <span>Health Journal</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/appointment?m=appointment'); ?>" class="nav-link">
                    <i class="icon-calendar2 "></i>
                    <span>Appointments</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/phone_consultation'); ?>" class="nav-link">
                    <i class="icon-phone"></i>
                    <span>Phone Consultation</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/video_consultation'); ?>" class="nav-link">
                    <i class="icon-video-camera"></i>
                    <span>Video Consultation</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/text_consultation'); ?>" class="nav-link">
                    <i class="icon-comment"></i>
                    <span>Question Consultation</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/consultation_note'); ?>" class="nav-link">
                    <i class="icon-repo-forked "></i>
                    <span>Consultation Note</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/monitoring'); ?>" class="nav-link">
                    <i class="icon-screen3"></i>
                    <span>Monitoring</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/prescription'); ?>" class="nav-link">
                    <i class="icon-file-openoffice"></i>
                    <span>Prescription</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php echo base_url('vc/patient/payment_history'); ?>" class="nav-link">
                    <i class="icon-cash3"></i>
                    <span>Payments Hstory</span>
                </a>
            </li>
        <?php endif; ?>
        <!-- this is the admin nav -->
        <?php if($userType == 'admin'){  ?>
            <?php 
              if(isset($canView)){
                foreach ($canView as $key => $value): ?>
               <?php 
                   $state='';
                    if ($canView[$key]['state']===0) {
                     continue;
                   }
            ?>
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="<?php echo $value['class']; ?>"></i> <span><?php echo $key; ?> </span></a>
                <ul class="nav nav-group-sub" data-submenu-title="<?php echo $key; ?>">
                    <?php foreach ($value['children'] as $label =>$link): ?>
                        <?php if(!is_array($link)){ ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url($link); ?>" class="nav-link"><?php echo $label; ?></a>
                        </li>
                        <?php } 
                        if(is_array($link)){ ?>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><?php echo $label; ?></a>
                                <ul class="nav nav-group-sub">
                                    <?php foreach ($link as $linkKey => $linkValue) { ?>
                                    <li class="nav-item"><a href="<?php echo $linkValue; ?>" class="nav-link"><?php echo $linkKey; ?></a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php endforeach; ?>
                </ul>
            </li>
    <?php endforeach; } } ?>
        <!-- /main -->
    </ul>
</div>
<!-- nav-item-open -->
<script type="text/javascript">
    // var liNav = $("#nav-sidebarNav").find("li").eq(2);
    // liNav.addClass("nav-item-expanded nav-item-open");
</script>
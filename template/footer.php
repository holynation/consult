    <!-- Footer -->
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                Footer
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                &copy; <?php echo date('Y'); ?> <a href="#"></a>Online Consultation by <a href="" target="_blank">Technode Solutions</a>
            </span>
        </div>
        <strong>{elapsed_time} seconds.</strong>
        
    </div>
    <!-- /footer -->
    </div>
        <!-- /main content -->
</div>
    <!-- /page content -->

<!-- Core JS files -->
    <!-- <script src="<?php //echo base_url(); ?>assets/global/js/main/jquery.min.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/global/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <script src="<?php echo base_url(); ?>assets/global/js/plugins/ui/moment/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/toastr/toastr.min.js"></script>

    <?php if(@$model == 'appointment' || (isset($trackerModel) || @$trackerModel == 'blood_sugar' || @$trackerModel == 'exercise' || @$trackerModel == 'sleep' || @$trackerModel == 'waist_measurement' || @$trackerModel == 'weight') ): ?>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/pickers/pickadate/picker.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/pickers/pickadate/picker.time.js"></script>
    <?php endif; ?>

    <script type="text/javascript" charset="utf-8" async defer>
        $('.pickadate-appoint').pickadate({
            today: 'Today',
            close: 'Close',
            clear: 'Clear',
            format: 'd mmmm yyyy',
            min: new Date()
        });
        $('.pickatime-time').pickatime({
            clear: '',
            interval: 10,
            max: [22,50],
            formatSubmit: 'HH:i',
            hiddenName: true,
            formatLabel: function(time) {
                var hours = ( time.pick - this.get('now').pick ) / 60,
                  label = hours < 0 ? ' !hours to now' : hours > 0 ? ' !hours from now' : 'now'
                return  'h:i a <sm!all class="text-muted">' + ( hours ? (Math.abs(hours).toFixed(2)) : '' ) + label +'</sm!all>'
            
            }
        });

        function loadFIleInput(fileAllow){
            var fileAllow = (fileAllow) ? fileAllow : ["jpeg","pdf","png", "txt","doc",'xls','ppt','dicom'];
            // Modal template
            var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                '  <div class="modal-content">\n' +
                '    <div class="modal-header align-items-center">\n' +
                '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                '    </div>\n' +
                '    <div class="modal-body">\n' +
                '      <div class="floating-buttons btn-group"></div>\n' +
                '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>\n';
            // Buttons inside zoom modal
            var previewZoomButtonClasses = {
                toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                fullscreen: 'btn btn-light btn-icon btn-sm',
                borderless: 'btn btn-light btn-icon btn-sm',
                close: 'btn btn-light btn-icon btn-sm'
            };

            // Icons inside zoom modal classes
            var previewZoomButtonIcons = {
                prev: '<i class="icon-arrow-left32"></i>',
                next: '<i class="icon-arrow-right32"></i>',
                toggleheader: '<i class="icon-menu-open"></i>',
                fullscreen: '<i class="icon-screen-full"></i>',
                borderless: '<i class="icon-alignment-unalign"></i>',
                close: '<i class="icon-cross2 font-size-base"></i>'
            };

            // File actions
            var fileActionSettings = {
                zoomClass: '',
                zoomIcon: '<i class="icon-zoomin3"></i>',
                dragClass: 'p-2',
                dragIcon: '<i class="icon-three-bars"></i>',
                removeClass: '',
                removeErrorClass: 'text-danger',
                removeIcon: '<i class="icon-bin"></i>',
                indicatorNew: '<i class="icon-file-plus text-success"></i>',
                indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                indicatorError: '<i class="icon-cross2 text-danger"></i>',
                indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
            };
            $('.file-input').fileinput({
                browseLabel: 'Attach a file',
                browseIcon: '<i class="icon-attachment mr-2"></i>',
                uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    modal: modalTemplate
                },
                maxFilesNum: 1,
                allowedFileExtensions: fileAllow,
                initialCaption: "No file selected",
                previewZoomButtonClasses: previewZoomButtonClasses,
                previewZoomButtonIcons: previewZoomButtonIcons,
                fileActionSettings: fileActionSettings
            });
        }
    </script>

    <?php if(@$model == 'text_consultation' || @$model == 'health_profile'): ?>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
    <script type="text/javascript" charset="utf-8" async defer>
        $("#someElseRadio").click(function(){
            $("#someoneElse").hide();
            $("<input type='hidden' name='user_type' id='user_type' value='personal' />").appendTo("input");
        });
        $("#someElseRadio1").click(function(){
            $("#someoneElse").show();
            $("<input type='hidden' name='user_type' id='user_type' value='other' />").appendTo("input");
        });
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });

        loadFIleInput();
    </script>
    <?php endif; ?>

    <?php if(@$model == 'dashboard'): ?>
    <!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/pickers/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/demo_pages/dashboard.js"></script>
    <?php endif; ?>

    <?php if(@$model == 'patient'): ?>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/jquery-mask-plugin/jquery.mask.min.js"></script>
    <?php endif; ?>

    <?php if(@$model == 'tracers'): ?>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/tables/datatables/datatables.min.js"></script>
    <?php endif; ?>

    <?php if(@$model == 'doctor' || @$model == 'schedule'): ?>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/global/js/plugins/fancy-time-picker/timedropper.min.js"></script>
    <script type="text/javascript" charset="utf-8" async defer>
        $( "#available_time_start" ).timeDropper({
          format: 'h:mm a',
          autoswitch: true,
          meridians: true,
          mousewheel: true,
          setCurrentTime: true,
          init_animation: "fadein",
          primaryColor: "#009688",
          borderColor: "#546E7A",
          backgroundColor: "#FFF",
          textColor: '#555'
        });
        $( "#available_time_end" ).timeDropper({
          format: 'h:mm a',
          autoswitch: true,
          meridians: true,
          mousewheel: true,
          setCurrentTime: true,
          init_animation: "fadein",
          primaryColor: "#009688",
          borderColor: "#546E7A",
          backgroundColor: "#FFF",
          textColor: '#555'
        });
    </script>
    <?php endif; ?>

    <?php if(@$model == 'chat'): ?>
    <script src="<?php echo base_url(); ?>assets/handlebars.runtime.js"></script>
    <script src="<?php echo base_url(); ?>assets/chats.precompiled.js"></script>
    <script src="<?php echo base_url(); ?>assets/lodash.js"></script>
    <?php endif; ?>

    <script src="<?php echo base_url(); ?>assets/private/js/app.js"></script>
    <script src="<?php echo base_url(); ?>assets/custom.js"></script>
    <!-- /theme JS files -->
</body>
</html>